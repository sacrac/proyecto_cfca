# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-09-14 03:35
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lugar', '0003_auto_20190201_1156'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comunidad',
            options={'ordering': ('nombre',), 'verbose_name_plural': 'Comunidad'},
        ),
        migrations.AlterModelOptions(
            name='departamento',
            options={'ordering': ('nombre',), 'verbose_name_plural': 'Departamentos'},
        ),
        migrations.AlterModelOptions(
            name='municipio',
            options={'ordering': ('nombre',), 'verbose_name_plural': 'Municipios'},
        ),
        migrations.AlterModelOptions(
            name='pais',
            options={'ordering': ('nombre',), 'verbose_name': 'Pais', 'verbose_name_plural': 'Paises'},
        ),
    ]
