from .models import *
from django.http import HttpResponse
import json as simplejson

from dal import autocomplete

def get_municipios(request, departamento):
    municipios = Municipio.objects.filter(departamento = departamento)
    lista = [(municipio.id, municipio.nombre) for municipio in municipios]
    return HttpResponse(simplejson.dumps(lista), mimetype='application/javascript')

#def get_comunidad(request, municipio):
#    comunidades = Comunidad.objects.filter(municipio = municipio)
#    lista = [(comunidad.id, comunidad.nombre) for comunidad in comunidades]
#    return HttpResponse(simplejson.dumps(lista), mimetype='application/javascript')

class PaisAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = Pais.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class DepartamentoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = Departamento.objects.all()

        pais = self.forwarded.get('pais', None)

        if pais:
           qs = qs.filter(pais=pais)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = Municipio.objects.all()

        departamento = self.forwarded.get('departamento', None)

        if departamento:
            qs = qs.filter(departamento=departamento)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class ComunidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = Comunidad.objects.all()

        comunidad = self.forwarded.get('comunidad', None)

        if comunidad:
            qs = qs.filter(comunidad=comunidad)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs
