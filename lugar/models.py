# -*- coding: UTF-8 -*-

from django.db import models

class Pais(models.Model):
    nombre = models.CharField(max_length=200)

    class Meta:
        verbose_name = u'Pais'
        verbose_name_plural = u'Paises'
        unique_together = ('nombre',)
        ordering = ('nombre',)

    def __str__(self):
        return self.nombre

class Departamento(models.Model):
    pais = models.ForeignKey(Pais)
    nombre = models.CharField(max_length=30, unique=True)
    slug = models.SlugField(unique=True, null=True, help_text="Usado como url unica(autorellenado)")
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Departamentos"
        unique_together = ('nombre',)
        ordering = ('nombre',)

class Municipio(models.Model):
    departamento = models.ForeignKey(Departamento)
    nombre = models.CharField(max_length=30, unique=True)
    slug = models.SlugField(unique=True, null=True, help_text="Usado como url unica(autorellenado)")
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    def __str__(self):
        return '%s - %s' % (self.departamento.nombre, self.nombre)

    class Meta:
        verbose_name_plural = "Municipios"
        ordering = ['departamento__nombre', ]
        unique_together = ('nombre',)
        ordering = ('nombre',)

class Comunidad(models.Model):
    municipio = models.ForeignKey(Municipio)
    nombre = models.CharField(max_length=40)
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)

    class Meta:
        verbose_name_plural="Comunidad"
        ordering = ('nombre',)

    def __str__(self):
        return "%s - %s" % (self.nombre, self.municipio)
