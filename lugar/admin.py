from django.contrib import admin
from .models import *
from .forms import *
from beneficiarios.models import BeneficiariosCaps
from django.db.models import Count
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class ComunidadResource(resources.ModelResource):

    class Meta:
        model = Comunidad

class MunicipioResource(resources.ModelResource):

    class Meta:
        model = Municipio

class DepartamentoResource(resources.ModelResource):

    class Meta:
        model = Departamento

class PaisAdmin(admin.ModelAdmin):
    #form = PaisAdminForm
    list_display = ['nombre']

class DepartamentoAdmin(ImportExportModelAdmin):
    resource_class = DepartamentoResource
    form = DepartamentoAdminForm
    list_display = ['nombre']
    list_filter = ['nombre']
    prepopulated_fields = {"slug": ("nombre", )}
    search_fields = ['nombre']

class MunicipioAdmin(ImportExportModelAdmin):
    resource_class = MunicipioResource
    form = MunicipioAdminForm
    list_display = ['nombre', 'departamento']
    list_filter = ['departamento']
    search_fields = ['nombre']
    prepopulated_fields = {"slug": ("nombre", )}

class FamilyCountFilter(admin.SimpleListFilter):
    title = 'Conteo por familia'
    parameter_name = 'get_family_count'

    def lookups(self, request, model_admin):
        return (
            ('Ceros', 'Conteos ceros'),
            ('Mas', 'Conteos > 1'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Ceros':
            return queryset
        elif value == 'Mas':
            return queryset.filter(beneficiarioscaps__jefe_familia=1).annotate(_get_family_count=Count('beneficiarioscaps__id'))
        return queryset

class ComunidadAdmin(ImportExportModelAdmin):
    resource_class = ComunidadResource
    form = ComunidadAdminForm
    list_display = ['nombre', 'municipio','get_family_count']
    list_filter = ['municipio',FamilyCountFilter]
    search_fields = ['nombre']

    # def get_queryset(self, request):
    #     queryset = super().get_queryset(request)
    #     print("Cagada")
    #     queryset = queryset.filter(beneficiarioscaps__jefe_familia__in=[1,2]).annotate(_get_family_count=Count('beneficiarioscaps__id'))
    #     print(queryset)
        
    #     return queryset

    def get_family_count(self, obj):
        #print(obj)
        total = BeneficiariosCaps.objects.filter(comunidad_id=obj.id,jefe_familia=1).count()
        return total

    get_family_count.short_description = 'Número de familias'
    #get_family_count.admin_order_field = '_get_family_count'


    
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(Comunidad, ComunidadAdmin)
admin.site.register(Pais, PaisAdmin)




