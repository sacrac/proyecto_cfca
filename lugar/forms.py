from django import forms

from dal import autocomplete

from .models import Pais,Departamento, Municipio, Comunidad


class PaisAdminForm(forms.ModelForm):

    class Meta:
        model = Pais
        fields = '__all__'
        widgets = {
            'nombre': autocomplete.ModelSelect2(url='pais-autocomplete', attrs={'data-language': 'es'}),

        }


class DepartamentoAdminForm(forms.ModelForm):

    class Meta:
        model = Departamento
        fields = '__all__'
        widgets = {
            'pais': autocomplete.ModelSelect2(url='pais-autocomplete', attrs={'data-language': 'es'}),
            # 'nombre': autocomplete.ModelSelect2(url='departamento-autocomplete',forward=['pais'], attrs={'data-language': 'es'}),

        }

class MunicipioAdminForm(forms.ModelForm):

    class Meta:
        model = Municipio
        fields = '__all__'
        widgets = {
            'departamento': autocomplete.ModelSelect2(url='departamento-autocomplete', attrs={'data-language': 'es'}),
            # 'nombre': autocomplete.ModelSelect2(url='municipio-autocomplete',forward=['departamento'], attrs={'data-language': 'es'}),

        }

class ComunidadAdminForm(forms.ModelForm):

    class Meta:
        model = Comunidad
        fields = '__all__'
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='municipio-autocomplete', attrs={'data-language': 'es'}),
            # 'nombre': autocomplete.ModelSelect2(url='comunidad-autocomplete',forward=['municipio'], attrs={'data-language': 'es'}),

        }
