from django.conf.urls import url, include
from .views import (PaisAutocomplete, DepartamentoAutocomplete,
                    MunicipioAutocomplete,ComunidadAutocomplete)



urlpatterns = [
    url(
        r'^pais-autocomplete/$',
        PaisAutocomplete.as_view(),
        name='pais-autocomplete',
    ),
    url(
        r'^departamento-autocomplete/$',
        DepartamentoAutocomplete.as_view(create_field='nombre'),
        name='departamento-autocomplete',
    ),
    url(
        r'^municipio-autocomplete/$',
        MunicipioAutocomplete.as_view(create_field='nombre'),
        name='municipio-autocomplete',
    ),
    url(
        r'^comunidad-autocomplete/$',
        ComunidadAutocomplete.as_view(create_field='nombre'),
        name='comunidad-autocomplete',
    ),

]
