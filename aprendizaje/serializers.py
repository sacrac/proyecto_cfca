from rest_framework import serializers
from appdirectorio.models import Directorio
from aprendizaje.models import Cursos, Modulos, Contenidos
from appdirectorio.models import Directorio


SEARCH_PATTERN = 'src=\"/media/uploads/'
SITE_DOMAIN = "https://www.caps-nicaragua.org"
REPLACE_WITH = 'src=\"%s/media/uploads/' % SITE_DOMAIN

class FixAbsolutePathSerializer(serializers.Field):

    def to_representation(self, value):
        text = value.replace(SEARCH_PATTERN, REPLACE_WITH)
        return text


class CursosSerializer(serializers.ModelSerializer):
    descripcion = FixAbsolutePathSerializer()
    class Meta:
        model = Cursos
        fields = ['id', 'titulo', 'imagen', 'descripcion', 'fecha', 'modificado']


class ModulosSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Modulos
        fields = ['id', 'curso_id', 'titulo', 'order', 'modificado']


class ContenidosSerializer(serializers.HyperlinkedModelSerializer):
    contenido = FixAbsolutePathSerializer()
    class Meta:
        model = Contenidos
        fields = ['id', 'modulo_id', 'titulo', 'contenido','order','nombre_video','url_video', 'modificado']


class DirectoriosSerializer(serializers.ModelSerializer):
	departamento = serializers.ReadOnlyField(source="departamento.nombre")
	municipio = serializers.ReadOnlyField(source="municipio.nombre")
	comunidad = serializers.ReadOnlyField(source="comunidad.nombre")
	tema = serializers.ReadOnlyField(source="tema.nombre_tema")
	tema_slug = serializers.ReadOnlyField(source="tema.slug")

	class Meta:
		model = Directorio
		fields = ('id','departamento','municipio','comunidad',
			      'tema','tema_slug','empresa','nombre_persona','direccion', 
			      'telefono', 'oficina','email','modificado')