# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-08-15 15:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aprendizaje', '0006_auto_20190705_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='contenidos',
            name='url_video',
            field=models.URLField(blank=True, null=True),
        ),
    ]
