# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-10-18 20:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aprendizaje', '0007_contenidos_url_video'),
    ]

    operations = [
        migrations.AddField(
            model_name='contenidos',
            name='nombre_video',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Nombre del video'),
        ),
    ]
