# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2021-05-26 16:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aprendizaje', '0009_cursos_activo'),
    ]

    operations = [
        migrations.AddField(
            model_name='contenidos',
            name='modificado',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='cursos',
            name='modificado',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='modulos',
            name='modificado',
            field=models.IntegerField(default=0),
        ),
    ]
