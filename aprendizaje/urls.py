from django.conf.urls import url

from . import views, api_views

urlpatterns = [
    url(r'^lista-cursos/$', views.CourseListView.as_view(), name="list_course"),
    url(r'^(?P<slug>[-\w]+)/$', views.CourseDetailView.as_view(), name="detail_course"),
    url(r'^seguir/(?P<id>[-\w]+)/$/', views.seguir_curso, name='seguir_curso'),
	url(r'^dejar/seguir/(?P<id>[-\w]+)/$/', views.dejar_seguir_curso, name='dejar_seguir_curso'),
	#api
	url(r'^api/v2/cursos/$', api_views.CursosList.as_view(), name="api_list_cursos"),
	url(r'^api/v2/modulos/$', api_views.ModulosList.as_view(), name="api_list_modulos"),
	url(r'^api/v2/contenidos/$', api_views.ContenidosList.as_view(), name="api_list_contenidos"),
	url(r'^api/v2/directorio/$', api_views.DirectorioList.as_view(), name="api_list_directorio"),
]
