from .models import Cursos, Modulos, Contenidos
from appdirectorio.models import Directorio
from .serializers import CursosSerializer, ModulosSerializer, ContenidosSerializer, DirectoriosSerializer
from rest_framework import generics

class CursosList(generics.ListCreateAPIView):
    queryset = Cursos.objects.filter(activo=True)
    serializer_class = CursosSerializer

class ModulosList(generics.ListCreateAPIView):
    queryset = Modulos.objects.all()
    serializer_class = ModulosSerializer

class ContenidosList(generics.ListCreateAPIView):
    queryset = Contenidos.objects.all()
    serializer_class = ContenidosSerializer

class DirectorioList(generics.ListCreateAPIView):
    queryset = Directorio.objects.all()
    serializer_class = DirectoriosSerializer