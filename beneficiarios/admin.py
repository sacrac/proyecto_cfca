from django.contrib import admin
from .models import BeneficiariosCaps, NoBeneficiariosCaps

from import_export import resources
from import_export.admin import ImportExportModelAdmin

# Register your models here.

class BeneficiariosResource(resources.ModelResource):

    class Meta:
        model = BeneficiariosCaps


class BeneficiariosAdmin(ImportExportModelAdmin):
    resource_class = BeneficiariosResource
    list_display = ('nombre_apellido','jefe_familia','sexo')
    search_fields = ('nombre_apellido','nombre_caps')
    list_filter = ('departamento','municipio','sexo')

admin.site.register(BeneficiariosCaps, BeneficiariosAdmin)
admin.site.register(NoBeneficiariosCaps)
