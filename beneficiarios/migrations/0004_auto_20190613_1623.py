# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-06-13 22:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('actividades', '0006_auto_20190613_1623'),
        ('lugar', '0003_auto_20190201_1156'),
        ('beneficiarios', '0003_nobeneficiarios'),
    ]

    operations = [
        migrations.CreateModel(
            name='BeneficiariosCaps',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_apellido', models.CharField(max_length=250)),
                ('jefe_familia', models.IntegerField(choices=[(1, 'Si'), (2, 'No')])),
                ('sexo', models.IntegerField(choices=[(1, 'Hombre'), (2, 'Mujer')])),
                ('fecha_nacimiento', models.DateField()),
                ('existe_caps', models.IntegerField(blank=True, choices=[(1, 'Si'), (2, 'No')], null=True)),
                ('nombre_caps', models.CharField(blank=True, max_length=250, null=True)),
                ('cargo_caps', models.CharField(blank=True, max_length=250, null=True)),
                ('localizacion_latitude', models.FloatField(blank=True, max_length=250, null=True)),
                ('localizacion_longitude', models.FloatField(blank=True, max_length=250, null=True)),
                ('comunidad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Comunidad')),
                ('departamento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Departamento')),
                ('municipio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Municipio')),
            ],
            options={
                'verbose_name': 'Beneficiario del proyecto',
                'verbose_name_plural': 'Beneficiarios del proyecto',
            },
        ),
        migrations.CreateModel(
            name='NoBeneficiariosCaps',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_apellido', models.CharField(max_length=250)),
                ('jefe_familia', models.IntegerField(choices=[(1, 'Si'), (2, 'No')])),
                ('sexo', models.IntegerField(choices=[(1, 'Hombre'), (2, 'Mujer')])),
                ('fecha_nacimiento', models.CharField(max_length=250)),
                ('existe_caps', models.IntegerField(blank=True, choices=[(1, 'Si'), (2, 'No')], null=True)),
                ('nombre_caps', models.CharField(blank=True, max_length=250, null=True)),
                ('cargo_caps', models.CharField(blank=True, max_length=250, null=True)),
                ('localizacion_latitude', models.FloatField(blank=True, max_length=250, null=True)),
                ('localizacion_longitude', models.FloatField(blank=True, max_length=250, null=True)),
                ('comunidad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Comunidad')),
                ('departamento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Departamento')),
                ('municipio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Municipio')),
            ],
            options={
                'verbose_name': 'No Beneficiario',
                'verbose_name_plural': 'No Beneficiarios',
            },
        ),
        migrations.DeleteModel(
            name='Beneficiarios',
        ),
        migrations.DeleteModel(
            name='NoBeneficiarios',
        ),
    ]
