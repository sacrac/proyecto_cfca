from django.db import models
from lugar.models import Comunidad, Municipio, Departamento
from datetime import date
# Create your models here.

CHOICES_SEXO = (
                    (1,'Hombre'),
                    (2,'Mujer'),
                )

CHOICES_OPCION = (
                    (1,'Si'),
                    (2,'No'),
                )

class BeneficiariosCaps(models.Model):
    nombre_apellido = models.CharField(max_length=250)
    jefe_familia = models.IntegerField(choices=CHOICES_OPCION)
    comunidad = models.ForeignKey(Comunidad)
    municipio = models.ForeignKey(Municipio)
    departamento = models.ForeignKey(Departamento)
    sexo = models.IntegerField(choices=CHOICES_SEXO)
    fecha_nacimiento = models.DateField()
    existe_caps = models.IntegerField(choices=CHOICES_OPCION, null=True, blank=True)
    nombre_caps = models.CharField(max_length=250, null=True, blank=True)
    cargo_caps = models.CharField(max_length=250, null=True, blank=True)
    localizacion_latitude = models.FloatField(max_length=250, null=True, blank=True)
    localizacion_longitude = models.FloatField(max_length=250, null=True, blank=True)
    discapasidad = models.IntegerField(choices=CHOICES_OPCION, null=True, blank=True)

    def __str__(self):
        return self.nombre_apellido

    class Meta:
        verbose_name = 'Beneficiario del proyecto'
        verbose_name_plural = 'Beneficiarios del proyecto'
        ordering = ('nombre_apellido',)

    def edad_persona(self):
        today = date.today()
        try:
            edad = self.fecha_nacimiento.replace(year=today.year)
        except ValueError:
            edad = self.fecha_nacimiento.replace(year=today.year, day=self.fecha_nacimiento.day-1)
        if edad > today:
            return today.year - self.fecha_nacimiento.year - 1
        else:
            return today.year - self.fecha_nacimiento.year

    # def calculate_age(born):
    #     today = date.today()
    #     return today.year - self.fecha_nacimiento.year - ((today.month, today.day) < (self.fecha_nacimiento.month, self.fecha_nacimiento.day))


class NoBeneficiariosCaps(models.Model):
    nombre_apellido = models.CharField(max_length=250)
    jefe_familia = models.IntegerField(choices=CHOICES_OPCION)
    comunidad = models.ForeignKey(Comunidad)
    municipio = models.ForeignKey(Municipio)
    departamento = models.ForeignKey(Departamento)
    sexo = models.IntegerField(choices=CHOICES_SEXO)
    fecha_nacimiento = models.CharField(max_length=250)
    existe_caps = models.IntegerField(choices=CHOICES_OPCION, null=True, blank=True)
    nombre_caps = models.CharField(max_length=250, null=True, blank=True)
    cargo_caps = models.CharField(max_length=250, null=True, blank=True)
    localizacion_latitude = models.FloatField(max_length=250, null=True, blank=True)
    localizacion_longitude = models.FloatField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.nombre_apellido

    class Meta:
        verbose_name = 'No Beneficiario'
        verbose_name_plural = 'No Beneficiarios'
        ordering = ('nombre_apellido',)

