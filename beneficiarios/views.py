from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.db.models import Count, Q, Sum, Value as V, F, Case, When, IntegerField
from django.db.models.functions import Coalesce

from .models import BeneficiariosCaps
from .forms import ConsultarCenso
from lugar.models import Comunidad, Municipio, Departamento
from datosCaps.models import FichaCaps
from actividades.models import EjesTransversales, Actividad

from collections import OrderedDict
# Create your views here.

class BeneficiarioHome(LoginRequiredMixin, TemplateView):
    template_name = 'seguimiento/segimiento_beneficiario.html'

    def get_context_data(self, **kwargs):
        context = super(BeneficiarioHome, self).get_context_data(**kwargs)
        query_actividad = Actividad.objects.select_related()
        query_beneficiario = BeneficiariosCaps.objects.select_related()


        #salidas sobre conteos de personas participando en los modulos
        grafo_actividad = OrderedDict()
        personas_capasitadas = 0
        caps_capasitados = []
        for obj in EjesTransversales.objects.exclude(id=7):
            total = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(total=Sum('numero_participante'))['total'] or 0
            hombres = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(hombres=Sum('numero_hombres'))['hombres'] or 0
            mujeres = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(mujeres=Sum('numero_mujeres'))['mujeres'] or 0
            #caps = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(cap=Count('caps'))['cap'] or 0
            caps = query_actividad.filter(tipo_actividad=19,modulos=obj).values_list('caps__id', flat=True).distinct()
            if total != None and total > 0:
                caps_capasitados.append(len(caps))
                grafo_actividad[obj.nombre] = [total,hombres,mujeres,caps]

        for k,v in grafo_actividad.items():
            personas_capasitadas += v[0]

        context['grafo_actividad'] = grafo_actividad
        context['personas_capasitadas'] = personas_capasitadas
        context['caps_capasitados'] = max(caps_capasitados)
        totales_juntas_directivas = OrderedDict()


        tabla_grafo_municipio = OrderedDict()
        tabla_junta_directiva = OrderedDict()

        for modulo in EjesTransversales.objects.exclude(id=7).order_by('id'):
            total = query_actividad.filter(tipo_actividad=19,modulos=modulo).aggregate(total=Sum('numero_participante'))['total'] or 0
            if total != None and total > 0:
                hombres_jd = 0
                mujeres_jd = 0
                tabla_grafo_municipio[modulo] = OrderedDict()
                tabla_junta_directiva[modulo] = OrderedDict()
                totales_juntas_directivas[modulo] = OrderedDict()
                for obj in Departamento.objects.exclude(id=3):
                    tabla_grafo_municipio[modulo][obj.nombre] = OrderedDict()
                    var_h = 0
                    var_m = 0
                    for muni in Municipio.objects.filter(departamento=obj):
                        presi_h = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_presidencia'),V(0)))['t']
                        presi_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_presidencia_m'),V(0)))['t']
                        tesorero = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_tesorero'),V(0)))['t']
                        tesorero_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_tesorero_m'),V(0)))['t']
                        secretario = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_secretario'),V(0)))['t']
                        secretario_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_secretario_m'),V(0)))['t']
                        vocal = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_vocal'),V(0)))['t']
                        vocal_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_vocal_m'),V(0)))['t']
                        fiscal = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_fiscal'),V(0)))['t']
                        fiscal_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_fiscal_m'),V(0)))['t']
                        apoyo = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_apoyo'),V(0)))['t']
                        apoyo_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_apoyo_m'),V(0)))['t']
                        hombres = presi_h+tesorero+secretario+vocal+fiscal+apoyo
                        mujeres = presi_m+tesorero_m+secretario_m+vocal_m+fiscal_m+apoyo_m

                        tabla_grafo_municipio[modulo][obj.nombre][muni.nombre] = [
                                                                        presi_h,
                                                                        presi_m,
                                                                        tesorero,
                                                                        tesorero_m,
                                                                        secretario,
                                                                        secretario_m,
                                                                        vocal,
                                                                        vocal_m,
                                                                        fiscal,
                                                                        fiscal_m,
                                                                        apoyo,
                                                                        apoyo_m,
                                                                        hombres,
                                                                        mujeres
                                                                    ]
                        hombres_jd += hombres
                        mujeres_jd += mujeres
                        var_h += hombres
                        var_m += mujeres
                    totales_juntas_directivas[modulo]['hombre'] = hombres_jd
                    totales_juntas_directivas[modulo]['mujeres'] = mujeres_jd
                    tabla_junta_directiva[modulo][obj.nombre] = [var_h,var_m]

        #fuera el for
        context['tabla_grafo_municipio'] = tabla_grafo_municipio

        context['tabla_junta_directiva'] = tabla_junta_directiva
        context['totales_juntas_directivas'] = totales_juntas_directivas

        return context

class BeneficiarioInversion(LoginRequiredMixin, TemplateView):
    template_name = 'seguimiento/seguimiento_beneficiario_inversion.html'

    def get_context_data(self, **kwargs):
        context = super(BeneficiarioInversion, self).get_context_data(**kwargs)
        query_actividad = Actividad.objects.select_related()
        query_beneficiario = BeneficiariosCaps.objects.select_related()

        #salidas de las inversiones de perforacion y mejoras
        context['conteo_total_perforacion'] = query_actividad.filter(tipo_actividad=12).count()
        context['conteo_total_mejoras'] = query_actividad.filter(tipo_actividad=13).count()
        years = []
        for en in Actividad.objects.order_by('fecha').values_list('fecha', flat=True):
          years.append((en.year,en.year))

        years_final = list(sorted(set(years)))

        context['comunidades_beneficiadas'] = Comunidad.objects.filter(actividad__modulos__in=[1,2,3,4,5,6]).order_by('nombre').distinct('nombre').count()

        inversiones_anio = OrderedDict()
        for y in years_final:
            ejecutado_perforacion = query_actividad.filter(fecha__year=y[0], tipo_actividad=12).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
            total_perforacion = ( query_actividad
                .filter(fecha__year=y[0], tipo_actividad=12)
                .aggregate(
                    alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                    comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                    otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
                )
             )

            total_perforacion_ong = ( query_actividad
                .filter(fecha__year=y[0], tipo_actividad=12)
                .aggregate(
                    ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0)),
                )
             )
            perforacion = ejecutado_perforacion + total_perforacion['alcaldia'] + total_perforacion['comunidad'] + \
                                    total_perforacion['otros'] + total_perforacion_ong['ong']

            ejecutado_mejora = query_actividad.filter(fecha__year=y[0], tipo_actividad=13).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
            total_mejora = query_actividad.filter(fecha__year=y[0], tipo_actividad=13).aggregate(
                    alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                    comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                    otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),   
                    
                )
            total_mejora_ong = ( query_actividad.filter(fecha__year=y[0], tipo_actividad=13).aggregate(
                ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0)),
                )
            )
             
            mejora = ejecutado_mejora + total_mejora['alcaldia'] + total_mejora['comunidad'] + \
                     total_mejora['otros'] + total_mejora_ong['ong']
            

            inversiones_anio[y[1]] = [perforacion, mejora]

        context['tabla_inversiones'] = inversiones_anio

        total_inveresiones = [sum(x) for x in zip(*inversiones_anio.values())]
        context['grafo_inversion_total'] = sum(total_inveresiones)

        tabla_beneficiario_mejora_perfo = []
        reporte = {}
        for acti in query_actividad.filter(tipo_actividad__in=[12,13]):
            total_personas = query_beneficiario.filter(comunidad=acti.comunidad).count()
            total_hombres = query_beneficiario.filter(comunidad=acti.comunidad, sexo=1).count()
            total_mujeres = query_beneficiario.filter(comunidad=acti.comunidad, sexo=2).count()
            menores_h = [obj for obj in query_beneficiario.filter(comunidad=acti.comunidad,sexo=1) if obj.edad_persona() <= 15]
            menores_m = [obj for obj in query_beneficiario.filter(comunidad=acti.comunidad,sexo=2) if obj.edad_persona() <= 15]
            tabla_beneficiario_mejora_perfo.append([total_personas,total_hombres,total_mujeres,len(menores_h),len(menores_m)])
            alcaldia = 0
            enacal = 0
            comunidad = 0
            ong = 0
            for objecto in acti.presupuestoinfraestructura_set.all():
                #print("Presupuesto")
                alcaldia = objecto.alcaldia
                comunidad = objecto.comunidad
                enacal = objecto.otros
                #print(dir(objecto))
            for dinero in acti.financiamientoinstituciones_set.all():
                ong = dinero.total
            reporte[acti.actividad] = [acti.comunidad,acti.fecha.year,acti.tipo_actividad,
                acti.presupuesto,acti.ejecutado,alcaldia,enacal,comunidad,ong,acti.get_estado_display()]

        context['total_bene_comunidad'] = [sum(x) for x in zip(*tabla_beneficiario_mejora_perfo)]
        context['reporte'] = reporte
        #Inversiones por munucipio y por año
        tabla_inversiones_municipio = OrderedDict()
        tabla_inversiones_totales = {}
        for y in years_final:
            tabla_inversiones_municipio[y[1]] = OrderedDict()
            lista_monto = []
            for muni in Municipio.objects.exclude(id=7):
                ejecutado_perforacion = query_actividad.filter(fecha__year=y[0],
                                                               tipo_actividad=12,
                                                               municipio=muni).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
                total_perforacion = ( query_actividad
                    .filter(fecha__year=y[0], tipo_actividad=12, municipio=muni)
                    .aggregate(
                        alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                        comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                        otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
                    )
                 )
                total_perforacion2 = ( query_actividad
                    .filter(fecha__year=y[0], tipo_actividad=12, municipio=muni)
                    .aggregate(
                        ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0))
                    )
                 )
                perforacion = ejecutado_perforacion + total_perforacion['alcaldia'] + total_perforacion['comunidad'] + \
                                        total_perforacion['otros'] + total_perforacion2['ong']

                ejecutado_mejora = query_actividad.filter(fecha__year=y[0],
                                                          tipo_actividad=13,
                                                          municipio=muni).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
                total_mejora = ( query_actividad
                    .filter(fecha__year=y[0], tipo_actividad=13, municipio=muni)
                    .aggregate(
                        alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                        comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                        otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
                    )
                 )
                total_mejora2 = ( query_actividad
                    .filter(fecha__year=y[0], tipo_actividad=13, municipio=muni)
                    .aggregate(
                        ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0))
                    )
                 )
                mejora = ejecutado_mejora + total_mejora['alcaldia'] + total_mejora['comunidad'] + \
                                    total_mejora['otros'] + total_mejora2['ong']
                if perforacion or mejora:
                    lista_monto = [(k, total_perforacion[k], v) for k, v in total_mejora.items() if v > 0]
                    tabla_inversiones_municipio[y[1]][muni.nombre] = [perforacion, mejora,
                                                                      ejecutado_perforacion,
                                                                      ejecutado_mejora,lista_monto]

        for y in years_final:
            total_perforacion_anio = query_actividad.filter(fecha__year=y[0],tipo_actividad=12).count()
            total_mejoras_anio = query_actividad.filter(fecha__year=y[0],tipo_actividad=13).count()
            tabla_inversiones_totales[y[1]] = [total_perforacion_anio,total_mejoras_anio]

        context['tabla_inversiones_municipio'] = tabla_inversiones_municipio
        context['tabla_inversiones_totales'] = tabla_inversiones_totales

        return context

class BeneficiarioActividad(LoginRequiredMixin, TemplateView):
    template_name = 'seguimiento/seguimiento_beneficiario_actividad.html'

    def get_context_data(self, **kwargs):
        context = super(BeneficiarioActividad, self).get_context_data(**kwargs)
        query_actividad = Actividad.objects.select_related()
        query_beneficiario = BeneficiariosCaps.objects.select_related()

        total_participante = query_actividad.filter(tipo_actividad=8,modulos=7).aggregate(t=Coalesce(Sum('numero_participante'),V(0)))['t']
        apoyo_h = query_actividad.filter(tipo_actividad=8,modulos=7).aggregate(t=Coalesce(Sum('numero_apoyo'),V(0)))['t']
        apoyo_m = query_actividad.filter(tipo_actividad=8,modulos=7).aggregate(t=Coalesce(Sum('numero_apoyo_m'),V(0)))['t']

        context['personas_participantes'] = total_participante
        context['personas_apoyo'] = apoyo_h + apoyo_m

        tabla_grafo_municipio = OrderedDict()
        for obj in Departamento.objects.exclude(id=3):
            tabla_grafo_municipio[obj.nombre] = OrderedDict()
            for muni in Municipio.objects.filter(departamento=obj):
                presi_h = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_presidencia'),V(0)))['t']
                presi_m = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_presidencia_m'),V(0)))['t']
                tesorero = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_tesorero'),V(0)))['t']
                tesorero_m = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_tesorero_m'),V(0)))['t']
                secretario = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_secretario'),V(0)))['t']
                secretario_m = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_secretario_m'),V(0)))['t']
                vocal = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_vocal'),V(0)))['t']
                vocal_m = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_vocal_m'),V(0)))['t']
                fiscal = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_fiscal'),V(0)))['t']
                fiscal_m = query_actividad.filter(tipo_actividad=8,modulos=7,municipio=muni).aggregate(t=Coalesce(Sum('numero_fiscal_m'),V(0)))['t']
                hombres = presi_h+tesorero+secretario+vocal+fiscal
                mujeres = presi_m+tesorero_m+secretario_m+vocal_m+fiscal_m
                tabla_grafo_municipio[obj.nombre][muni.nombre] = [
                                                                presi_h,
                                                                presi_m,
                                                                tesorero,
                                                                tesorero_m,
                                                                secretario,
                                                                secretario_m,
                                                                vocal,
                                                                vocal_m,
                                                                fiscal,
                                                                fiscal_m,
                                                                hombres,
                                                                mujeres
                                                            ]
        context['tabla_grafo_municipio'] = tabla_grafo_municipio

        tabla_totales_jd = OrderedDict()
        for k,v in tabla_grafo_municipio.items():
            a = []
            for key,value in v.items():
                a.append(value)
                tabla_totales_jd[k]= a

        tabla_junta_directiva = OrderedDict()
        for k,v in tabla_totales_jd.items():
            tabla_junta_directiva[k] = [sum(x) for x in zip(*v)]

        context['tabla_junta_directiva'] = tabla_junta_directiva


        return context

@login_required
def reporte_mejoras_perforacion(request, year=None, template='seguimiento/tabla_inversiones.html'):
    reporte = {}
    anio = year
    for obj in Actividad.objects.filter(fecha__year=year,tipo_actividad__in=[12,13]).exclude(tema__id=7).select_related():
        #print(dir(obj))
        alcaldia = 0
        enacal = 0
        comunidad = 0
        ong = 0
        lista_ong = []
        for objecto in obj.presupuestoinfraestructura_set.all():
            #print("Presupuesto")
            if objecto.alcaldia is not None:
                alcaldia = objecto.alcaldia
            else:
                alcaldia = 0
            if objecto.comunidad is not None:
                comunidad = objecto.comunidad
            else:
                comunidad = 0
            if objecto.otros is not None:
                enacal = objecto.otros
            else:
                enacal = 0
            #print(objecto.otros)
        for dinero in obj.financiamientoinstituciones_set.all():
            if dinero.monto is not None:
                ong += dinero.monto
                lista_ong.append([dinero.institucion.nombre, dinero.institucion.logo])
            else:
                ong = 0

        total = obj.ejecutado + alcaldia + comunidad + enacal + ong

        reporte[obj.actividad] = [obj.comunidad,obj.fecha.year,obj.tipo_actividad,
                obj.presupuesto,obj.ejecutado,alcaldia,enacal,comunidad,ong,
                total,obj.get_estado_display(),lista_ong]

    total_simas = sum([v[4] for k,v in reporte.items()])
    total_alcaldia = sum([v[5] for k,v in reporte.items()])
    total_enacal = sum([v[6] for k,v in reporte.items()])
    total_comunidad = sum([v[7] for k,v in reporte.items()])
    total_ong = sum([v[8] for k,v in reporte.items()])
    gran_total = sum([v[9] for k,v in reporte.items()])


    return render(request, template, locals())


def mejoras_perforacion_personas(request,template='seguimiento/tabla_personas.html'):
    reporte = {}
    lista_comunidad = []
    total = 0
    hombres = 0
    mujeres = 0
    menores_hombres = 0
    menores_mujeres = 0
    for obj in Actividad.objects.filter(tipo_actividad__in=[12,13]).select_related():
        total_personas = BeneficiariosCaps.objects.filter(comunidad=obj.comunidad).count()
        total_hombres = BeneficiariosCaps.objects.filter(comunidad=obj.comunidad, sexo=1).count()
        total_mujeres = BeneficiariosCaps.objects.filter(comunidad=obj.comunidad, sexo=2).count()
        menores_h = [obj1 for obj1 in BeneficiariosCaps.objects.filter(comunidad=obj.comunidad,sexo=1) if obj1.edad_persona() <= 15]
        menores_m = [obj1 for obj1 in BeneficiariosCaps.objects.filter(comunidad=obj.comunidad,sexo=2) if obj1.edad_persona() <= 15]
        total += total_personas
        hombres += total_hombres
        mujeres += total_mujeres

        reporte[obj] =  [obj.tipo_actividad,obj.comunidad,obj.fecha,total_personas,total_hombres,
                                  total_mujeres,len(menores_h),len(menores_m)]

    return render(request, template, locals())



#salidas con filtros para el censo

def _queryset_filtrado_censo(request):
    params = {}
    # if 'departamento' in request.session:
    #     params['departamento__in'] = request.session['departamento']

    if 'municipio' in request.session:
        params['municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['comunidad__in'] = request.session['comunidad']


    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print(params)

    return BeneficiariosCaps.objects.filter(**params)


def consulta_censo(request, template='filtros/consultar_censo.html'):
    if request.method == 'POST':
        form = ConsultarCenso(request.POST)
        if form.is_valid():
            # request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
           
            centinela = 1
            print("fue valido")
        else:
            centinela = 0
            print("No fue valido")
    else:
        form = ConsultarCenso()
        centinela = 0

        if 'municipio' in request.session:
            try:
                del request.session['municipio']
                del request.session['comunidad']
            except:
                pass
    
    datos = BeneficiariosCaps.objects.aggregate(
        total=Count('id'),
        hombre=Sum(
         Case(When(sexo=1, then=1),
              output_field=IntegerField())
        ),
        mujeres=Sum(
            Case(When(sexo=2, then=1),
                output_field=IntegerField())
        ),

    )
    filtered_child_h = [x for x in BeneficiariosCaps.objects.filter(sexo=1) if x.edad_persona() <=15]
    filtered_child_m = [x for x in BeneficiariosCaps.objects.filter(sexo=2) if x.edad_persona() <=15]

    return render(request, template, {'form': form, 'centinela': centinela,
                                    'dato':datos,'ninos':len(filtered_child_h),
                                    'ninas': len(filtered_child_m)})

def salidas_filtro_censo(request, template="filtros/filtro_censo.html"):
    filtro2 = _queryset_filtrado_censo(request)
    filtro = filtro2.select_related('comunidad','municipio')
    return render(request,template,{'filtro':filtro,'conteo':filtro.count()})