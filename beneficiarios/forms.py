
from django import forms
from lugar.models import *

from dal import autocomplete

class ConsultarCenso(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ConsultarCenso, self).__init__(*args, **kwargs)
          # self.fields['departamento'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
          #                                           'data-placeholder':'Escoge departamento', 'tabindex':'-1','area-hidden': 'true'})
          # self.fields['municipio'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
          #                                           'data-placeholder':'Escoge municipio', 'tabindex':'-1','area-hidden': 'true'})
     
          self.fields['comunidad'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
                                                    'data-placeholder':'Escoge comunidad', 'tabindex':'-1','area-hidden': 'true'})

          if 'departamento' in self.data:
            try:
                pais_id = int(self.data.get('departamento'))
                self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass

          if 'municipio' in self.data:
            try:
                departamento_id = int(self.data.get('municipio'))
                self.fields['comunidad'].queryset = Comunidad.objects.filter(municipio_id=departamento_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from

          
    
    # departamento = forms.ModelMultipleChoiceField(queryset=Departamento.objects.all(),
    #                                               required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.all(), 
                required=False,
                )
    comunidad = forms.ModelMultipleChoiceField(queryset=Comunidad.objects.none(),
                required=False,
                widget=autocomplete.ModelSelect2Multiple(url='comu-censo-filtro-autocomplete',forward=['municipio'],attrs={'data-language': 'es','data-placeholder':'Comunidades'}))