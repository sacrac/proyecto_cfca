from django.conf.urls import url


from . import views


urlpatterns = [
	 url(
        r'^filtros-censo/$',
        views.consulta_censo,
        name='consultar-censo'
    ),
    url(
        r'^salida-censo/$',
        views.salidas_filtro_censo,
        name='salida_censo'
    ),

 ]