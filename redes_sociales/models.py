from django.db import models
import datetime
# Create your models here.

ANIOS = []
d=0
for i in range(datetime.date.today().year,2017,-1):
    ANIOS.append((i,str(i)))

CHOICES_MESES = (
        (1,'Enero'),
        (2,'Febrero'),
        (3,'Marzo'),
        (4,'Abril'),
        (5,'Mayo'),
        (6,'Junio'),
        (7,'Julio'),
        (8,'Agosto'),
        (9,'Septiembre'),
        (10,'Octubre'),
        (11,'Noviembre'),
        (12,'Diciembre'),
    )

class RedesSociales(models.Model):
    year = models.IntegerField(choices=ANIOS, verbose_name='Año')
    class Meta:
        verbose_name = "Redes Sociales"
        verbose_name_plural = "Redes Sociales"

    def __str__(self):
        return str(self.year)

class RedesSocialesMeses(models.Model):
    redes = models.ForeignKey(RedesSociales, on_delete=models.CASCADE)
    mes = models.IntegerField(choices=CHOICES_MESES)
    fb_interacion = models.IntegerField()
    fb_seguidores = models.IntegerField()
    fb_alcance = models.IntegerField()
    ig_interacion = models.IntegerField()
    ig_seguidores = models.IntegerField()
    ig_alcance = models.IntegerField()
    tw_interacion = models.IntegerField()
    tw_seguidores = models.IntegerField()
    tw_impresiones = models.IntegerField()
    class Meta:
        verbose_name = "Redes Sociales Meses"
        verbose_name_plural = "Redes Sociales Meses"

    def __str__(self):
        return 'meses'

class CampanasRedes(models.Model):
    nombre = models.CharField(max_length=250)
    fecha_inicio = models.DateField()
    fecha_finalizacion = models.DateField()
    alcance_fb = models.IntegerField()
    alcance_ig = models.IntegerField()
    alcance_tw = models.IntegerField()
    interaccion_fb = models.IntegerField()
    interaccion_ig = models.IntegerField()
    interaccion_tw = models.IntegerField()
    publicaciones_fb = models.IntegerField()
    publicaciones_ig = models.IntegerField()
    publicaciones_tw = models.IntegerField()
    sitio_web_notas = models.IntegerField()
    sitio_web_vistas = models.IntegerField()

    class Meta:
        verbose_name = "Campañas Redes"
        verbose_name_plural = "Campañas Redes"

    def __str__(self):
        return self.nombre

class SitioWebAnalytic(models.Model):
    year = models.IntegerField(choices=ANIOS, verbose_name='Año')
    class Meta:
        verbose_name = "Sitio Web Estadisticas"
        verbose_name_plural = "Sitio Web Estadisticas"

    def __str__(self):
        return str(self.year)

class EstadisticasWeb(models.Model):
    sitio = models.ForeignKey(SitioWebAnalytic, on_delete=models.CASCADE)
    mes = models.IntegerField(choices=CHOICES_MESES)
    paginas_vistas = models.IntegerField()
    usarios = models.IntegerField()
    duracion = models.CharField(max_length=20)
    usuario_femenino = models.IntegerField()
    por_femenino = models.FloatField(verbose_name='% femenino')
    usuario_masculino = models.IntegerField()
    por_masculino = models.FloatField(verbose_name='% masculino')
    class Meta:
        verbose_name = "Estadistica"
        verbose_name_plural = "Estadisticas"

    def __str__(self):
        return 'estadisticas'
    
    
    
    