from django.conf.urls import url, include
from .views import (home_redes,campanas_redes,
                    estadistica_web
                    )

urlpatterns = [
    url(
        r'^home/$',
        home_redes,
        name='redes-home',
    ),
    url(
        r'^campaing/$',
        campanas_redes,
        name='campanas_redes'
    ),
    url(
        r'^estadisticas/$',
        estadistica_web,
        name='estadistica_web'
    ),

]