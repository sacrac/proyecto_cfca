from django.shortcuts import render

from .models import *
import datetime
from collections import OrderedDict

# Create your views here.
def home_redes(request, template='redes/home.html'):

    filtered = []
    var_meses = request.GET.getlist('mes')
    
    if len(var_meses) <= 0:
        filtered = CHOICES_MESES
    else:
        for idk,idv in CHOICES_MESES:
            if str(idk) in var_meses:
                filtered.append((idk,idv))

    anios = []
    for x in RedesSociales.objects.all().values_list('year', flat=True):
        anios.append(x)

    query_redes = RedesSocialesMeses.objects.select_related('redes')

    tabla_data = {}
    for obj in anios:
        tabla_data[str(obj)] = {}
        for meses in filtered:
            campos = query_redes.filter(redes__year=obj,
                mes=meses[0]).values_list('fb_interacion','fb_seguidores','fb_alcance',
                'ig_interacion','ig_seguidores','ig_alcance','tw_interacion',
                'tw_seguidores','tw_impresiones')
            if campos:
                tabla_data[str(obj)][meses[1]] = campos

    anualidades = OrderedDict()
    for obj in anios:
        fb_interac = sum([x[0][0] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        fb_follow = [x[0][1] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()]
        fb_alcance = sum([x[0][2] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])

        ig_interac = sum([x[0][3] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        ig_follow = [x[0][4] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()]
        ig_alcance = sum([x[0][5] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])

        tw_interac = sum([x[0][6] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        tw_follow = [x[0][7] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()]
        tw_alcance = sum([x[0][8] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        anualidades[str(obj)] = [fb_interac,fb_follow,fb_alcance,
                                ig_interac,ig_follow,ig_alcance,
                                tw_interac,tw_follow,tw_alcance]
    
    
    return render(request, template, locals())

def campanas_redes(request, template='redes/campanas.html'):

    tabla_data = CampanasRedes.objects.all()
            
    return render(request, template, locals())


def estadistica_web(request, template='redes/estadisticas.html'):

    filtered = []
    var_meses = request.GET.getlist('mes')
    
    if len(var_meses) <= 0:
        filtered = CHOICES_MESES
    else:
        for idk,idv in CHOICES_MESES:
            if str(idk) in var_meses:
                filtered.append((idk,idv))

    anios = []
    for x in SitioWebAnalytic.objects.all().values_list('year', flat=True):
        anios.append(x)

    query_redes = EstadisticasWeb.objects.select_related('sitio')

    tabla_data = {}
    for obj in anios:
        tabla_data[str(obj)] = {}
        for meses in filtered:
            campos = query_redes.filter(sitio__year=obj,
                mes=meses[0]).values_list('paginas_vistas','usarios','duracion',
                'usuario_femenino','por_femenino','usuario_masculino','por_masculino',
                )
            if campos:
                tabla_data[str(obj)][meses[1]] = campos

    anualidades = OrderedDict()
    for obj in anios:
        duracion_sum = datetime.timedelta()
        paginas = sum([x[0][0] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        usuarios = sum([x[0][1] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        #duracion_sum = sum([x[0][2] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        for k,v in tabla_data.items():
            if str(k) == str(obj):
                for i in v.values():
                    (h, m, s) = i[0][2].split(':')
                    d = datetime.timedelta(hours=int(h),minutes=int(m),seconds=int(s))
                    duracion_sum += d
        femenino = sum([x[0][3] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        por_femenino = sum([x[0][4] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        masculino = sum([x[0][5] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])

        por_masculino = sum([x[0][6] for k, v in tabla_data.items() if str(k) == str(obj) for x in v.values()])
        
        anualidades[str(obj)] = [paginas,usuarios,duracion_sum,
                                femenino,por_femenino,masculino,
                                por_masculino]
    
    return render(request, template, locals())