from django.contrib import admin
from .models import *

class InlineRedesMeses(admin.TabularInline):
    model = RedesSocialesMeses
    extra = 1
    max_num = 12

class InlineEstadisticasMeses(admin.TabularInline):
    model = EstadisticasWeb
    extra = 1
    max_num = 12

class RedesAdmin(admin.ModelAdmin):
    inlines = [InlineRedesMeses]

class CampanasAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('nombre', 'fecha_inicio', 'fecha_finalizacion')
        }),
        ('Campaña Alcance', {
            'classes': ('wide',),
            'fields': ('alcance_fb', 'alcance_ig','alcance_tw'),
        }),
        ('Campaña Interacción', {
            'classes': ('wide',),
            'fields': ('interaccion_fb', 'interaccion_ig','interaccion_tw'),
        }),
        ('Cantidad de publicaciones', {
            'classes': ('wide',),
            'fields': ('publicaciones_fb', 'publicaciones_ig','publicaciones_tw'),
        }),
        ('Sitio Web', {
            'classes': ('wide',),
            'fields': ('sitio_web_notas', 'sitio_web_vistas'),
        }),
    )

class SitioWebEstadisticaAdmin(admin.ModelAdmin):
    inlines = [InlineEstadisticasMeses]

# Register your models here.
admin.site.register(RedesSociales, RedesAdmin)
admin.site.register(CampanasRedes, CampanasAdmin)
admin.site.register(SitioWebAnalytic, SitioWebEstadisticaAdmin)