# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2020-11-04 16:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redes_sociales', '0002_auto_20201103_1051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estadisticasweb',
            name='por_femenino',
            field=models.FloatField(verbose_name='% femenino'),
        ),
        migrations.AlterField(
            model_name='estadisticasweb',
            name='por_masculino',
            field=models.FloatField(verbose_name='% masculino'),
        ),
    ]
