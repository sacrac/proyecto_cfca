from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.db.models import Count, Q, Sum, Value as V, F
from django.db.models.functions import Coalesce

from collections import OrderedDict

from actividades.models import Actividad
from actividadgerencial.models import (ActividadGerencial,
                                        ActividadPrincipal,
                                        CHOICES_TRIMESTRE,
                                        SubActividades,
                                        MetasAlcanzadas,
                                        Years
                                        )
from actividadgerencial.forms import ConsultaForm, ConsultaActividades
# Create your views here.


class ActvidadesProgramatica(LoginRequiredMixin, FormView):
    template_name = 'seguimiento/segimiento_actividades.html'
    form_class = ConsultaActividades
    success_url = '/actividades/'
    #plus_context = dict()

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        #print("se envio el form puto")
        #print(form.cleaned_data['year'])
        #self.plus_context['year'] = form.cleaned_data['year']
        self.request.session['year'] = form.cleaned_data['year']
        return super(ActvidadesProgramatica, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ActvidadesProgramatica, self).get_context_data(**kwargs)
        #lista_excluidos = [6,7,15,19,20,21,22,25,26,27,30,31,33,35,37,40,41,45,48,49,51]
        query_actividad = Actividad.objects.select_related()
        query_actividadGerencial = ActividadGerencial.objects.select_related().order_by('actividad')
        query_metas = MetasAlcanzadas.objects.select_related('actividad_gerencial')

        #[1,2,10,11,12,23,28,29,32,36,38,43,44,46,50]
        lista_conteo_id = list(SubActividades.objects.filter(clasificacion=1).values_list('id',flat=True))
         #[4,16,34,39,42]
        lista_suma_participantes = list(SubActividades.objects.filter(clasificacion=2).values_list('id',flat=True))
        #[3,5,8,9,13,17,18,24,47]
        lista_junta_directiva = list(SubActividades.objects.filter(clasificacion=3).values_list('id',flat=True))

        lista_productos= list(SubActividades.objects.filter(clasificacion=4).values_list('id',flat=True))
        lista_conteo_caps = list(SubActividades.objects.filter(clasificacion=5).values_list('id',flat=True))

        #print("desde el context data")
        try:
            var_year = self.request.session['year']
        except:
            var_year = 2019
        #context['anio'] = self.plus_context

        tabla_programatica = OrderedDict()
        if var_year:
            for obj in query_actividadGerencial:
                if obj.actividad_id in lista_conteo_caps:
                    print("conteo caps")
                    t1_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t1_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Count('id'), V(0)))['t']
                    t2_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t2_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Count('id'), V(0)))['t']
                    t3_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t3_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Count('id'), V(0)))['t']
                    t4_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t4_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Count('id'), V(0)))['t']

                    if query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre__in=[1,2,3,4],anio=var_year).count() > 0:
                        try:
                            porcentaje = ((t1_alcanzada + t2_alcanzada + t3_alcanzada + t4_alcanzada)/(t1_meta + t2_meta + t3_meta + t4_meta))*100
                        except:
                            porcentaje = 0
                        tabla_programatica[obj.actividad.nombre] = [t1_meta, t1_alcanzada,
                                                                t2_meta, t2_alcanzada,
                                                                t3_meta, t3_alcanzada,
                                                                t4_meta, t4_alcanzada,
                                                                porcentaje
                                                                ]

                if obj.actividad_id in lista_productos:
                    t1_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t1_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']
                    t2_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t2_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']
                    t3_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t3_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']
                    t4_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t4_alcanzada = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']

                    if query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre__in=[1,2,3,4],anio=var_year).count() > 0:
                        try:
                            porcentaje = ((t1_alcanzada + t2_alcanzada + t3_alcanzada + t4_alcanzada)/(t1_meta + t2_meta + t3_meta + t4_meta))*100
                        except:
                            porcentaje = 0
                        tabla_programatica[obj.actividad.nombre] = [t1_meta, t1_alcanzada,
                                                                t2_meta, t2_alcanzada,
                                                                t3_meta, t3_alcanzada,
                                                                t4_meta, t4_alcanzada,
                                                                porcentaje
                                                                ]

                if obj.actividad_id in lista_conteo_id:
                    #print("entro en conteo")
                    t1_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t1_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=1,fecha__year=str(var_year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']
                    t2_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t2_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=2,fecha__year=str(var_year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']
                    t3_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t3_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=3,fecha__year=str(var_year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']
                    t4_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t4_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=4,fecha__year=str(var_year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']

                    if query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre__in=[1,2,3,4],anio=var_year).count() > 0:
                        try:
                            porcentaje = ((t1_alcanzada + t2_alcanzada + t3_alcanzada + t4_alcanzada)/(t1_meta + t2_meta + t3_meta + t4_meta))*100
                        except:
                            porcentaje = 0
                        tabla_programatica[obj.actividad.nombre] = [t1_meta, t1_alcanzada,
                                                                t2_meta, t2_alcanzada,
                                                                t3_meta, t3_alcanzada,
                                                                t4_meta, t4_alcanzada,
                                                                porcentaje
                                                                ]
                elif obj.actividad_id in lista_suma_participantes:
                    #print("entro a sumas de participantes")
                    t1_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t1_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=1,fecha__year=str(var_year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']
                    t2_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t2_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=2,fecha__year=str(var_year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']
                    t3_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t3_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=3,fecha__year=str(var_year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']
                    t4_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t4_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=4,fecha__year=str(var_year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']

                    if query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre__in=[1,2,3,4],anio=var_year).count() > 0:
                        try:
                            porcentaje = ((t1_alcanzada + t2_alcanzada + t3_alcanzada + t4_alcanzada)/(t1_meta + t2_meta + t3_meta + t4_meta))*100
                        except:
                            porcentaje = 0
                        tabla_programatica[obj.actividad.nombre] = [t1_meta, t1_alcanzada,
                                                                        t2_meta, t2_alcanzada,
                                                                        t3_meta, t3_alcanzada,
                                                                        t4_meta, t4_alcanzada,
                                                                        porcentaje
                                                                        ]
                elif  obj.actividad_id in lista_junta_directiva:
                    #print("entro  juntas directivas'")
                    t1_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=1,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t1_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=1,fecha__year=str(var_year)).aggregate(
                                                    t=Coalesce(
                                                        Sum(
                                                            F('numero_presidencia') +
                                                            F('numero_presidencia_m') +
                                                            F('numero_tesorero') +
                                                            F('numero_tesorero_m') +
                                                            F('numero_secretario') +
                                                            F('numero_secretario_m') +
                                                            F('numero_vocal') +
                                                            F('numero_vocal_m') +
                                                            F('numero_fiscal') +
                                                            F('numero_fiscal_m')), V(0))
                                                    )['t']

                    t2_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=2,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t2_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=2,fecha__year=str(var_year)).aggregate(
                                                    t=Coalesce(
                                                        Sum(
                                                            F('numero_presidencia') +
                                                            F('numero_presidencia_m') +
                                                            F('numero_tesorero') +
                                                            F('numero_tesorero_m') +
                                                            F('numero_secretario') +
                                                            F('numero_secretario_m') +
                                                            F('numero_vocal') +
                                                            F('numero_vocal_m') +
                                                            F('numero_fiscal') +
                                                            F('numero_fiscal_m')), V(0))
                                                    )['t']
                    t3_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=3,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t3_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=3,fecha__year=str(var_year)).aggregate(
                                                    t=Coalesce(
                                                        Sum(
                                                            F('numero_presidencia') +
                                                            F('numero_presidencia_m') +
                                                            F('numero_tesorero') +
                                                            F('numero_tesorero_m') +
                                                            F('numero_secretario') +
                                                            F('numero_secretario_m') +
                                                            F('numero_vocal') +
                                                            F('numero_vocal_m') +
                                                            F('numero_fiscal') +
                                                            F('numero_fiscal_m')), V(0))
                                                    )['t']
                    t4_meta = query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre=4,anio=var_year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
                    t4_alcanzada = query_actividad.filter(sub_actividad=obj.actividad,trimestre=4,fecha__year=str(var_year)).aggregate(
                                                    t=Coalesce(
                                                        Sum(
                                                            F('numero_presidencia') +
                                                            F('numero_presidencia_m') +
                                                            F('numero_tesorero') +
                                                            F('numero_tesorero_m') +
                                                            F('numero_secretario') +
                                                            F('numero_secretario_m') +
                                                            F('numero_vocal') +
                                                            F('numero_vocal_m') +
                                                            F('numero_fiscal') +
                                                            F('numero_fiscal_m')), V(0))
                                                    )['t']
                    if query_metas.filter(actividad_gerencial__actividad=obj.actividad,trimestre__in=[1,2,3,4],anio=var_year).count() > 0:
                        try:
                            porcentaje = ((t1_alcanzada + t2_alcanzada + t3_alcanzada + t4_alcanzada)/(t1_meta + t2_meta + t3_meta + t4_meta))*100
                        except:
                            porcentaje = 0
                        tabla_programatica[obj.actividad.nombre] = [t1_meta, t1_alcanzada,
                                                                        t2_meta, t2_alcanzada,
                                                                        t3_meta, t3_alcanzada,
                                                                        t4_meta, t4_alcanzada,
                                                                        porcentaje
                                                                        ]
                else:
                    print("nada pasa")

        #print(tabla_programatica)
        context['tabla'] = tabla_programatica

        return context


@login_required
def ResultadosView(request, template='seguimiento/segimiento_resultado.html'):
    if request.method == 'POST':
        form = ConsultaForm(request.POST)
        if form.is_valid():
            request.session['year'] = form.cleaned_data['year']
            request.session['subactividad'] = form.cleaned_data['subactividad']
            centinela = 1
    else:
        form = ConsultaForm()
        centinela = 0


    tabla_programatica = OrderedDict()

    if centinela:
        year = request.session['year']
        nombre = request.session['subactividad']
        subactividad = request.session['subactividad'].id
        query_actividad = Actividad.objects.select_related()
        query_actividadGerencial = ActividadGerencial.objects.select_related()
        query_metas = MetasAlcanzadas.objects.select_related('actividad_gerencial')

        #[1,2,10,11,12,23,28,29,32,36,38,43,44,46,50]
        lista_conteo_id = list(SubActividades.objects.filter(clasificacion=1).values_list('id',flat=True))
         #[4,16,34,39,42]
        lista_suma_participantes = list(SubActividades.objects.filter(clasificacion=2).values_list('id',flat=True))
        #[3,5,8,9,13,17,18,24,47]
        lista_junta_directiva = list(SubActividades.objects.filter(clasificacion=3).values_list('id',flat=True))

        lista_productos= list(SubActividades.objects.filter(clasificacion=4).values_list('id',flat=True))
        lista_conteo_caps = list(SubActividades.objects.filter(clasificacion=5).values_list('id',flat=True))

        if subactividad in lista_conteo_caps:
            print("conteo caps")
            t1_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t1_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Count('id'), V(0)))['t']
            t2_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t2_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Count('id'), V(0)))['t']
            t3_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t3_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Count('id'), V(0)))['t']
            t4_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t4_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Count('id'), V(0)))['t']

            if query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre__in=[1,2,3,4],anio=year).count() > 0:
                tabla_programatica[nombre] = [t1_meta, t1_alcanzada,
                                            t2_meta, t2_alcanzada,
                                            t3_meta, t3_alcanzada,
                                            t4_meta, t4_alcanzada
                                            ]

        if subactividad in lista_productos:
            print("lista de productos")
            t1_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t1_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']
            t2_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t2_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']
            t3_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t3_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']
            t4_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t4_alcanzada = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Sum('meta_alcanzada'), V(0)))['t']

            if query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre__in=[1,2,3,4],anio=year).count() > 0:
                tabla_programatica[nombre] = [t1_meta, t1_alcanzada,
                                                        t2_meta, t2_alcanzada,
                                                        t3_meta, t3_alcanzada,
                                                        t4_meta, t4_alcanzada
                                                      ]

        if subactividad in lista_conteo_id:
            print("entro en conteo")
            t1_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t1_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=1,fecha__year=str(year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']
            t2_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t2_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=2,fecha__year=str(year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']
            t3_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t3_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=3,fecha__year=str(year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']
            t4_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t4_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=4,fecha__year=str(year)).aggregate(t=Coalesce(Count('id'), V(0)))['t']

            if query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre__in=[1,2,3,4],anio=year).count() > 0:
                tabla_programatica[nombre] = [t1_meta, t1_alcanzada,
                                                        t2_meta, t2_alcanzada,
                                                        t3_meta, t3_alcanzada,
                                                        t4_meta, t4_alcanzada
                                                        ]
        if subactividad in lista_suma_participantes:
            print("entro a sumas de participantes")
            t1_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t1_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=1,fecha__year=str(year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']
            t2_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t2_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=2,fecha__year=str(year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']
            t3_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t3_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=3,fecha__year=str(year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']
            t4_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t4_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=4,fecha__year=str(year)).aggregate(t=Coalesce(Sum('numero_participante'), V(0)))['t']

            if query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre__in=[1,2,3,4],anio=year).count() > 0:
                tabla_programatica[nombre] = [t1_meta, t1_alcanzada,
                                                                t2_meta, t2_alcanzada,
                                                                t3_meta, t3_alcanzada,
                                                                t4_meta, t4_alcanzada
                                                                ]
        if  subactividad in lista_junta_directiva:
            print("entro  juntas directivas'")
            t1_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=1,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t1_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=1,fecha__year=str(year)).aggregate(
                                            t=Coalesce(
                                                Sum(
                                                    F('numero_presidencia') +
                                                    F('numero_presidencia_m') +
                                                    F('numero_tesorero') +
                                                    F('numero_tesorero_m') +
                                                    F('numero_secretario') +
                                                    F('numero_secretario_m') +
                                                    F('numero_vocal') +
                                                    F('numero_vocal_m') +
                                                    F('numero_fiscal') +
                                                    F('numero_fiscal_m')), V(0))
                                            )['t']

            t2_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=2,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t2_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=2,fecha__year=str(year)).aggregate(
                                            t=Coalesce(
                                                Sum(
                                                    F('numero_presidencia') +
                                                    F('numero_presidencia_m') +
                                                    F('numero_tesorero') +
                                                    F('numero_tesorero_m') +
                                                    F('numero_secretario') +
                                                    F('numero_secretario_m') +
                                                    F('numero_vocal') +
                                                    F('numero_vocal_m') +
                                                    F('numero_fiscal') +
                                                    F('numero_fiscal_m')), V(0))
                                            )['t']
            t3_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=3,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t3_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=3,fecha__year=str(year)).aggregate(
                                            t=Coalesce(
                                                Sum(
                                                    F('numero_presidencia') +
                                                    F('numero_presidencia_m') +
                                                    F('numero_tesorero') +
                                                    F('numero_tesorero_m') +
                                                    F('numero_secretario') +
                                                    F('numero_secretario_m') +
                                                    F('numero_vocal') +
                                                    F('numero_vocal_m') +
                                                    F('numero_fiscal') +
                                                    F('numero_fiscal_m')), V(0))
                                            )['t']
            t4_meta = query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre=4,anio=year).aggregate(t=Coalesce(Sum('meta'), V(0)))['t']
            t4_alcanzada = query_actividad.filter(sub_actividad=subactividad,trimestre=4,fecha__year=str(year)).aggregate(
                                            t=Coalesce(
                                                Sum(
                                                    F('numero_presidencia') +
                                                    F('numero_presidencia_m') +
                                                    F('numero_tesorero') +
                                                    F('numero_tesorero_m') +
                                                    F('numero_secretario') +
                                                    F('numero_secretario_m') +
                                                    F('numero_vocal') +
                                                    F('numero_vocal_m') +
                                                    F('numero_fiscal') +
                                                    F('numero_fiscal_m')), V(0))
                                            )['t']
            if query_metas.filter(actividad_gerencial__actividad=subactividad,trimestre__in=[1,2,3,4],anio=year).count() > 0:
                tabla_programatica[nombre] = [t1_meta, t1_alcanzada,
                                                                t2_meta, t2_alcanzada,
                                                                t3_meta, t3_alcanzada,
                                                                t4_meta, t4_alcanzada
                                                                ]

    # else:
    #     var_mensaje = "aun no ha consultado para ver algún gráfico"


    return render(request, template, {'form':form,
                                      'tabla_programatica':tabla_programatica})
