from django import forms

from dal import autocomplete
from dal import forward

from .models import SubActividades, ActividadGerencial, Years, ActividadPrincipal

class SubActividadesAdminForm(forms.ModelForm):

    class Meta:
        model = SubActividades
        fields = '__all__'
        widgets = {
            'actividad_principal': autocomplete.ModelSelect2(url='sub-autocomplete', attrs={'data-language': 'es'}),

        }

class ActividadesAdminForm(forms.ModelForm):

    class Meta:
        model = ActividadGerencial
        fields = '__all__'
        widgets = {
            'actividad': autocomplete.ModelSelect2(url='subactividad-autocomplete', attrs={'data-language': 'es'}),

        }


class ConsultaForm(forms.Form):
    year = forms.ModelChoiceField(queryset=Years.objects.all(), label="Año")
    actividad_principal = forms.ModelChoiceField(queryset=ActividadPrincipal.objects.all(), label="Actividad principal",
        widget=autocomplete.ModelSelect2(url='actividad-autocomplete'))
    subactividad = forms.ModelChoiceField(queryset=SubActividades.objects.all(),
        widget=autocomplete.ModelSelect2(url='subactividad-autocomplete',forward=('actividad_principal', )))

class ConsultaActividades(forms.Form):
    year = forms.ModelChoiceField(queryset=Years.objects.all(), label="Año")