# -*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
#from actividades.models import Indicadores
# Create your models here.

@python_2_unicode_compatible
class ActividadPrincipal(models.Model):
    nombre = models.CharField(max_length=250)
    nombre_corto = models.CharField(max_length=250, null=True)
    #indicador = models.ForeignKey(Indicadores)

    def __str__(self):
        return self.nombre_corto

    class Meta:
        verbose_name='Actividad principal'
        verbose_name_plural='Actividades principales'
        ordering = ('id',)

CHOICES_CLASIFICADOS = (
                     (1, 'Eventos/Encuentros'),
                     (2, 'Personas Participantes'),
                     (3, 'Juntas directivas CAPS'),
                     (4, 'Productos'),
                     (5, 'Conteo CAPS'),
                     )

@python_2_unicode_compatible
class SubActividades(models.Model):
    actividad_principal = models.ForeignKey(ActividadPrincipal)
    nombre = models.CharField(max_length=250)
    nombre_corto = models.CharField(max_length=250, null=True)
    productos = models.TextField(null=True, blank=True)
    clasificacion = models.IntegerField(choices=CHOICES_CLASIFICADOS)

    def __str__(self):
        return self.nombre_corto

    class Meta:
        verbose_name='Sub Actividad'
        verbose_name_plural='Sub Actividades'
        ordering = ('id',)

@python_2_unicode_compatible
class Years(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name='año'
        verbose_name_plural='años'

CHOICES_TRIMESTRE = (
                     (1, 'T1'),
                     (2, 'T2'),
                     (3, 'T3'),
                     (4, 'T4'),
                     )

class ActividadGerencial(models.Model):
    actividad = models.ForeignKey(SubActividades, on_delete=models.CASCADE,
                                  verbose_name='sub actividad')

    class Meta:
        verbose_name = 'Actividad Gerencial'
        verbose_name_plural = 'Actividades Gerenciales'

class MetasAlcanzadas(models.Model):
    actividad_gerencial = models.ForeignKey(ActividadGerencial, on_delete=models.CASCADE)
    anio = models.ForeignKey(Years, on_delete=models.CASCADE)
    trimestre = models.IntegerField(choices=CHOICES_TRIMESTRE)
    meta = models.IntegerField()
    meta_alcanzada = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = 'Meta Alcanzada'
        verbose_name_plural = 'Metas Alcanzadas'







