from django.contrib import admin
from .models import ActividadPrincipal, SubActividades, Years, ActividadGerencial, MetasAlcanzadas
from .forms import SubActividadesAdminForm, ActividadesAdminForm

class MetasAlcanzadas(admin.TabularInline):
    model = MetasAlcanzadas
    extra = 1

class ActividadGerencialAdmin(admin.ModelAdmin):
    form = ActividadesAdminForm
    inlines = [MetasAlcanzadas]
    list_display = ('actividad',)

class SubActividadAdmin(admin.ModelAdmin):
    #form = SubActividadesAdminForm
    search_fields = ('nombre', 'nombre_corto')
    list_filter = ['actividad_principal__nombre']
    list_display = ('nombre','actividad_principal', 'nombre_corto')

class ActividadAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'nombre_corto')
    list_display = ('nombre','nombre_corto')

# Register your models here.
admin.site.register(ActividadPrincipal, ActividadAdmin)
admin.site.register(SubActividades, SubActividadAdmin)
admin.site.register(Years)
admin.site.register(ActividadGerencial, ActividadGerencialAdmin)
#admin.site.register(MetasAlcanzadas)
