# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-08-16 04:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('actividadgerencial', '0003_auto_20190607_1035'),
    ]

    operations = [
        migrations.AddField(
            model_name='subactividades',
            name='productos',
            field=models.TextField(blank=True, null=True),
        ),
    ]
