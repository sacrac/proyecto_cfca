from django.apps import AppConfig


class ActividadgerencialConfig(AppConfig):
    name = 'actividadgerencial'
    verbose_name = "Actividades Gerenciales"
