from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from .models import (ResultadoActividades, Resultados, 
                     EjesTransversales, Actividad, 
                     TipoActividad, TipoActor, Temas, ActividadPrincipal)
from lugar.models import *
from dal import autocomplete

class ResultadoActividadesAdminForm(forms.ModelForm):
    resultado = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Resultados.objects.all())
    objetivo = forms.CharField(widget=CKEditorUploadingWidget(), required=False)
    descripcion = forms.CharField(widget=CKEditorUploadingWidget(), required=False)
    resultado_actividad = forms.CharField(widget=CKEditorUploadingWidget(), required=False)
    recomendaciones = forms.CharField(widget=CKEditorUploadingWidget(), required=False)

    class Meta:
        model = ResultadoActividades
        fields = '__all__'


class ActividadesAdminForm(forms.ModelForm):
    tipo_actividad = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=TipoActividad.objects.all())
    tipo_actor = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=TipoActor.objects.all())
    modulos = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=EjesTransversales.objects.all(), label="Modulos")
    class Meta:
        model = Actividad
        fields = '__all__'
        widgets = {
            'actividad_principal': autocomplete.ModelSelect2(url='actividad-autocomplete', attrs={'data-language': 'es'}),
            'sub_actividad': autocomplete.ModelSelect2(url='subactividad-autocomplete',forward=['actividad_principal'], attrs={'data-language': 'es'}),
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete',forward=['departamento']),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'lugar': autocomplete.ModelSelect2(url='lugar-autocomplete',forward=['municipio'])
        }



class ActividadFormFiltros(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ActividadFormFiltros, self).__init__(*args, **kwargs)
          self.fields['departamento'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
                                                    'data-placeholder':'Escoge departamento', 'tabindex':'-1','area-hidden': 'true'})
          self.fields['actividad_principal'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
    #       self.fields['pais'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
    #       self.fields['productor'].widget.attrs.update({'class': 'form-control'})
    #       self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
    #       self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['comunidad'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible',
                                                    'data-placeholder':'Escoge departamento', 'tabindex':'-1','area-hidden': 'true'})
          self.fields['tema'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})

    #       if 'pais' in self.data:
    #         try:
    #             pais_id = int(self.data.get('pais'))
    #             self.fields['departamento'].queryset = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
    #         except (ValueError, TypeError):
    #             pass  # invalid input from the client; ignore and fallback to empty City queryset

          if 'departamento' in self.data:
            try:
                pais_id = int(self.data.get('departamento'))
                self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass

          if 'municipio' in self.data:
            try:
                departamento_id = int(self.data.get('municipio'))
                self.fields['comunidad'].queryset = Comunidad.objects.filter(municipio_id=departamento_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from

          
    fecha_inicial = forms.CharField(widget=forms.DateInput(attrs={'class':'form-control'}))
    fecha_final = forms.CharField(widget=forms.DateInput(attrs={'class':'form-control'}))
    departamento = forms.ModelMultipleChoiceField(queryset=Departamento.objects.all(),
                                                  required=False)
    municipio = forms.ModelMultipleChoiceField(queryset=Municipio.objects.none(), 
                required=False,
                widget=autocomplete.ModelSelect2Multiple(url='muni-filtro-autocomplete',forward=['departamento'],attrs={'data-language': 'es','data-placeholder':'Municipios'}))
    comunidad = forms.ModelMultipleChoiceField(queryset=Comunidad.objects.none(),
                required=False,
                widget=autocomplete.ModelSelect2Multiple(url='comu-filtro-autocomplete',forward=['municipio'],attrs={'data-language': 'es','data-placeholder':'Comunidades'}))
    tema = forms.ModelChoiceField(queryset=Temas.objects.all(), required=False)
    actividad_principal = forms.ModelChoiceField(queryset=ActividadPrincipal.objects.all(), 
                         required=False)
    tipo_actividad = forms.ModelMultipleChoiceField(queryset=TipoActividad.objects.all(),
                                                    required=False)
    modulos = forms.ModelMultipleChoiceField(queryset=EjesTransversales.objects.all(),
                                             required=False)



