from django.conf.urls import url, include
from .views import (SubActividadAutocomplete, ActividadAutocomplete,
                    MunicipioFiltroAutocomplete,ComunidadFiltroAutocomplete,
                    SubAutocomplete,consulta_actividades,tabla_generica,
                    tabla_miembros,tabla_inversiones,beneficiario_filtro,
                    salida_beneficiario_inversion,ComunidadFiltroCensoAutocomplete
                    )

from peticiones.views import PeticionRespuestaAutocomplete




urlpatterns = [
    url(
        r'^actividad-autocomplete/$',
        ActividadAutocomplete.as_view(),
        name='actividad-autocomplete',
    ),
    url(
        r'^subactividad-autocomplete/$',
        SubActividadAutocomplete.as_view(create_field='nombre'),
        name='subactividad-autocomplete',
    ),
    url(
        r'^sub-autocomplete/$',
        SubAutocomplete.as_view(),
        name='sub-autocomplete',
    ),
    url(
        r'^muni-filtro-autocomplete/$',
        MunicipioFiltroAutocomplete.as_view(create_field='nombre'),
        name='muni-filtro-autocomplete',
    ),
    url(
        r'^comu-filtro-autocomplete/$',
        ComunidadFiltroAutocomplete.as_view(create_field='nombre'),
        name='comu-filtro-autocomplete',
    ),
    url(
        r'^comu-censo-filtro-autocomplete/$',
        ComunidadFiltroCensoAutocomplete.as_view(create_field='nombre'),
        name='comu-censo-filtro-autocomplete',
    ),
    url(
        r'^filtros/$',
        consulta_actividades,
        name='consultar'
    ),
    url(
        r'^salida_generica/$',
        tabla_generica,
        name='salida_generica'
    ),
    url(
        r'^salida_miembros/$',
        tabla_miembros,
        name='salida_miembros'
    ),
    url(
        r'^tabla_inversiones/$',
        tabla_inversiones,
        name='salida_inversiones'
    ),
    url(
        r'^salida_beneficiario/$',
        beneficiario_filtro,
        name='salida_beneficiario'
    ),
    url(
        r'^beneficiario_inversion/$',
        salida_beneficiario_inversion,
        name='beneficiario_inversion'
    ),
    url(
        r'^peticion-autocomplete/$',
        PeticionRespuestaAutocomplete.as_view(),
        name='peticion-autocomplete',
    ),

]
