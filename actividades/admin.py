# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
from .forms import ResultadoActividadesAdminForm, ActividadesAdminForm

class InlinePresupuestoInfraestructura(admin.TabularInline):
    model = PresupuestoInfraestructura
    fields = ('alcaldia','otros','comunidad',)
    extra = 1
    max_num = 1

class InlineParticipanteTipos(admin.TabularInline):
    model = ParticipanteTipos
    extra = 1

class InlineResultadoActividades(admin.StackedInline):
    form = ResultadoActividadesAdminForm
    model = ResultadoActividades
    extra = 1
    max_num = 1

class InlineImagenesAdjuntos(admin.TabularInline):
    model = ImagenesAdjuntos
    extra = 1

class InlineFinanciamientoInstituciones(admin.TabularInline):
    model = FinanciamientoInstituciones
    extra = 1

class ActividadesAdmin(admin.ModelAdmin):
    # def get_queryset(self, request):
    #     queryset = super(ActividadesAdmin, self).get_queryset(request)
    #     queryset = queryset.prefetch_related('caps',
    #                                          'beneficiarios',
    #                                         'modulos',
    #                                         'tipo_actividad',
    #                                         'tipo_actor',
    #                                         'no_beneficiarios'). \
    #     select_related('organizador',
    #                    'tema',
    #                    'departamento',
    #                    'municipio',
    #                    'comunidad',
    #                    'lugar',
    #                    'actividad_principal',
    #                    'sub_actividad',
    #                    'encuesta_pre',
    #                    'encuesta_post')
    #     return queryset

    form = ActividadesAdminForm
    filter_horizontal = ('caps',)
    fields = (
            ('fecha','organizador'),
            ('actividad'),
            ('ambito_actividad','tema'),('departamento','municipio'),
            ('comunidad','lugar'),'actividad_principal', 'sub_actividad',
            ('tipo_actividad','modulos'),
            ('tipo_actor','numero_participante','numero_hombres','numero_mujeres'),
            ('numero_presidencia','numero_presidencia_m','numero_tesorero','numero_tesorero_m'),
            ('numero_secretario','numero_secretario_m','numero_vocal','numero_vocal_m'),
            ('numero_fiscal','numero_fiscal_m','numero_apoyo','numero_apoyo_m'),
            'caps',
            ('encuesta_pre','encuesta_post'),('presupuesto','ejecutado','estado'),
            'descripcion_inversion',
            )
    #raw_id_fields = ('caps',)
    list_select_related = ('organizador',
                           'tema','departamento','municipio',
                           'comunidad','lugar','actividad_principal',
                           'sub_actividad','encuesta_pre','encuesta_post')
    search_fields = ('actividad','organizador__nombre')
    #raw_id_fields = ('organizador','tema','caps','encuesta_pre','encuesta_post')
    list_display = ('actividad','fecha','organizador','numero_participante','numero_hombres','numero_mujeres')
    list_filter = ('modulos','tipo_actor','departamento','municipio','tipo_actividad')
    date_hierarchy = 'fecha'
    inlines = [InlinePresupuestoInfraestructura,InlineFinanciamientoInstituciones,
               InlineResultadoActividades,InlineImagenesAdjuntos]

    class Media:
        css = {
            'all': ('css/admin/actividad_admin.css',)
        }
        js = ('js/actividad_admin.js',)

    #def save_formset(self, request, form, formset, change):
    #    if formset.model == FinanciamientoInstituciones:
    #        financiamiento = formset.save(commit=True)
    #        for ej in financiamiento:
    #            ej.total = ej.monto
    #            ej.save()
    #    return super(ActividadesAdmin, self).save_formset(request, form, formset, change)



# Register your models here.
admin.site.register(Organizadores)
admin.site.register(TipoActividad)
admin.site.register(Actividad, ActividadesAdmin)
#admin.site.register(ParticipanteEdad)
#admin.site.register(ParticipanteTipos)
admin.site.register(Resultados)
admin.site.register(EjesTransversales)
#admin.site.register(ResultadoActividades)
#admin.site.register(ImagenesAdjuntos)
admin.site.register(TipoActor)
admin.site.register(Indicadores)
admin.site.register(Temas)
admin.site.register(Lugares)
admin.site.register(Instituciones)
