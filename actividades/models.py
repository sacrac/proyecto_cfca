# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from sorl.thumbnail import ImageField
from lugar.models import Departamento, Municipio, Comunidad
from datosActores.models import FichaActor
from datosCaps.models import CHOICE_EDAD_FAMILIA, FichaCaps
from beneficiarios.models import BeneficiariosCaps, NoBeneficiariosCaps
from encuestas.models import Survey
from actividadgerencial.models import ActividadPrincipal, SubActividades, CHOICES_TRIMESTRE

from datetime import date

# Create your models here.
@python_2_unicode_compatible
class Organizadores(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Organizador'
        verbose_name_plural = 'Organizadores'


CHOICE_AMBITO_ACTIVIDAD = ((1,'Comunal'),(2,'Municipal'),(3,'Departamental'),(4,'Nacional'))

class Lugares(models.Model):
    municipio = models.ForeignKey(Municipio)
    lugar = models.CharField(max_length=250)

    def __str__(self):
        return self.lugar

@python_2_unicode_compatible
class EjesTransversales(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Modulos'
        verbose_name_plural = 'Modulos'

@python_2_unicode_compatible
class TipoActividad(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de actividad'
        verbose_name_plural = 'Tipos de actividades'

@python_2_unicode_compatible
class Temas(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tema'
        verbose_name_plural = 'Temas'

@python_2_unicode_compatible
class TipoActor(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de actor'
        verbose_name_plural = 'Tipos de Actores'

CHOICE_STATUS = ((1,'En Proceso'),(2,'Finalizada'),)

@python_2_unicode_compatible
class Actividad(models.Model):
    fecha = models.DateField()
    organizador = models.ForeignKey(Organizadores)
    ambito_actividad = models.IntegerField(choices=CHOICE_AMBITO_ACTIVIDAD)
    tema = models.ForeignKey(Temas, null=True)
    departamento = models.ForeignKey(Departamento)
    municipio = models.ForeignKey(Municipio)
    comunidad = models.ForeignKey(Comunidad, null=True, blank=True)
    lugar = models.ForeignKey(Lugares, null=True, blank=True, verbose_name="local")
    actividad_principal = models.ForeignKey(ActividadPrincipal, null=True)
    sub_actividad = models.ForeignKey(SubActividades, null=True)
    actividad = models.CharField('Nombre de la actividad', max_length=250)
    tipo_actividad = models.ManyToManyField(TipoActividad)
    modulos = models.ManyToManyField(EjesTransversales,verbose_name="Modulos", blank=True)
    tipo_actor = models.ManyToManyField(TipoActor)
    numero_participante = models.IntegerField('Numero de participante', null=True, blank=True)
    numero_hombres = models.IntegerField('Numero de hombres', null=True, blank=True)
    numero_mujeres = models.IntegerField('Numero de mujeres', null=True, blank=True)
    numero_presidencia = models.IntegerField('# presidente hombres', null=True, blank=True)
    numero_presidencia_m = models.IntegerField('# presidente mujeres', null=True, blank=True)
    numero_tesorero = models.IntegerField('# tesorero hombre', null=True, blank=True)
    numero_tesorero_m = models.IntegerField('# tesorero mujer', null=True, blank=True)
    numero_secretario = models.IntegerField('# secretario hombre', null=True, blank=True)
    numero_secretario_m = models.IntegerField('# secretario mujer', null=True, blank=True)
    numero_vocal = models.IntegerField('# vocal hombre', null=True, blank=True)
    numero_vocal_m = models.IntegerField('# vocal mujer', null=True, blank=True)
    numero_fiscal = models.IntegerField('# fiscal hombre', null=True, blank=True)
    numero_fiscal_m = models.IntegerField('# fiscal mujeres', null=True, blank=True)
    numero_apoyo = models.IntegerField('# apoyo hombre', null=True, blank=True)
    numero_apoyo_m = models.IntegerField('# apoyo mujeres', null=True, blank=True)
    caps = models.ManyToManyField(FichaCaps, blank=True)
    beneficiarios = models.ManyToManyField(BeneficiariosCaps, blank=True)
    no_beneficiarios = models.ManyToManyField(NoBeneficiariosCaps, blank=True)
    encuesta_pre = models.ForeignKey(Survey, related_name='surver_pre', null=True, blank=True)
    encuesta_post = models.ForeignKey(Survey, related_name='surver_post', null=True, blank=True)
    presupuesto = models.FloatField(null=True, blank=True)
    ejecutado = models.FloatField(null=True, blank=True)
    estado = models.IntegerField(choices=CHOICE_STATUS, null=True, blank=True)
    descripcion_inversion = models.TextField(null=True, blank=True)

    trimestre = models.IntegerField(choices=CHOICES_TRIMESTRE, null=True, blank=True, editable=False)

    def __str__(self):
        return self.actividad

    def save(self, *args, **kwargs):
        if self.fecha >= date(year=self.fecha.year, month=1, day=1) and self.fecha <= date(year=self.fecha.year, month=3, day=31):
            #print("trimestre 1")
            self.trimestre = 1
        elif self.fecha >= date(year=self.fecha.year, month=4, day=1) and self.fecha <= date(year=self.fecha.year, month=6, day=30):
            #print("trimestre 2")
            self.trimestre = 2
        elif self.fecha >= date(year=self.fecha.year, month=7, day=1) and self.fecha <= date(year=self.fecha.year, month=9, day=30):
            #print("trimestre 3")
            self.trimestre = 3
        elif self.fecha >= date(year=self.fecha.year, month=10, day=1) and self.fecha <= date(year=self.fecha.year, month=12, day=31):
            #print("trimestre 4")
            self.trimestre = 4
        else:
            print("que fecha es esta cosa")

        super(Actividad, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'


class PresupuestoInfraestructura(models.Model):
    actividad = models.ForeignKey(Actividad)
    alcaldia = models.FloatField(null=True, blank=True)
    comunidad = models.FloatField(null=True, blank=True)
    empresa_privada = models.FloatField(null=True, blank=True)
    ong = models.FloatField(null=True, blank=True)
    otros = models.FloatField('Enacal', null=True, blank=True)

    class Meta:
        verbose_name = 'Presupuesto para infraestructura'
        verbose_name_plural = 'Presupuestos para infraestructuras'

class ParticipanteEdad(models.Model):
    actividad = models.ForeignKey(Actividad)
    edad = models.IntegerField(choices=CHOICE_EDAD_FAMILIA)
    cantidad = models.IntegerField()

    class Meta:
        verbose_name = 'Participante por edad'
        verbose_name_plural = 'Participantes por edades'


CHOICE_TIPOS = (
                (1,'Juntas directivas CAPS'),
                (2,'Presidente'),
                (3,'Secretario'),
                (4,'Vocal'),
                (5,'Tesorero'),
                (6,'Fiscal'),
                (7,'Usuarios'),
                (8,'Técnico ONG'),
                (9,'Técnicos UMAS'),
                (10,'Técnicos estatales'),
                (11,'Periodistas'),
                (12,'Docentes'),
                )

class ParticipanteTipos(models.Model):
    actividad = models.ForeignKey(Actividad)
    particpantes = models.IntegerField(choices=CHOICE_TIPOS)
    cantidad = models.IntegerField()

    class Meta:
        verbose_name = 'Participante por tipo'
        verbose_name_plural = 'Participantes por tipos'

@python_2_unicode_compatible
class Indicadores(models.Model):
    indicador = models.CharField(max_length=250)

    def __str__(self):
        return self.indicador

    class Meta:
        verbose_name_plural = 'Indicadores'


@python_2_unicode_compatible
class Resultados(models.Model):
    nombre = models.CharField(max_length=250)
    indicador = models.ForeignKey(Indicadores, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Resultado'
        verbose_name_plural = 'Resultados'

class ResultadoActividades(models.Model):
    actividad = models.ForeignKey(Actividad)
    resultado = models.ManyToManyField(Resultados)
    #eje_transversal = models.ManyToManyField(EjesTransversales,verbose_name="Modulos")
    objetivo = models.TextField('Objetivo de la actividad',null=True,blank=True)
    descripcion = models.TextField(null=True,blank=True)
    resultado_actividad = models.TextField(null=True,blank=True)
    recomendaciones = models.TextField(null=True,blank=True)

    class Meta:
        verbose_name = 'Resultado de la actividad'
        verbose_name_plural = 'Resultados de las actividades'

class ImagenesAdjuntos(models.Model):
    activdad = models.ForeignKey(Actividad)
    foto = ImageField(upload_to='fotos/actividad/', null=True, blank=True)
    credito_foto = models.CharField(max_length=250, null=True, blank=True)
    archivo = models.FileField(upload_to='adjunto/actividad', max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = 'Adjjunto e Imagen de la actividad'
        verbose_name_plural = 'Adjuntos de las actividades'

#Entradas por instituciones
class Instituciones(models.Model):
    nombre = models.CharField(max_length=250)
    logo = ImageField(upload_to='logos/instituciones/', null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Institucion'
        verbose_name_plural = 'Instituciones'

class FinanciamientoInstituciones(models.Model):
    actividad = models.ForeignKey(Actividad)
    institucion = models.ForeignKey(Instituciones)
    monto = models.FloatField()

    total = models.FloatField(editable=False, null=True, blank=True, default=0)

    def save(self, *args, **kwargs):
        if self.monto:
            self.total += self.monto
        super(FinanciamientoInstituciones, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Instituciones que financian mejoras/perforaciones'
        verbose_name_plural = 'Instituciones que financian mejoras/perforaciones'










