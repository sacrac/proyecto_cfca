# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-06-07 16:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('actividadgerencial', '0003_auto_20190607_1035'),
        ('actividades', '0004_auto_20190301_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='actividad',
            name='actividad_principal',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='actividadgerencial.ActividadPrincipal'),
        ),
    ]
