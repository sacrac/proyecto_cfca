# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db.models import Count, Q, Sum, Value as V, F
from django.db.models.functions import Coalesce
from actividadgerencial.models import SubActividades, ActividadPrincipal
from dal import autocomplete
from .forms import ActividadFormFiltros
from .models import Actividad, EjesTransversales
from lugar.models import Comunidad, Municipio, Departamento
from .models import BeneficiariosCaps
from collections import OrderedDict
import datetime
# Create your views here.

class ActividadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = ActividadPrincipal.objects.all()

        #actividad_principal = self.forwarded.get('actividad_principal', None)

        #if actividad_principal:
        #    qs = qs.filter(actividad_principal=actividad_principal)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class SubActividadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = SubActividades.objects.all()

        actividad_principal = self.forwarded.get('actividad_principal', None)

        if actividad_principal:
            qs = qs.filter(actividad_principal=actividad_principal)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class SubAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = SubActividades.objects.all()

        #actividad_principal = self.forwarded.get('actividad_principal', None)

        #if actividad_principal:
        #    qs = qs.filter(actividad_principal=actividad_principal)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


class MunicipioFiltroAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Municipio.objects.all()
        departamento = self.forwarded.get('departamento', None)
        if departamento:
            qs = qs.filter(departamento__in=departamento)
        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)
        return qs

class ComunidadFiltroAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Comunidad.objects.all()
        municipio = self.forwarded.get('municipio', None)
        if municipio:
            qs = qs.filter(municipio__in=municipio)
        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)
        return qs

class ComunidadFiltroCensoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Comunidad.objects.all()
        municipio = self.forwarded.get('municipio', None)
        if municipio:
            qs = qs.filter(municipio=municipio)
        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)
        return qs

def _queryset_actividad(request):
    params = {}

    if 'fecha_inicial' in request.session:
       params['fecha__range'] = (request.session['fecha_inicial'],request.session['fecha_final'])

    if 'departamento' in request.session:
        params['departamento__in'] = request.session['departamento']

    if 'municipio' in request.session:
        params['municipio__in'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['comunidad__in'] = request.session['comunidad']

    if 'tema' in request.session:
        params['tema'] = request.session['tema']

    if 'actividad_principal' in request.session:
        params['actividad_principal'] = request.session['actividad_principal']

    if 'tipo_actividad' in request.session:
        params['tipo_actividad__in'] = request.session['tipo_actividad']

    if 'modulos' in request.session:
        params['modulos__in'] = request.session['modulos']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print("paramettros")
    print(params)

    return Actividad.objects.filter(**params).select_related()


def consulta_actividades(request, template='filtros/consultar.html'):
    if request.method == 'POST':
        form = ActividadFormFiltros(request.POST)
        if form.is_valid():
            request.session['fecha_inicial'] = form.cleaned_data['fecha_inicial']
            request.session['fecha_final'] = form.cleaned_data['fecha_final']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['tema'] = form.cleaned_data['tema']
            request.session['actividad_principal'] = form.cleaned_data['actividad_principal']
            request.session['tipo_actividad'] = form.cleaned_data['tipo_actividad']     
            request.session['modulos'] = form.cleaned_data['modulos']
            centinela = 1
            print("fue valido")
        else:
            centinela = 0
            print("No fue valido")
    else:
        form = ActividadFormFiltros()
        centinela = 0

        if 'fecha_inicial' in request.session:
            try:
                del request.session['fecha_inicial']
                del request.session['fecha_final']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['tema']
                del request.session['actividad_principal']
                del request.session['tipo_actividad']
                del request.session['modulos']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})

def tabla_generica(request,template='filtros/tabla_generica.html'):
    filtro = _queryset_actividad(request)
    
    dups = (
        filtro.values_list('caps__id',flat=True)
        .annotate(count=Count('caps__id'))
        .values('caps__id','caps__nombre_caps')
        .order_by()
        .filter(count__gt=1)
    )
    
    return render(request,template,{'filtro':filtro,'conteo':filtro.count(),
                                    'caps_repetidos':dups, 'num_caps_repetidos':len(dups)})


def tabla_miembros(request,template='filtros/tabla_miembros.html'):
    filtro = _queryset_actividad(request)
    return render(request,template,{'filtro':filtro,'conteo':filtro.count()})

def tabla_inversiones(request,template='filtros/tabla_inversiones.html'):
    filtro = _queryset_actividad(request)
    return render(request,template,{'filtro':filtro,'conteo':filtro.count()})



def beneficiario_filtro(request,template='seguimiento/segimiento_beneficiario.html'):
    query_actividad = _queryset_actividad(request)


    #salidas sobre conteos de personas participando en los modulos
    grafo_actividad = OrderedDict()
    personas_capasitadas = 0
    caps_capasitados = []
    for obj in EjesTransversales.objects.exclude(id=7):
        total = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(total=Sum('numero_participante'))['total'] or 0
        hombres = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(hombres=Sum('numero_hombres'))['hombres'] or 0
        mujeres = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(mujeres=Sum('numero_mujeres'))['mujeres'] or 0
        #caps = query_actividad.filter(tipo_actividad=19,modulos=obj).aggregate(cap=Count('caps'))['cap'] or 0
        caps = query_actividad.filter(tipo_actividad=19,modulos=obj).values_list('caps__id', flat=True).distinct()
        if total != None and total > 0:
            caps_capasitados.append(len(caps))
            grafo_actividad[obj.nombre] = [total,hombres,mujeres,caps]

    for k,v in grafo_actividad.items():
        personas_capasitadas += v[0]

    grafo_actividad = grafo_actividad
    personas_capasitadas = personas_capasitadas
    try:
        caps_capasitados = max(caps_capasitados)
    except:
        caps_capasitados = 0
        
    totales_juntas_directivas = OrderedDict()


    tabla_grafo_municipio = OrderedDict()
    tabla_junta_directiva = OrderedDict()

    for modulo in EjesTransversales.objects.exclude(id=7).order_by('id'):
        total = query_actividad.filter(tipo_actividad=19,modulos=modulo).aggregate(total=Sum('numero_participante'))['total'] or 0
        if total != None and total > 0:
            hombres_jd = 0
            mujeres_jd = 0
            tabla_grafo_municipio[modulo] = OrderedDict()
            tabla_junta_directiva[modulo] = OrderedDict()
            totales_juntas_directivas[modulo] = OrderedDict()
            for obj in Departamento.objects.exclude(id=3):
                tabla_grafo_municipio[modulo][obj.nombre] = OrderedDict()
                var_h = 0
                var_m = 0
                for muni in Municipio.objects.filter(departamento=obj):
                    presi_h = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_presidencia'),V(0)))['t']
                    presi_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_presidencia_m'),V(0)))['t']
                    tesorero = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_tesorero'),V(0)))['t']
                    tesorero_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_tesorero_m'),V(0)))['t']
                    secretario = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_secretario'),V(0)))['t']
                    secretario_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_secretario_m'),V(0)))['t']
                    vocal = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_vocal'),V(0)))['t']
                    vocal_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_vocal_m'),V(0)))['t']
                    fiscal = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_fiscal'),V(0)))['t']
                    fiscal_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_fiscal_m'),V(0)))['t']
                    apoyo = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_apoyo'),V(0)))['t']
                    apoyo_m = query_actividad.filter(tipo_actividad=19,modulos=modulo,municipio=muni).aggregate(t=Coalesce(Sum('numero_apoyo_m'),V(0)))['t']
                    hombres = presi_h+tesorero+secretario+vocal+fiscal+apoyo
                    mujeres = presi_m+tesorero_m+secretario_m+vocal_m+fiscal_m+apoyo_m

                    tabla_grafo_municipio[modulo][obj.nombre][muni.nombre] = [
                                                                    presi_h,
                                                                    presi_m,
                                                                    tesorero,
                                                                    tesorero_m,
                                                                    secretario,
                                                                    secretario_m,
                                                                    vocal,
                                                                    vocal_m,
                                                                    fiscal,
                                                                    fiscal_m,
                                                                    apoyo,
                                                                    apoyo_m,
                                                                    hombres,
                                                                    mujeres
                                                                ]
                    hombres_jd += hombres
                    mujeres_jd += mujeres
                    var_h += hombres
                    var_m += mujeres
                totales_juntas_directivas[modulo]['hombre'] = hombres_jd
                totales_juntas_directivas[modulo]['mujeres'] = mujeres_jd
                tabla_junta_directiva[modulo][obj.nombre] = [var_h,var_m]

    
    return render(request,template, locals())

def salida_beneficiario_inversion(request, template='seguimiento/seguimiento_beneficiario_inversion.html'):
    query_actividad = _queryset_actividad(request)
    query_beneficiario = BeneficiariosCaps.objects.select_related()

    #salidas de las inversiones de perforacion y mejoras
    conteo_total_perforacion = query_actividad.filter(tipo_actividad=12).count()
    conteo_total_mejoras = query_actividad.filter(tipo_actividad=13).count()
    years = []
    fecha_i = datetime.datetime.strptime(request.session['fecha_inicial'],'%Y-%m-%d')
    fecha_f = datetime.datetime.strptime(request.session['fecha_final'],'%Y-%m-%d')
    print(fecha_i)

    actual = fecha_i.year
    span = fecha_f.year - fecha_i.year
    if span == 0:
        years.append((fecha_f.year,fecha_f.year))
    else:
        for i in range(span):
            actual += 1
            years.append((actual,actual))
   
    years_final = years
  
    comunidades_beneficiadas = Comunidad.objects.filter(actividad__modulos__in=[1,2,3,4,5,6]).order_by('nombre').distinct('nombre').count()

    inversiones_anio = OrderedDict()
    for y in years_final:
        ejecutado_perforacion = query_actividad.filter(fecha__year=y[0], tipo_actividad=12).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
        total_perforacion = ( query_actividad
            .filter(fecha__year=y[0], tipo_actividad=12)
            .aggregate(
                alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                #empresa=Coalesce(Sum(F('presupuestoinfraestructura__empresa_privada')),V(0)),
                ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0)),
                otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
            )
         )
        perforacion = ejecutado_perforacion + total_perforacion['alcaldia'] + total_perforacion['comunidad'] + \
                                total_perforacion['otros'] + total_perforacion['ong']

        ejecutado_mejora = query_actividad.filter(fecha__year=y[0], tipo_actividad=13).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
        total_mejora = ( query_actividad
            .filter(fecha__year=y[0], tipo_actividad=13)
            .aggregate(
                alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                #empresa=Coalesce(Sum(F('presupuestoinfraestructura__empresa_privada')),V(0)),
                ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0)),
                otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
            )
         )
        mejora = ejecutado_mejora + total_mejora['alcaldia'] + total_mejora['comunidad'] + \
                                total_mejora['otros'] + total_mejora['ong']

        inversiones_anio[y[1]] = [perforacion, mejora]

    tabla_inversiones = inversiones_anio

    total_inveresiones = [sum(x) for x in zip(*inversiones_anio.values())]
    grafo_inversion_total = sum(total_inveresiones)

    tabla_beneficiario_mejora_perfo = []
    reporte = {}
    for acti in query_actividad.filter(tipo_actividad__in=[12,13]):
        total_personas = query_beneficiario.filter(comunidad=acti.comunidad).count()
        total_hombres = query_beneficiario.filter(comunidad=acti.comunidad, sexo=1).count()
        total_mujeres = query_beneficiario.filter(comunidad=acti.comunidad, sexo=2).count()
        menores_h = [obj for obj in query_beneficiario.filter(comunidad=acti.comunidad,sexo=1) if obj.edad_persona() <= 15]
        menores_m = [obj for obj in query_beneficiario.filter(comunidad=acti.comunidad,sexo=2) if obj.edad_persona() <= 15]
        tabla_beneficiario_mejora_perfo.append([total_personas,total_hombres,total_mujeres,len(menores_h),len(menores_m)])
        alcaldia = 0
        enacal = 0
        comunidad = 0
        ong = 0
        for objecto in acti.presupuestoinfraestructura_set.all():
            #print("Presupuesto")
            alcaldia = objecto.alcaldia
            comunidad = objecto.comunidad
            enacal = objecto.otros
            #print(dir(objecto))
        for dinero in acti.financiamientoinstituciones_set.all():
            ong = dinero.total
        reporte[acti.actividad] = [acti.comunidad,acti.fecha.year,acti.tipo_actividad,
            acti.presupuesto,acti.ejecutado,alcaldia,enacal,comunidad,ong,acti.get_estado_display()]

    total_bene_comunidad = [sum(x) for x in zip(*tabla_beneficiario_mejora_perfo)]
    reporte
    #Inversiones por munucipio y por año
    tabla_inversiones_municipio = OrderedDict()
    tabla_inversiones_totales = {}
    for y in years_final:
        tabla_inversiones_municipio[y[1]] = OrderedDict()
        lista_monto = []
        for muni in Municipio.objects.exclude(id=7):
            ejecutado_perforacion = query_actividad.filter(fecha__year=y[0],
                                                           tipo_actividad=12,
                                                           municipio=muni).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
            total_perforacion = ( query_actividad
                .filter(fecha__year=y[0], tipo_actividad=12, municipio=muni)
                .aggregate(
                    alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                    comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                    #empresa=Coalesce(Sum(F('presupuestoinfraestructura__empresa_privada')),V(0)),
                    ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0)),
                    otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
                )
             )
            perforacion = ejecutado_perforacion + total_perforacion['alcaldia'] + total_perforacion['comunidad'] + \
                                    total_perforacion['otros'] + total_perforacion['ong']

            ejecutado_mejora = query_actividad.filter(fecha__year=y[0],
                                                      tipo_actividad=13,
                                                      municipio=muni).aggregate(t=Coalesce(Sum('ejecutado'),V(0)))['t']
            total_mejora = ( query_actividad
                .filter(fecha__year=y[0], tipo_actividad=13, municipio=muni)
                .aggregate(
                    alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                    comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                    #empresa=Coalesce(Sum(F('presupuestoinfraestructura__empresa_privada')),V(0)),
                    ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0)),
                    otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
                )
             )
            mejora = ejecutado_mejora + total_mejora['alcaldia'] + total_mejora['comunidad'] + \
                                total_mejora['otros'] + total_mejora['ong']
            if perforacion or mejora:
                lista_monto = [(k, total_perforacion[k], v) for k, v in total_mejora.items() if v > 0]
                tabla_inversiones_municipio[y[1]][muni.nombre] = [perforacion, mejora,
                                                                  ejecutado_perforacion,
                                                                  ejecutado_mejora,lista_monto]

    for y in years_final:
        total_perforacion_anio = query_actividad.filter(fecha__year=y[0],tipo_actividad=12).count()
        total_mejoras_anio = query_actividad.filter(fecha__year=y[0],tipo_actividad=13).count()
        tabla_inversiones_totales[y[1]] = [total_perforacion_anio,total_mejoras_anio]

    tabla_inversiones_municipio
    tabla_inversiones_totales

    return render(request, template, locals())
