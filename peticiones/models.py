# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone as tz
from django.db import models

from lugar.models import Departamento, Municipio, Comunidad
from datosCaps.models import FichaCaps
# Create your models here.

@python_2_unicode_compatible
class Instituciones(models.Model):
    siglas = models.CharField(max_length=50)
    nombre_completo = models.CharField(max_length=250)

    def __str__(self):
        return self.siglas

    class Meta:
        verbose_name = 'Institución'
        verbose_name_plural = 'Instituciones'

@python_2_unicode_compatible
class TiposPeticiones(models.Model):
    titulo = models.CharField(max_length=250)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Tipo Petición'
        verbose_name_plural = 'Tipos de peticiones'


@python_2_unicode_compatible
class Contactos(models.Model):
    nombre = models.CharField(max_length=250)
    correo = models.EmailField()
    institucion_pertenece = models.ForeignKey(Instituciones)

    def __str__(self):
        return "%s - %s" % (self.nombre, self.institucion_pertenece)

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'

CHOICE_GENERO = (('hombre','Hombre'),('mujer','Mujer'))
CHOICE_FORMATO = ((1,'Escrito'),(2,'Whatsapp'),(3,'Correo Electronico'),(4,'Facebook'))
CHOICE_TIPO_ACCION = ((1,'Petición'),(2,'Consulta'))

@python_2_unicode_compatible
class Peticion(models.Model):
    fecha = models.DateTimeField(default=tz.now)
    caps = models.ForeignKey(FichaCaps)
    sexo = models.CharField('Sexo del que hace la petición',choices=CHOICE_GENERO,
        max_length=10, null=True, blank=True)
    formato = models.IntegerField(choices=CHOICE_FORMATO, null=True, blank=True)
    tipo_accion = models.IntegerField(choices=CHOICE_TIPO_ACCION, null=True, blank=True)
    solicitud_a = models.ManyToManyField(Instituciones, blank=True)
    tipo_peticion = models.ForeignKey(TiposPeticiones, null=True, blank=True)
    # institucion = models.ForeignKey(Instituciones, verbose_name="Institución que dirige la petición",
    #     null=True, blank=True)
    descripcion = models.TextField("Descripción de la petición")
    

    def __str__(self):
        return "%s -- FECHA: %s" % (self.caps.nombre_caps, self.fecha.strftime("%d %B %Y"))

    class Meta:
        verbose_name = 'Petición'
        verbose_name_plural = 'Peticiones'
        ordering = ('-fecha',)

CHOICE_TIPO_RESPUESTA = ((1,'Resuelta'),(2,'En proceso'))

class RespuestaPeticion(models.Model):
    peticion = models.ForeignKey(Peticion)
    fecha = models.DateField(null=True, blank=True)
    tipo_resolucion = models.IntegerField(choices=CHOICE_TIPO_RESPUESTA, 
                                          null=True, blank=True)
    #resuelta = models.BooleanField(default=False)
    respuesta = models.TextField('Respuesta a la peticion')
    por_escrito = models.BooleanField()
    por_verbal = models.BooleanField()
    subir_archivo = models.FileField(upload_to='adjuntosPeticiones/', null=True, blank=True)
    #sin_respuesta = models.BooleanField(editable=False)


    def __str__(self):
        return "%s" % self.peticion

    class Meta:
        verbose_name = 'Respuesta a la petición'
        verbose_name_plural = 'Respuestas a las peticiones'


