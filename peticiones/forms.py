from django import forms
from django.utils import timezone as tz
from .models import *
from dal import autocomplete
from ckeditor_uploader.widgets import CKEditorUploadingWidget

class PeticionForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(PeticionForm, self).__init__(*args, **kwargs)
    #     self.fields['fecha'] = forms.CharField(required=True)
    #     self.fields['caps'] = forms.ModelChoiceField(queryset=FichaCaps.objects.select_related('departamento','municipio').prefetch_related('comunidad'), required=True)
    #     self.fields['sexo'] = forms.ChoiceField(choices=CHOICE_GENERO,required=True)
    #     self.fields['tipo_accion'] = forms.ChoiceField(choices=CHOICE_TIPO_ACCION,required=True)
    #     self.fields['institucion'] = forms.ModelChoiceField(queryset=Instituciones.objects.all(),required=True)
    #     self.fields['descripcion'] = forms.CharField(required=True, widget=forms.Textarea)
    #     self.fields['tipo_peticion'] = forms.ModelChoiceField(queryset=TiposPeticiones.objects.all(),required=True)
      
    class Meta:
        model = Peticion
        fields = '__all__'

class RespuestaPeticionesForm(forms.ModelForm):
    respuesta = forms.CharField(widget=CKEditorUploadingWidget())
    class Meta:
        model = RespuestaPeticion
        fields = '__all__'
        widgets = {
            'peticion': autocomplete.ModelSelect2(url='peticion-autocomplete', attrs={'data-language': 'es'}),   
        }
