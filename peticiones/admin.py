# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Instituciones, RespuestaPeticion, Peticion,TiposPeticiones
from .forms import RespuestaPeticionesForm
# Register your models here.

class PeticionesAdmin(admin.ModelAdmin):
    list_select_related = ('caps',)
    raw_id_fields = ('caps',)
    list_display = ('get_caps','fecha','sexo')
    search_fields = ('caps__nombre_caps',)
    list_filter = ('fecha','sexo','tipo_peticion')

    def get_caps(self, obj):
        return obj.caps.nombre_caps
    get_caps.short_description = 'Casp'
    get_caps.admin_order_field = 'caps__nombre_caps'


class RespuestaPeticionAdmin(admin.ModelAdmin):
    form = RespuestaPeticionesForm
    list_select_related = ('peticion',)
    #raw_id_fields = ('caps',)
    list_display = ('get_caps','fecha')
    search_fields = ('peticion',)
    list_filter = ('fecha',)

    def get_caps(self, obj):
        return obj.peticion.caps.nombre_caps
    get_caps.short_description = 'Casp'
    get_caps.admin_order_field = 'caps__nombre_caps'

admin.site.register(Peticion, PeticionesAdmin)
admin.site.register(RespuestaPeticion, RespuestaPeticionAdmin)
admin.site.register(Instituciones)
admin.site.register(TiposPeticiones)