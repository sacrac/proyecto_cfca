# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.utils import timezone
from .models import *
from .forms import PeticionForm
import datetime
from dal import autocomplete

# Create your views here.
class PeticionRespuestaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Peticion.objects.all()
        if self.q:
            qs = qs.filter(caps__nombre_caps__istartswith=self.q)

        return qs

def peticiones(request, template = "seguimiento/peticiones.html"):
    total_peticiones = Peticion.objects.count()
    peticiones_resueltas = RespuestaPeticion.objects.filter(tipo_resolucion=1).count()
    peticiones_pendientes = total_peticiones - peticiones_resueltas

    tabla = Peticion.objects.select_related('tipo_peticion').prefetch_related('caps','solicitud_a').order_by('-fecha')
    # .values_list('tipo_peticion__titulo','fecha','sexo','caps__nombre_caps',
    #              'solicitud_a__siglas','respuestapeticion__fecha',
    #              'respuestapeticion__resuelta','descripcion','id').distinct()

    map_peticiones = Peticion.objects.filter(caps__latitud__gt=0).values_list('caps__latitud',
                                                                'caps__longitud',
                                                                'caps__nombre_caps',
                                                                'caps__direccion',
                                                                'caps__municipio__nombre',
                                                                'caps__id')

    grafo_municipios = {}
    for x in Municipio.objects.all():
        conteo = tabla.filter(caps__municipio_id = x.id).count()
        if conteo > 0:
            grafo_municipios[x.nombre] = conteo
    
    sexo_peticion = {}
    for x in CHOICE_GENERO:
        conteo = Peticion.objects.filter(sexo = x[0]).count()
        sexo_peticion[x[1]] = conteo

    formato_peticion = {}
    for x in CHOICE_FORMATO:
        conteo = Peticion.objects.filter(formato = x[0]).count()
        formato_peticion[x[1]] = conteo

    grafo_peticion = {}
    for x in TiposPeticiones.objects.all():
        conteo = Peticion.objects.filter(tipo_peticion = x).count()
        porcentaje = round(( conteo / total_peticiones ) * 100, 2)
        grafo_peticion[x.titulo] = (conteo, porcentaje)

    return render(request,template,locals())

def save_peticiones(request, template="peticiones/save_peticiones.html"):
    if request.method == 'POST':
        form = PeticionForm(request.POST)
        if form.is_valid():
            form.save() 
            return redirect('/peticiones/thank/')
        else:
            print("Hubo error no se grabo nada!")
    else:
        form = PeticionForm()
    return render(request, template, locals())
