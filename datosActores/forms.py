from django import forms
from django.contrib import admin
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from .models import DatosDinamicosActor, Proyectos
from lugar.models import Municipio


class DatosActorAdminForm(forms.ModelForm):
    proyectos = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Proyectos.objects.all())
    municipios_atiende = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Municipio.objects.all())
    class Meta:
        model = DatosDinamicosActor
        fields = '__all__'
