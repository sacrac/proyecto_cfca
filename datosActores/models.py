# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from multiselectfield import MultiSelectField
from sorl.thumbnail import ImageField
from lugar.models import Municipio

# Create your models here.

CHOICES_AMBITO = ((1, 'Local'),
                 (2, 'Municipal'),
                 (3, 'Nacional'),
                 )

class FichaActor(models.Model):
    nombre_siglas = models.CharField(max_length=250)
    nombre_completo = models.CharField(max_length=250)
    nombre_contacto = models.CharField(max_length=250)
    cargo = models.CharField(max_length=250)
    correo = models.EmailField(max_length=254, null=True, blank=True)
    pagina_web = models.URLField(max_length=200, null=True, blank=True)
    pagina_facebook = models.URLField(max_length=200, null=True, blank=True)
    twitter = models.URLField(max_length=200, null=True, blank=True)
    instragram = models.URLField(max_length=200, null=True, blank=True)
    otro = models.URLField(max_length=200, null=True, blank=True)
    telefono_fijo = models.CharField(max_length=50, null=True, blank=True)
    celular_movistar = models.CharField(max_length=50, null=True, blank=True)
    celular_claro = models.CharField(max_length=50, null=True, blank=True)
    direccion = models.TextField('Dirección física/sede')
    ambito_accion = MultiSelectField(choices=CHOICES_AMBITO)
    logo = ImageField(upload_to='logo/ficha_actor/', null=True, blank=True)
    objetivo = models.TextField('Objetivo de la organización')
    accion = models.TextField('Líneas de acción de la organización')

    def __str__(self):
        return self.nombre_siglas

    class Meta:
        verbose_name = 'Ficha de actor'
        verbose_name_plural = 'Fichas de actores'


class Proyectos(models.Model):
    nombre = models.CharField(max_length=250)
    fecha_inicio = models.DateField()
    fecha_finalizacion = models.DateField()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyectos'


class DatosDinamicosActor(models.Model):
    ficha_actor = models.ForeignKey(FichaActor)
    fecha = models.DateField()
    proyectos = models.ManyToManyField(Proyectos)
    personal = models.IntegerField('Numero de personal',null=True, blank=True)
    financiamiento = models.CharField(max_length=50)
    presupuesto_anual = models.FloatField(null=True, blank=True)
    municipios_atiende = models.ManyToManyField(Municipio)

    class Meta:
        verbose_name = 'Dato del actor'
        verbose_name_plural = 'Datos de los actores'

class CapsAtendidos(models.Model):
    ficha_actor = models.ForeignKey(FichaActor)
    municipio = models.ForeignKey(Municipio)
    numero = models.IntegerField()

    class Meta:
        verbose_name = 'Caps atendido'
        verbose_name_plural = 'Caps atendidos'







