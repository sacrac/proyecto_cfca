# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-13 19:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import multiselectfield.db.fields
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('lugar', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CapsAtendidos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Caps atendido',
                'verbose_name_plural': 'Caps atendidos',
            },
        ),
        migrations.CreateModel(
            name='DatosDinamicosActor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('personal', models.IntegerField(blank=True, null=True, verbose_name='Numero de personal')),
                ('financiamiento', models.CharField(max_length=50)),
                ('presupuesto_anual', models.FloatField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Dato del actor',
                'verbose_name_plural': 'Datos de los actores',
            },
        ),
        migrations.CreateModel(
            name='FichaActor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_siglas', models.CharField(max_length=250)),
                ('nombre_completo', models.CharField(max_length=250)),
                ('nombre_contacto', models.CharField(max_length=250)),
                ('cargo', models.CharField(max_length=250)),
                ('correo', models.EmailField(blank=True, max_length=254, null=True)),
                ('pagina_web', models.URLField(blank=True, null=True)),
                ('pagina_facebook', models.URLField(blank=True, null=True)),
                ('twitter', models.URLField(blank=True, null=True)),
                ('instragram', models.URLField(blank=True, null=True)),
                ('otro', models.URLField(blank=True, null=True)),
                ('telefono_fijo', models.CharField(blank=True, max_length=50, null=True)),
                ('celular_movistar', models.CharField(blank=True, max_length=50, null=True)),
                ('celular_claro', models.CharField(blank=True, max_length=50, null=True)),
                ('direccion', models.TextField(verbose_name='Direcci\xf3n f\xedsica/sede')),
                ('ambito_accion', multiselectfield.db.fields.MultiSelectField(choices=[(1, 'Local'), (2, 'Municipal'), (3, 'Nacional')], max_length=5)),
                ('logo', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='logo/ficha_actor/')),
                ('objetivo', models.TextField(verbose_name='Objetivo de la organizaci\xf3n')),
                ('accion', models.TextField(verbose_name='L\xedneas de acci\xf3n de la organizaci\xf3n')),
            ],
            options={
                'verbose_name': 'Ficha de actor',
                'verbose_name_plural': 'Fichas de actores',
            },
        ),
        migrations.CreateModel(
            name='Proyectos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
                ('fecha_inicio', models.DateField()),
                ('fecha_finalizacion', models.DateField()),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
            },
        ),
        migrations.AddField(
            model_name='datosdinamicosactor',
            name='ficha_actor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosActores.FichaActor'),
        ),
        migrations.AddField(
            model_name='datosdinamicosactor',
            name='municipios_atiende',
            field=models.ManyToManyField(to='lugar.Municipio'),
        ),
        migrations.AddField(
            model_name='datosdinamicosactor',
            name='proyectos',
            field=models.ManyToManyField(to='datosActores.Proyectos'),
        ),
        migrations.AddField(
            model_name='capsatendidos',
            name='ficha_actor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosActores.FichaActor'),
        ),
        migrations.AddField(
            model_name='capsatendidos',
            name='municipio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lugar.Municipio'),
        ),
    ]
