# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import FichaActor, Proyectos, DatosDinamicosActor, CapsAtendidos
from .forms import DatosActorAdminForm

class InlineDatosDinamicoActor(admin.TabularInline):
    form = DatosActorAdminForm
    model = DatosDinamicosActor
    extra = 1

class InlineCapsAtendido(admin.TabularInline):
    model = CapsAtendidos
    extra = 1

class FichaActorAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (('nombre_siglas', 'nombre_completo'), ('nombre_contacto', 'cargo'), ('correo', 'pagina_web'))
        }),
        ('Redes Sociales', {
            'fields': (('pagina_facebook', 'twitter'), ('instragram', 'otro')),
        }),
         ('Contactos', {
            'fields': (('telefono_fijo', 'celular_movistar', 'celular_claro'), 'direccion',('ambito_accion','logo'),'objetivo','accion'),
        }),
    )
    list_display = ('nombre_siglas','nombre_contacto', 'cargo','correo')
    list_filter = ('nombre_siglas', 'nombre_completo')
    inlines = [InlineDatosDinamicoActor,InlineCapsAtendido ]

# Register your models here.
admin.site.register(FichaActor, FichaActorAdmin)
admin.site.register(Proyectos)
#admin.site.register(DatosDinamicosActor)
#admin.site.register(CapsAtendidos)
