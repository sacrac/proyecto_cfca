from django.db import models
from lugar.models import Departamento, Municipio
from datosCaps.models import FichaCaps

# Create your models here.

class ListaParticipante(models.Model):
    fecha = models.DateField()
    nombre_actividad = models.CharField(max_length=140)
    local_actividad = models.CharField(max_length=140)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE)
    adjunto = models.FileField(upload_to='archivosExcel/', null=True, blank=True)
    
    class Meta:
        verbose_name = "Eventos/Actividades"
        verbose_name_plural = "Eventos/Actividades"

    def __str__(self):
        return self.nombre_actividad

    def get_absolute_url(self):
        return reverse("listaparticipante_detail", kwargs={"pk": self.pk})

CHOICES_SEXO = (("M","Masculino"), ("F", "Femenino"))

class AsistenciaParticipacion(models.Model):
    lista = models.ForeignKey(ListaParticipante, on_delete=models.CASCADE)
    nombre_apellido = models.CharField(max_length=150)
    sexo = models.CharField(choices=CHOICES_SEXO, max_length=1)
    cedula = models.CharField("Número de cedula", max_length=50, null=True, blank=True)
    caps = models.ForeignKey(FichaCaps, on_delete=models.CASCADE)
    cargo_junta_directiva = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Lista de asistencias"
        verbose_name_plural = "Lista de asistencias"

    def __str__(self):
        return self.nombre_apellido

    def get_absolute_url(self):
        return reverse("asistenciaparticipacion_detail", kwargs={"pk": self.pk})
