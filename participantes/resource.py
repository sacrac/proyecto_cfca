from import_export import resources
from .models import AsistenciaParticipacion, ListaParticipante

class AsistenciaResource(resources.ModelResource):
    
    class Meta:
        model = AsistenciaParticipacion


class ListaResource(resources.ModelResource):
    
    class Meta:
        model = ListaParticipante