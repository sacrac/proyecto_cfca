from django import forms
from encuestas.models import Survey


class EncuestaForms(forms.Form):
	encuestas = forms.ModelMultipleChoiceField(queryset=Survey.objects.all())
		