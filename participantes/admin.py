from django.contrib import admin
from .models import ListaParticipante, AsistenciaParticipacion
from .resource import AsistenciaResource, ListaResource
from import_export.admin import ImportExportModelAdmin, ImportMixin

class AsistenciaParticipacionInline(admin.TabularInline):
    '''Tabular Inline View for asistentes '''
    model = AsistenciaParticipacion
    extra = 1
    raw_id_fields = ('caps',)

class ListaParticipanteAdmin(admin.ModelAdmin):
    resource_class = ListaResource
    inlines = [AsistenciaParticipacionInline]
    #empty_value_display = 'No tiene adjunto'
    list_display = ('nombre_actividad','fecha','local_actividad','municipio','adjunto_tag')
    list_filter = ('fecha','departamento')
    search_fields = ('nombre_actividad','local_actividad',)

    def adjunto_tag(self,obj):
        if obj.adjunto != '':
            return True
        return False
    adjunto_tag.boolean = True
    adjunto_tag.short_description = 'Adjunto'

class AsistenciaAdmin(ImportMixin, admin.ModelAdmin):
    resource_class = AsistenciaResource
    raw_id_fields = ('caps',)



# Register your models here.
admin.site.register(ListaParticipante, ListaParticipanteAdmin)
admin.site.register(AsistenciaParticipacion, AsistenciaAdmin)