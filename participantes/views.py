from django.shortcuts import render
from django.db.models import F, Count, Value
from .forms import EncuestaForms
from encuestas.models import Response
from lugar.models import Municipio

# Create your views here.
def _queryset_filtrado_survey(request):
    params = {}

    if 'encuestas' in request.session:
        params['survey__in'] = request.session['encuestas']


    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print(params)

    return Response.objects.filter(**params)

def consultar_survey(request, template='filtros/consultar_survey.html'):
    if request.method == 'POST':
        form = EncuestaForms(request.POST)
        if form.is_valid():
            request.session['encuestas'] = form.cleaned_data['encuestas']
           
            centinela = 1
            print("fue valido")
        else:
            centinela = 0
            print("No fue valido")
    else:
        form = EncuestaForms()
        centinela = 0

        if 'encuestas' in request.session:
            try:
                del request.session['encuestas']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})

def salidas_filtro_encuesta(request, template="filtros/filtro_encuesta.html"):
    filtro2 = _queryset_filtrado_survey(request)
    filtro = filtro2.select_related('comunidad','municipio','survey','caps')
    # print("########################")
    # print(request.session['encuestas'][0])
    # print(request.session['encuestas'][1])
    # print("########################")
    variables_encuestas = request.session['encuestas']

    tabla_conteo = {}
    for obj in Municipio.objects.all():
        hombre_pre = filtro.filter(municipio_id=obj.id,
                                   sexo=1,
                                   survey=request.session['encuestas'][0]).count()
        mujer_pre = filtro.filter(municipio_id=obj.id,
                                  sexo=2,
                                  survey=request.session['encuestas'][0]).count()
        total_pre = hombre_pre + mujer_pre
        hombre_post = filtro.filter(municipio_id=obj.id,
                                   sexo=1,
                                   survey=request.session['encuestas'][1]).count()
        mujer_post = filtro.filter(municipio_id=obj.id,
                                   sexo=2,
                                   survey=request.session['encuestas'][1]).count()
        total_post = hombre_post + mujer_post
        if total_post != 0 and total_pre != 0:
            tabla_conteo[obj.nombre] = [hombre_pre,mujer_pre,total_pre,
                                    hombre_post,mujer_post,total_post]
    
    g_hombre_pre = 0
    g_mujer_pre = 0
    g_total_pre = 0
    g_hombre_post = 0
    g_mujer_post = 0
    g_total_post = 0
    for k,v in tabla_conteo.items():
        g_hombre_pre += v[0]
        g_mujer_pre += v[1]
        g_total_pre += v[2]
        g_hombre_post += v[3]
        g_mujer_post += v[4]
        g_total_post += v[5]


    return render(request,template,{'filtro':tabla_conteo,
                                    'conteo':filtro.count(),
                                    'pre':request.session['encuestas'][0],
                                    'post':request.session['encuestas'][1],
                                    'g_hombre_pre':g_hombre_pre,
                                    'g_mujer_pre':g_mujer_pre,
                                    'g_total_pre':g_total_pre,
                                    'g_hombre_post':g_hombre_post,
                                    'g_mujer_post':g_mujer_post,
                                    'g_total_post':g_total_post})