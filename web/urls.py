from django.conf.urls import url
from django.views.generic import TemplateView
from django.views.decorators.cache import cache_page

from . import views
from beneficiarios.views import (BeneficiarioHome, 
                                BeneficiarioInversion, 
                                BeneficiarioActividad, 
                                reporte_mejoras_perforacion)
from actividadgerencial.views import ActvidadesProgramatica, ResultadosView
from modulofinanciero.views import *
from peticiones.views import *
from participantes.views import *

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="index"),
    url(r'^search/$', views.SearchResultsView.as_view(), name='search_results'),
    url(r'^lista-noticias/$', views.NewsListView.as_view(), name="list_news"),
    url(r'^lista-noticias-vidas/$', views.NewsListVidasView.as_view(), name="list_news-vidas"),
    url(r'^noticia/(?P<slug>[-\w]+)/$', views.NewsDetailView.as_view(), name="detail_news"),
    url(r'^documentos/$', views.DocumentListView.as_view(), name="list_documents"),
    #url(r'^galerias/$', views.GaleriaListView.as_view(), name="list_gallery"),
    url(r'^galerias/$', views.GaleriaListView.as_view(), name="list_gallery"),
    url(r'^videos/$', views.VideoListView.as_view(), name="list_videos"),
    url(r'^audios/$', TemplateView.as_view(template_name="web/audios_list.html")),
    url(r'^eventos/$', views.EventosListView.as_view(), name="list_eventos"),
    url(r'^eventos/(?P<slug>[-\w]+)/$', views.EventoDetailView.as_view(), name="detail_eventos"),
    url(r'^tags/(?P<slug>[-\w]+)/$', views.TagView.as_view(), name="tags_news"),
    url(r'^tematicas/(?P<tematicas>[-\w]+)/$', views.TematicaView.as_view(), name="tematicas_news"),
    url(r'^faq/$', TemplateView.as_view(template_name="web/faq.html")),
    url(r'^consulta/$', TemplateView.as_view(template_name="seguimiento/segimiento_consulta.html")),
    url(r'^beneficiario/$', BeneficiarioHome.as_view(), name='beneficiario'),
    url(r'^beneficiario-inversion/$', BeneficiarioInversion.as_view(), name='beneficiario-inversion'),
    # url(r'^beneficiario-actividad/$', BeneficiarioActividad.as_view(), name='beneficiario-actividad'),
    url(r'^resultado/$', ResultadosView, name="resultados"),
    url(r'^impacto/$', TemplateView.as_view(template_name="seguimiento/segimiento_impacto.html")),
    url(r'^financiero/$', financiero, name='modulo_financiero'),
    url(r'^actividades/$', ActvidadesProgramatica.as_view(), name="actividades-programaticas"),
    url(r'^perfilcaps/$', TemplateView.as_view(template_name="seguimiento/perfilcaps.html")),
    url(r'^peticiones/$', peticiones, name="peticiones"),
    url(r'^peticiones/make/$', save_peticiones, name="make_peticiones"),
    url(r'^peticiones/thank/$', TemplateView.as_view(template_name="peticiones/thank.html"), name="thank_peticiones"),
    url(r'^reporte-mejoras/(?P<year>\d+)/$', reporte_mejoras_perforacion, name="reporte"),
    url(r'^covid19-nicaragua/$', TemplateView.as_view(template_name="web/covid.html")),
    url(r'^filtro-encuestas/$', consultar_survey, name="consultar-survey"),
    url(r'^salida-filtro-encuestas/$', salidas_filtro_encuesta, name="salida-consultar-survey"),

]
