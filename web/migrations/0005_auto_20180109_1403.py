# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-09 20:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_galerias'),
    ]

    operations = [
        migrations.AddField(
            model_name='galerias',
            name='fecha',
            field=models.DateField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='galeriasfotos',
            name='tematicas',
            field=models.ManyToManyField(blank=True, to='web.Tematicas'),
        ),
    ]
