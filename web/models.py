# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.dispatch import receiver

from sorl.thumbnail import ImageField
from taggit_autosuggest.managers import TaggableManager
from location_field.models.plain import PlainLocationField

from lugar.models import Departamento, Municipio
from newsletters.models import CorreoEnvioNotificacion
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

# Create your models here.
@python_2_unicode_compatible
class Tematicas(models.Model):
    nombre = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)
    color = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.nombre))
        super(Tematicas, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Tematica'
        verbose_name_plural = 'Tematicas'

@python_2_unicode_compatible
class ArchivosAdjuntos(models.Model):
    nombre = models.CharField(max_length=250)
    credito = models.CharField(max_length=250)
    archivo = models.FileField(upload_to='adjuntos/')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Archivos adjunto'
        verbose_name_plural = 'Archivos adjuntos'

@python_2_unicode_compatible
class GaleriasFotos(models.Model):
    titulo = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)
    fecha = models.DateTimeField('Fecha', auto_now=True)
    credito = models.CharField(max_length=250)
    tematicas = models.ManyToManyField(Tematicas, blank=True)
    archivo = ImageField(upload_to='galeria/')
    infografia_mostrar = models.BooleanField(default=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(GaleriasFotos, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Galeria de foto'
        verbose_name_plural = 'Galerias de fotos'

    @models.permalink
    def get_absolute_url(self):
        return ("galeria-detalle", [self.slug,])

CHOICE_TIPO_NOTICIA = ((1,'Infórmate'),(2,'Vida de CAPS'),)
CHOICE_PERMISO_PUBLICACION = ((1,'Publicada'),(2,'En desarrollo'),)

@python_2_unicode_compatible
class Noticias(models.Model):
    titulo = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)
    fecha_publicacion = models.DateTimeField('Fecha de publicación')
    en_portada = models.BooleanField(default=False)
    permanente_en_inicio = models.BooleanField(default=False)
    foto_principal = ImageField(upload_to='foto/noticias/', null=True, blank=True)
    descripcion_foto = models.CharField(max_length=150, null=True, blank=True)
    video_principal = models.URLField(null=True, blank=True)
    credito_foto = models.CharField(max_length=250, null=True, blank=True)
    texto_noticia = models.TextField('Texto de la noticia')
    autor_noticia = models.ForeignKey(User, verbose_name='Autor de la noticia')
    seccion = models.IntegerField(choices=CHOICE_TIPO_NOTICIA)
    tematicas = models.ManyToManyField(Tematicas)
    tags = TaggableManager('palabras_claves')
    adjuntar = GenericRelation(ArchivosAdjuntos)
    galeria = GenericRelation(GaleriasFotos)
    publicar = models.IntegerField(choices=CHOICE_PERMISO_PUBLICACION)
    enviar_notificacion = models.BooleanField(default=False)

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(Noticias, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Noticia'
        verbose_name_plural = 'Noticias'


    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('detail_news', args=[str(self.slug)])
        #return ("detail_news", [self.slug,])

    def get_tematica(self):
        tema = self.tematicas.all()[0].nombre
        return tema

    def get_tematica_color(self):
        tema = self.tematicas.all()[0].color
        return tema

    def todas_tematicas(self):
        all_temas = ','.join([obj.nombre for obj in self.tematicas.all()])
        return all_temas

@receiver(post_save, sender=Noticias)
def send_new_message_notification(sender, created, **kwargs):
    message = kwargs['instance']
    obj = User.objects.get(pk=message.autor_noticia.pk)
    if message.publicar == 1 and message.enviar_notificacion == True:
        #print("enviar mensaje por correo!")
        site = Site.objects.get_current()
        contenido = render_to_string('web/notify_new_nota.txt', {'nota': message,
                                     'url': '%s/noticia/%s' % (site, message.slug),
                                     })
        send_mail('Noticias CAPS Nicaragua', contenido, 'proyecto.cfca@gmail.com', 
            [user.correo_electronico for user in CorreoEnvioNotificacion.objects.all() if user.correo_electronico])
   

@python_2_unicode_compatible
class VoceDelAgua(models.Model):
    titulo = models.CharField('Autor', max_length=250)
    fecha = models.DateTimeField(auto_now=True)
    foto_principal = ImageField(upload_to='foto/voces_del_agua/', null=True, blank=True)
    credito_foto = models.CharField('lugar', max_length=250, null=True, blank=True)
    descripcion = models.TextField('Descripción')

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Voces del agua'
        verbose_name_plural = 'Voces del agua'

    @models.permalink
    def get_absolute_url(self):
        return reverse('vocesdelagua-detalle', args=[str(self.id)])

CHOICE_TIPO_PUBLICACION = ((1,'Revista'),(2,'Manual'),(3,'Investigación'),(4,'Informe'),)

@python_2_unicode_compatible
class Publicaciones(models.Model):
    titulo = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)
    fecha = models.DateTimeField(auto_now=True)
    imagen_portada = ImageField(upload_to='foto/publicaciones/', null=True, blank=True)
    autor = models.CharField(verbose_name='Autor de la publicación', max_length=250)
    lugar = models.CharField(max_length=250, null=True, blank=True)
    descripcion = models.TextField('Descripción')
    tematicas = models.ManyToManyField(Tematicas)
    palabras_claves = TaggableManager()
    edicion = models.CharField('Edición', max_length=50)
    tipo_publicacion = models.IntegerField(choices=CHOICE_TIPO_PUBLICACION, verbose_name='Tipo de publicación')
    adjuntar = GenericRelation(ArchivosAdjuntos)

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(Publicaciones, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Publicación'
        verbose_name_plural = 'Publicaciones'

    @models.permalink
    def get_absolute_url(self):
        return ("publicacion-detalle", [self.slug,])

    def todas_tematicas(self):
        all_temas = ', '.join([obj.nombre for obj in self.tematicas.all()])
        return all_temas

    def all_tematicas(self):
        all_temas = ' '.join([slugify(obj.nombre) for obj in self.tematicas.all()])
        return all_temas

class ListasVideos(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Listas para los videos"


@python_2_unicode_compatible
class Videos(models.Model):
    titulo = models.CharField(max_length=250)
    lista_categoria = models.ForeignKey(ListasVideos, null=True, blank=True)
    slug = models.SlugField(max_length=250, editable=False)
    fecha = models.DateTimeField(auto_now=True)
    imagen_portada = ImageField(upload_to='foto/videos/', null=True, blank=True)
    video_url = models.URLField()
    descripcion = models.TextField('Descripción')
    tematicas = models.ManyToManyField(Tematicas)
    palabras_claves = TaggableManager()

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(Videos, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'

    @models.permalink
    def get_absolute_url(self):
        return ("video-detalle", [self.slug,])


@python_2_unicode_compatible
class Eventos(models.Model):
    titulo = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)
    fecha = models.DateTimeField()
    imagen_portada = ImageField(upload_to='foto/eventos/', null=True, blank=True)
    autor = models.ForeignKey(User, verbose_name='Autor de la publicación')
    lugar = models.CharField(max_length=250, null=True, blank=True)
    departamento = models.ForeignKey(Departamento, null=True)
    municipio = models.ForeignKey(Municipio, null=True)
    descripcion = models.TextField('Descripción')
    tematicas = models.ManyToManyField(Tematicas)
    nombre_contacto = models.CharField('Nombre del contacto',max_length=250)
    telefono = models.CharField('Teléfono',max_length=50)
    correo_electronico = models.EmailField()
    adjuntar = GenericRelation(ArchivosAdjuntos)
    location = PlainLocationField(based_fields=['lugar'], zoom=7, null=True, blank=True)

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(Eventos, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'

    @models.permalink
    def get_absolute_url(self):
        return ("evento-detalle", [self.slug,])

@python_2_unicode_compatible
class Categorias(models.Model):
    categoria = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)

    def __str__(self):
        return self.categoria

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.categoria))
        super(Categorias, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'



@python_2_unicode_compatible
class Galerias(models.Model):
    titulo = models.CharField('Titulo de la galeria', max_length=250)
    categoria = models.ForeignKey(Categorias, null=True, blank=True)
    slug = models.SlugField(max_length=250, editable=False)
    fecha = models.DateField(auto_now=True)
    galeria = GenericRelation(GaleriasFotos)

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(Galerias, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Galeria de foto'
        verbose_name_plural = 'Galerias de fotos'

    @models.permalink
    def get_absolute_url(self):
        return ("galeria-detalle", [self.slug,])
