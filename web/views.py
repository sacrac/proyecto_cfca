# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from django.db.models import Count, Q, Sum, Value as V, F
from django.db.models.functions import Coalesce
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.conf import settings
import datetime

from .models import (Noticias, Eventos, GaleriasFotos, VoceDelAgua, Tematicas,
                     Publicaciones,Videos, Galerias)
from actividades.models import Actividad
from beneficiarios.models import BeneficiariosCaps

# Create your views here.
class MenuMixin(object):

    def get_context_data(self, **kwargs):
        context = super(MenuMixin, self).get_context_data(**kwargs)
        context['publicaciones'] = Noticias.objects.prefetch_related('tematicas').order_by('-fecha_publicacion')[:6]

        return context

class BusquedaMixin(object):

    def get_context_data(self, **kwargs):
        context = super(BusquedaMixin, self).get_context_data(**kwargs)
        all_tematicas = Tematicas.objects.all()
        count_tematicas = all_tematicas.annotate(num_times=Count('noticias__tematicas'))
        mydict = {}
        for tema in count_tematicas:
            mydict[tema.nombre] = [tema.num_times,tema.slug]

        context['categorias'] = mydict
        return context

#@method_decorator(login_required, name='dispatch')
class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        query_actividad = Actividad.objects.select_related()
        query_beneficiario = BeneficiariosCaps.objects.select_related()
        all_news = Noticias.objects.filter(publicar=1).prefetch_related('tematicas').order_by('-fecha_publicacion')
        context['noticias_principales'] = all_news.filter(en_portada=True, permanente_en_inicio=False,tematicas__slug__in=['tic','inversiones','umas','alianzas','genero']).distinct()[:5]
        context['noticia_permanente'] = all_news.filter(en_portada=True,permanente_en_inicio=True,tematicas__slug__in=['tic','inversiones','umas','alianzas','genero']).first()
        context['noticias_inversion'] = all_news.filter(tematicas__slug__contains='inversiones')[:2]
        context['noticias_actividades'] = all_news.filter(tematicas__slug__contains='alianzas')[:5]
        context['eventos'] = Eventos.objects.order_by('-fecha')[:6]
        context['galerias'] = Galerias.objects.exclude(id=11)[:5]
        context['galerias_index'] =GaleriasFotos.objects.filter(infografia_mostrar=True)
                
        context['voces'] = VoceDelAgua.objects.order_by('-id')
        #webpush_settings = getattr(settings, 'WEBPUSH_SETTINGS', {})
        #context['vapid_key'] = webpush_settings.get('VAPID_PUBLIC_KEY')
        #caps_groups = "caps_nicaragua"
        #context['group'] = caps_groups
        context['videos'] = Videos.objects.order_by('-id')[:5]
        actividades = Actividad.objects.filter(modulos__in=[1,2,3,4,5,6]).aggregate(total=Count('id'),hombre=Sum('numero_hombres'),mujer=Sum('numero_mujeres'))


        inversion_simas = Actividad.objects.filter(tipo_actividad__in=[12,13]).aggregate(total=Coalesce(Sum('ejecutado'),V(0)))['total']
        #print(ejecutado_simas)
        total_inversion = ( query_actividad
                .filter(tipo_actividad__in=[12,13])
                .aggregate(
                    alcaldia=Coalesce(Sum(F('presupuestoinfraestructura__alcaldia')),V(0)),
                    comunidad=Coalesce(Sum(F('presupuestoinfraestructura__comunidad')),V(0)),
                    otros=Coalesce(Sum(F('presupuestoinfraestructura__otros')),V(0)),
                )
             )
        

        total_ong = query_actividad.filter(tipo_actividad__in=[12,13]).aggregate(
                    ong=Coalesce(Sum(F('financiamientoinstituciones__monto')),V(0))
                )

        cofinanciado = total_inversion['alcaldia'] + total_inversion['comunidad'] + \
                                    total_inversion['otros'] + total_ong['ong']
             

        tabla_beneficiario_mejora_perfo = []
        for acti in query_actividad.filter(tipo_actividad__in=[12,13]):
            total_personas = query_beneficiario.filter(comunidad=acti.comunidad).count()
            #total_hombres = query_beneficiario.filter(comunidad=acti.comunidad, sexo=1).count()
            #total_mujeres = query_beneficiario.filter(comunidad=acti.comunidad, sexo=2).count()
            #menores = [obj for obj in query_beneficiario.filter(comunidad=acti.comunidad) if obj.edad_persona() <=15]
            tabla_beneficiario_mejora_perfo.append([total_personas])#,total_hombres,total_mujeres,len(menores)])

        context['participantes'] = actividades['total']
        context['participantes_hombres'] = actividades['hombre']
        context['participantes_mujeres'] = actividades['mujer']
        context['total_ejecutado'] = inversion_simas
        context['cofinanciado'] = cofinanciado
        #context['total_bene_comunidad'] = [sum(x) for x in zip(*tabla_beneficiario_mejora_perfo)]

        return context

class NewsListView(BusquedaMixin, ListView):
    model = Noticias
    queryset =  Noticias.objects.filter(publicar=1,seccion=1).order_by('-fecha_publicacion')
    template_name = 'web/noticias_list.html'
    #paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)

        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]
        #context['tags'] = TaggedItem.objects.all()

        return context

class NewsListVidasView(BusquedaMixin, ListView):
    model = Noticias
    queryset =  Noticias.objects.filter(publicar=1,seccion=2).order_by('-fecha_publicacion')
    template_name = 'web/noticias_vidas_list.html'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(NewsListVidasView, self).get_context_data(**kwargs)

        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]
        #context['tags'] = TaggedItem.objects.all()

        return context

class NewsDetailView(BusquedaMixin, DetailView):
    model = Noticias
    template_name = 'web/noticias_detail.html'

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        #print self.object.tematicas.all()
        news_related = Noticias.objects.filter(publicar=1,tematicas__in=self.object.tematicas.all()).exclude(id=self.object.id)[:5]
        context['noticias_relacionas'] = news_related
        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]

        return context
import itertools
class DocumentListView(ListView):
    model = Publicaciones
    template_name = "web/documentos_list.html"
    paginate_by = 8
    ordering = ['-fecha']

    def get_context_data(self, **kwargs):
        context = super(DocumentListView, self).get_context_data(**kwargs)
        temas = []
        #print(self.object_list)
        for obj in self.object_list:
            ab = itertools.chain(list(obj.tematicas.values_list('id',flat=True)))
            temas.extend(list(ab))
        context['tematica'] = Tematicas.objects.filter(id__in=list(set(temas)))

        return context

class VideoListView(ListView):
    model = Videos
    template_name = "web/videos_list.html"
    paginate_by = 8
    ordering = ['-id']

class GaleriaListView(ListView):
    model = Galerias
    template_name = "web/galerias_mobil.html"
    paginate_by = 12

# import pyautogui
# def GaleriaListView(request):
#     object_list = Galerias.objects.all()
#     template = ''
#     ancho = pyautogui.size().width
#     if ancho > 1000:
#         template = 'web/galerias_list.html'
#     else:
#         template = 'web/galerias_mobil.html'
#     return render(request, template, locals())

class EventosListView(ListView):
    model = Eventos
    template_name = 'web/eventos_list.html'
    paginate_by = 8
    ordering = ['-fecha']

class EventoDetailView(DetailView):
    model = Eventos
    template_name = 'web/eventos_detail.html'

    def get_context_data(self, **kwargs):
        context = super(EventoDetailView, self).get_context_data(**kwargs)
        hoy = datetime.datetime.now()
        context['next_eventos'] = Eventos.objects.filter(fecha__gte=hoy).order_by('-fecha').exclude(id=self.object.id)[:3]
        context['last_noticias'] = Noticias.objects.order_by('-fecha_publicacion')[:3]
        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]
        try:
            context['location'] = self.object.location.split(',')
        except:
            pass

        return context


class TagView(BusquedaMixin, ListView):
    model = Noticias
    template_name = 'web/tag_list.html'

    def get_queryset(self):
         queryset = super(TagView, self).get_queryset()
         return queryset.filter(tags__slug__in=[self.kwargs['slug']])

    def get_context_data(self, **kwargs):
        context = super(TagView, self).get_context_data(**kwargs)

        context['tags'] = self.kwargs['slug']
        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]
        return context


class TematicaView(BusquedaMixin, ListView):
    model = Noticias
    template_name = 'web/tematica_list.html'

    def get_queryset(self):
         queryset = super(TematicaView, self).get_queryset()
         return queryset.filter(tematicas__slug__in=[self.kwargs['tematicas']])

    def get_context_data(self, **kwargs):
        context = super(TematicaView, self).get_context_data(**kwargs)

        context['tematicas'] = self.kwargs['tematicas']
        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]
        return context

class SearchResultsView(BusquedaMixin, ListView):
    model = Noticias
    template_name = 'web/search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        
        try:
            object_list = Noticias.objects.filter(
                Q(titulo__icontains=query) | Q(texto_noticia__icontains=query)
            )
        except:
            object_list = []

        return object_list

    def get_context_data(self, **kwargs):
        context = super(SearchResultsView, self).get_context_data(**kwargs)
        context['ultimos_videos'] = Videos.objects.order_by('-fecha')[:5]
        return context
