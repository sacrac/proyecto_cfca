from django import template
from django.core.files.storage import default_storage

register = template.Library()

@register.filter(name='file_exists')
def file_exists(filepath):
    if default_storage.exists(filepath):
        return filepath


@register.filter(name='multiplicar')
def multiplicar(data1, data2):
    
    return data1 * data2

@register.filter(name='ultimo')
def ultimo(data):
    if len(data) <= 0:
        return 0
    else:
        return data[-1]