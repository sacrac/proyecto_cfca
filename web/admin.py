# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.contrib import admin
from django import forms
from django.contrib.contenttypes.admin import  GenericTabularInline
from django.utils.html import format_html

from ckeditor_uploader.widgets import CKEditorUploadingWidget

from .models import (Tematicas, ArchivosAdjuntos,
                     GaleriasFotos, Noticias, VoceDelAgua,
                     Publicaciones, Videos, Eventos, Galerias, ListasVideos,
                     Categorias)
from .forms import NoticiasAdminForm, TematicasAdminForm, FormEventoMapa

class FlatPageForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = FlatPage
        fields = '__all__'


class PageAdmin(FlatPageAdmin):
    form = FlatPageForm

class AdminTematicas(admin.ModelAdmin):
    form = TematicasAdminForm
    list_display = ('nombre', 'slug', 'colored_name')
    search_fields = ('nombre',)
    list_filter = ('nombre',)

    def colored_name(self, obj):
        return format_html('<span style="background-color:{};width:40px; heigth:20px; color:{};">COLOR</span>',obj.color,obj.color)
    colored_name.short_description = 'Color visual'

class InlineArchivosAdjuntos(GenericTabularInline):
    model = ArchivosAdjuntos
    extra = 1

class InlineGaleriasFotos(GenericTabularInline):
    form = TematicasAdminForm
    model = GaleriasFotos
    extra = 1

class AdminNoticias(admin.ModelAdmin):
    form = NoticiasAdminForm
    fields = (
                'titulo', 'fecha_publicacion',
               ('en_portada','permanente_en_inicio'),'foto_principal', ('descripcion_foto','credito_foto'),
               'video_principal','texto_noticia','autor_noticia','seccion',
               'tematicas','tags',
               'publicar','enviar_notificacion'
            )
    list_display = ('titulo', 'fecha_publicacion',
                    'en_portada', 'autor_noticia',
                    'publicar','temas_noticias')
    search_fields = ('titulo',)
    list_filter = ('tematicas','publicar','fecha_publicacion')
    inlines = [InlineArchivosAdjuntos, InlineGaleriasFotos]
    # def bool_publicacion(self, obj):
    #     if obj.adjuntar.count() > 0:
    #         return True
    #     return False
    # bool_publicacion.boolean = True
    # bool_publicacion.short_description = 'Tiene adjunto'
    def temas_noticias(self, obj):
        return ",".join([p.nombre for p in obj.tematicas.all()])
    #temas_noticias.boolean = True
    temas_noticias.short_description = 'Tematicas'

class AdminVoceDelAgua(admin.ModelAdmin):
    list_display = ('titulo',)
    search_fields = ('titulo',)
    list_filter = ('fecha',)

class AdminPublicaciones(admin.ModelAdmin):
    list_display = ('titulo', 'autor', 'edicion', 'bool_publicacion')
    search_fields = ('titulo',)
    list_filter = ('tematicas','fecha')
    inlines = [InlineArchivosAdjuntos]

    def bool_publicacion(self, obj):
        if obj.adjuntar.count() > 0:
            return True
        return False
    bool_publicacion.boolean = True
    bool_publicacion.short_description = 'Tiene adjunto'

class AdminVideos(admin.ModelAdmin):
    list_display = ('titulo',)
    search_fields = ('titulo',)
    list_filter = ('fecha',)

# class AdminEventos(admin.ModelAdmin):
#     form = FormEventoMapa
#     list_display = ('titulo',)
#     search_fields = ('titulo',)
#     list_filter = ('fecha',)

class InlineGaleria(GenericTabularInline):
    model = GaleriasFotos
    extra = 1

class AdminGalerias(admin.ModelAdmin):
    list_display = ('titulo',)
    search_fields = ('titulo',)
    list_filter = ('fecha',)
    inlines = [InlineGaleria]

class AdminEventos(admin.ModelAdmin):
    form = FormEventoMapa
    fields = (
              ('titulo', 'imagen_portada'), 'fecha',
               ('autor', 'departamento','municipio'),
               'descripcion','tematicas',('nombre_contacto',
               'telefono','correo_electronico'),
               'lugar', 'location'
            )
    list_display = ('titulo','fecha',)
    search_fields = ('titulo',)
    list_filter = ('fecha',)


# Register your models here.
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, PageAdmin)
admin.site.register(Tematicas, AdminTematicas)
#admin.site.register(ArchivosAdjuntos)
admin.site.register(Categorias)
admin.site.register(Noticias, AdminNoticias)
admin.site.register(VoceDelAgua)
admin.site.register(Publicaciones, AdminPublicaciones)
admin.site.register(Videos)
admin.site.register(Eventos, AdminEventos)
admin.site.register(Galerias, AdminGalerias)
admin.site.register(ListasVideos)
