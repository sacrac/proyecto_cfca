from django import forms
from django.forms.widgets import TextInput
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from .models import Noticias, Tematicas, Eventos

class NoticiasAdminForm(forms.ModelForm):
    texto_noticia = forms.CharField(widget=CKEditorUploadingWidget())
    tematicas = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Tematicas.objects.all())
    class Meta:
        model = Noticias
        fields = '__all__'


class TematicasAdminForm(forms.ModelForm):
    #tematicas = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Tematicas.objects.all())
    class Meta:
        model = Tematicas
        fields = '__all__'
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }

class FormEventoMapa(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormEventoMapa, self).__init__(*args, **kwargs)
        self.initial['location'] = '12.11586547566659,-86.2921142578125'

    class Meta:
        model = Eventos
        fields = '__all__'
