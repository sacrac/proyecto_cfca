import logging

from django.conf import settings
from django.core.mail import send_mail

from .models import CorreoEnvioNotificacion

logger = logging.getLogger('django')


class BaseMailer():
    def __init__(self, to_email, subject, message, html_message):
        self.to_email = to_email
        self.subject = subject
        self.message = message
        self.html_message = html_message

    def send_email(self):
        send_mail(
            subject=self.subject,
            message=self.message,
            html_message=self.html_message,
            from_email='noreply@simas.org.ni',
            recipient_list=[obj.correo_electronico for obj in CorreoEnvioNotificacion.objects.all()],
            fail_silently=False,
        )

class NewNoticia(BaseMailer):
    def __init__(self, to_email):
        super().__init__(to_email,  
         subject='Nueva Noticia', 
         message='Hay una nueva noticia que le puede interesar leer', 
         html_message='<p>Prueba para ver como poner el contenido.</p>')



        