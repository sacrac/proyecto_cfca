from django.db import models

# Create your models here.

class CorreoEnvioNotificacion(models.Model):
    nombre = models.CharField(max_length=250)
    cargo = models.CharField(max_length=250)
    correo_electronico = models.EmailField()
    

    class Meta:
        verbose_name = "Correo Envio Notificacion"
        verbose_name_plural = "Correos Envio Notificaciones"

    def __str__(self):
        return self.nombre
