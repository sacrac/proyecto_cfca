from django.contrib import admin
from .models import CorreoEnvioNotificacion

class CorreosAdmin(admin.ModelAdmin):
    list_display = ('nombre','cargo','correo_electronico')
    search_fields = ('nombre', 'correo_electronico')

# Register your models here.
admin.site.register(CorreoEnvioNotificacion, CorreosAdmin)