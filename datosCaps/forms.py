from django import forms
from .models import AsistenciaTecnica, FichaCaps
from lugar.models import Comunidad, Municipio, Departamento

class PerfilAdminForm(forms.ModelForm):
    class Meta:
        model = FichaCaps
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PerfilAdminForm, self).__init__(*args, **kwargs)
        self.fields['comunidad'].queryset = Comunidad.objects.prefetch_related('municipio','municipio__departamento').all()
        self.fields['municipio'].queryset = Municipio.objects.select_related('departamento').all()
        self.fields['departamento'].queryset = Departamento.objects.select_related('pais').all()


class AsistenciaInlineAdminForm(forms.ModelForm):
    class Meta:
        model = AsistenciaTecnica
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AsistenciaInlineAdminForm, self).__init__(*args, **kwargs)
        self.initial = {
                'opcion': 1,
                'opcion': 2,
            }



CHOICE_MAYOR_COMPLEJIDAD = (
    ('','-----'),
    ("1",'Mini acueducto por bombeo eléctrico solar (MABE-ES)'),
    ("2",'Mini acueducto por bombeo eléctrico comercial (MABE-EC)'),
    ("3",'Mini-acueducto por gravedad (MAG)'),
    ("4",'Pozo excavado con bomba de mano (PEBM)'),
    ("5",'Pozo perforado con bomba de mano (PPBM)'),
)

CHOICE_SI_NO = (
    ('','-----'),
    (1,'Si'),
    (2,'No'),
   
)

class FiltroMapaForms(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FiltroMapaForms, self).__init__(*args, **kwargs)
        self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['tipo_sistema'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['legalizado'].widget.attrs.update({'class': 'form-control'})
        self.fields['legal_nacional'].widget.attrs.update({'class': 'form-control'})
        self.fields['perforacion'].widget.attrs.update({'class': 'form-control'})
        self.fields['mejora'].widget.attrs.update({'class': 'form-control'})
       
        if 'municipio' in self.data:
            try:
                departamento_id = int(self.data.get('departamento'))
                self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=departamento_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty Comunidad queryset   

    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.none(), required=False)
    tipo_sistema = forms.ChoiceField(choices=CHOICE_MAYOR_COMPLEJIDAD, required=False)
    legalizado = forms.BooleanField(required=False)
    legal_nacional = forms.BooleanField(required=False)
    perforacion = forms.BooleanField(required=False)
    mejora = forms.BooleanField(required=False)



CHOICE_ANIO = (
    ('','-----'),
    (2017,'2017'),
    (2018,'2018'),
    (2019,'2019'),
    (2020,'2020'),
    (2021,'2021'),
    (2022,'2022'),
)

class FiltroActividadMapaForms(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FiltroActividadMapaForms, self).__init__(*args, **kwargs)
        #self.fields['fecha'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        #self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        #self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['perforacion'].widget.attrs.update({'class': 'form-control'})
        self.fields['mejora'].widget.attrs.update({'class': 'form-control'})
       
        # if 'municipio' in self.data:
        #     try:
        #         departamento_id = int(self.data.get('departamento'))
        #         self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=departamento_id).order_by('nombre')
        #     except (ValueError, TypeError):
        #         pass  # invalid input from the client; ignore and fallback to empty Comunidad queryset   

    #fecha = forms.ChoiceField(choices=CHOICE_ANIO, required=False)
    #departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), required=False)
    #municipio = forms.ModelChoiceField(queryset=Municipio.objects.none(), required=False)
    perforacion = forms.BooleanField(required=False)
    mejora = forms.BooleanField(required=False)




