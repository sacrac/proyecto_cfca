# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from nested_inline.admin import NestedStackedInline, NestedTabularInline, NestedModelAdmin
from .models import *
from .forms import AsistenciaInlineAdminForm, PerfilAdminForm
from django.forms.models import BaseModelFormSet
from django.utils.functional import curry
from django.forms.models import BaseInlineFormSet
from django.forms.models import ModelForm

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from django.db.models import Count, OuterRef, Subquery
# class CustomInlineFormset(BaseInlineFormSet):
#     """
#     Custom formset that support initial data
#     """

#     def initial_form_count(self):
#         """
#         set 0 to use initial_extra explicitly.
#         """
#         if self.initial_extra:
#             return 0
#         else:
#             return BaseInlineFormSet.initial_form_count(self)

#     def total_form_count(self):
#         """
#         here use the initial_extra len to determine needed forms
#         """
#         if self.initial_extra:
#             count = len(self.initial_extra) if self.initial_extra else 0
#             count += self.extra
#             return count
#         else:
#             return BaseInlineFormSet.total_form_count(self)

# class CustomModelForm(ModelForm):
#     """
#     Custom model form that support initial data when save
#     """

#     def has_changed(self):
#         """
#         Returns True if we have initial data.
#         """
#         has_changed = ModelForm.has_changed(self)
#         return bool(self.initial or has_changed)

# class CustomInlineAdmin(admin.TabularInline):
#     """
#     Custom inline admin that support initial data
#     """
#     form = CustomModelForm
#     formset = CustomInlineFormset

class CapsResource(resources.ModelResource):

    class Meta:
        model = FichaCaps


class InlineDatosConstitucion(admin.TabularInline):
    model = DatosConstitucion
    extra = 1
    max_num = 5

class InlineDatosGenerales(admin.TabularInline):
    model = DatosGenerales
    extra = 1
    max_num = 5

class InlineEstructuraGobernanza(admin.TabularInline):
    model = EstructuraGobernanza
    extra = 1
    max_num = 5

class InlineComposicionEstructura(admin.TabularInline):
    model = ComposicionEstructura
    extra = 1
    max_num = 5

class InlineFunciones(admin.TabularInline):
    model = Funciones
    extra = 1
    max_num = 5

class InlinePatrimonio(admin.TabularInline):
    model = Patrimonio
    extra = 1
    max_num = 5

class InlinePatrimonioEstimacion(admin.TabularInline):
    model = PatrimonioEstimacion
    extra = 1
    max_num = 7

class InlineAsistenciaTecnica(admin.TabularInline):
    #form = AsistenciaInlineAdminForm
    model = AsistenciaTecnica
    extra = 1
    max_num = 50

class InlineImagenesAdjuntos(admin.TabularInline):
    model = ImagenesAdjuntos
    extra = 1

class InlineDocumentosCaps(admin.TabularInline):
    model = DocumentosCaps
    extra = 1

class InlineFodaCaps(admin.StackedInline):
    model = FodaCaps
    extra = 1

class FichaCapsAdmin(ImportExportModelAdmin):
    form = PerfilAdminForm
    resource_class = CapsResource
    list_select_related = (
        'departamento',
        'municipio',
    )
    filter_horizontal = ('comunidad',)
    inlines = [InlineDatosGenerales,InlineDatosConstitucion,
               InlineEstructuraGobernanza,InlineComposicionEstructura,
               InlineFunciones,InlinePatrimonio,InlinePatrimonioEstimacion,
               InlineAsistenciaTecnica,InlineFodaCaps,InlineImagenesAdjuntos,
               InlineDocumentosCaps]
    list_display = ('nombre_caps','municipio','latitud',
                    'longitud','get_fotos','ruc',
                    'field_cantidad_familia')
    list_filter = ['departamento','municipio']
    search_fields = ('nombre_caps',)

    def get_queryset(self, request, *args, **kwargs):
        queryset = super().get_queryset(request, *args, **kwargs).prefetch_related('imagenesadjuntos_set','datosconstitucion_set')
        queryset = queryset.annotate(
            _foto_count=Count("imagenesadjuntos__foto", distinct=True)
        )
        return queryset

    def get_fotos(self, obj):
        if obj._foto_count > 0:
            return True
        return False
    get_fotos.boolean = True
    get_fotos.short_description = 'Tiene Fotos'

    def field_cantidad_familia(self, obj):
        return [ x.cantidad_beneficiarios for x in obj.datosconstitucion_set.all() ][0] or 0

    field_cantidad_familia.short_description = 'Cantidad personas'

    # def get_comunidades(self, obj):
    #     return "\n".join([p.nombre for p in obj.comunidad.prefetch_related('municipio').all()])
    # get_comunidades.short_description = 'Comunidades'
    #get_comunidades.admin_order_field = 'pais__nombre'
    # def get_fotos(self, obj):
    #     contar = []
    #     is_bolean = False
    #     for obj in obj.imagenesadjuntos_set.all():
    #         if obj.foto != '':
    #             contar.append(obj)
    #     if len(contar) > 0:
    #         is_bolean = True
    #     return is_bolean
    # get_fotos.boolean = True
    
    #get_fotos.admin_order_field = 'pais__nombre'

    class Media:
        #js = ('js/admin/perfil_admin.js',)
        css = {
             'all': ('css/admin/perfil_admin.css',)
        }

    # def _create_formsets(self, request, obj, change):
    #     """overide to provide initial data for inline formset"""
    #     formsets = []
    #     inline_instances = []
    #     prefixes = {}
    #     get_formsets_args = [request]
    #     if change:
    #         get_formsets_args.append(obj)
    #     for FormSet, inline in self.get_formsets_with_inlines(*get_formsets_args):
    #         prefix = FormSet.get_default_prefix()
    #         prefixes[prefix] = prefixes.get(prefix, 0) + 1
    #         if prefixes[prefix] != 1 or not prefix:
    #             prefix = "%s-%s" % (prefix, prefixes[prefix])
    #         formset_params = {
    #             'instance': obj,
    #             'prefix': prefix,
    #             'queryset': inline.get_queryset(request),
    #         }
    #         if request.method == 'POST':
    #             formset_params.update({
    #                 'data': request.POST,
    #                 'files': request.FILES,
    #                 'save_as_new': '_saveasnew' in request.POST
    #             })

    #         if change:
    #             formsets.append(FormSet(**formset_params))
    #             inline_instances.append(inline)
    #         else:
    #             if isinstance(inline, InlineAsistenciaTecnica):
    #                 inline_initial_data = [
    #                     {'opcion': 1},
    #                     {'opcion': 2},
    #                     {'opcion': 3},
    #                     {'opcion': 4},
    #                     {'opcion': 5},
    #                     {'opcion': 6},
    #                     {'opcion': 7},
    #                     {'opcion': 8},
    #                     {'opcion': 9},
    #                     {'opcion': 10},
    #                 ]
    #                 formset_params['initial'] = inline_initial_data
    #                 formsets.append(FormSet(**formset_params))
    #                 inline_instances.append(inline)
    #             else:
    #                 formsets.append(FormSet(**formset_params))
    #                 inline_instances.append(inline)

    #     return formsets, inline_instances

# Register your models here.
#admin.site.register(Necesidades)
#admin.site.register(NecesidadesAtendidas)
admin.site.register(FichaCaps, FichaCapsAdmin)
#admin.site.register(ImagenesAdjuntos)
#admin.site.register(Rubro)
#admin.site.register(ProveedorFondo)
#admin.site.register(InversionProveedor)
#admin.site.register(OtroProveedorFondo)
#admin.site.register(InversionOtrosProveedores)

# class DatosGeneralesAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(DatosGenerales, DatosGeneralesAdmin)

# class DatosConstitucionAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(DatosConstitucion, DatosConstitucionAdmin)

# class EstructuraGobernanzaAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(EstructuraGobernanza, EstructuraGobernanzaAdmin)

# class ComposicionEstructuraAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(ComposicionEstructura, ComposicionEstructuraAdmin)

# class FuncionesAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(Funciones, FuncionesAdmin)

# class PatrimonioAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(Patrimonio, PatrimonioAdmin)

# class PatrimonioEstimacionAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(PatrimonioEstimacion, PatrimonioEstimacionAdmin)

# class AsistenciaTecnicaAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(AsistenciaTecnica, AsistenciaTecnicaAdmin)

# class FodaCapsAdmin(ImportExportModelAdmin):
#     pass
# admin.site.register(FodaCaps, FodaCapsAdmin)
