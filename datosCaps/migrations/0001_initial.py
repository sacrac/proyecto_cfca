# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-13 19:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FamiliasAtendidas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('numero_tomas', models.IntegerField()),
                ('numero_familias', models.IntegerField()),
                ('numero_personas', models.IntegerField()),
            ],
            options={
                'verbose_name_plural': 'N\xfamero de familias atendidas',
            },
        ),
        migrations.CreateModel(
            name='FichaCaps',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero_registro', models.CharField(max_length=100)),
                ('nombre_caps', models.CharField(max_length=50, verbose_name='Nombre del CAPS')),
                ('contacto', models.CharField(blank=True, max_length=250, null=True)),
                ('cargo', models.CharField(blank=True, max_length=250, null=True)),
                ('facebook', models.URLField(blank=True, max_length=250, null=True)),
                ('telefono', models.CharField(blank=True, max_length=50, null=True)),
                ('cel_claro', models.CharField(blank=True, max_length=50, null=True)),
                ('cel_movistar', models.CharField(blank=True, max_length=50, null=True)),
                ('direccion', models.TextField()),
                ('mayor_complejidad', models.IntegerField(choices=[(1, 'Mini acueducto por bombeo el\xe9ctrico solar (MABE-ES)'), (2, 'Mini acueducto por bombeo el\xe9ctrico comercial (MABE-EC)'), (3, 'Mini-acueducto por gravedad (MAG)')])),
                ('menor_complejidad', models.IntegerField(choices=[(1, 'Pozo excavado con bomba de mano (PEBM)'), (2, 'Pozo perforado con bomba de mano (PPBM)')])),
            ],
            options={
                'verbose_name': 'Ficha CAPS',
                'verbose_name_plural': 'Ficha CAPS',
            },
        ),
        migrations.CreateModel(
            name='ImagenesAdjuntos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='fotos/ficha_caps/')),
                ('credito_foto', models.CharField(blank=True, max_length=250, null=True)),
                ('archivo', models.FileField(blank=True, null=True, upload_to='adjunto/ficha_caps')),
                ('ficha_caps', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.FichaCaps')),
            ],
            options={
                'verbose_name': 'Imagen archivo',
                'verbose_name_plural': 'Imagenes archivos',
            },
        ),
        migrations.CreateModel(
            name='InversionOtrosProveedores',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ejecutado', models.FloatField()),
            ],
            options={
                'verbose_name_plural': 'Tipos de inversiones de otros proveedores',
            },
        ),
        migrations.CreateModel(
            name='InversionProveedor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('presupuestado', models.FloatField()),
                ('ejecutado', models.FloatField()),
            ],
            options={
                'verbose_name_plural': 'Tipos de inversiones',
            },
        ),
        migrations.CreateModel(
            name='MiembrosFamilias',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('edad', models.IntegerField(choices=[(1, 'Hombres mayores 31 a\xf1os'), (2, 'Mujeres mayores 31 a\xf1os '), (3, 'Hombre joven 19 a 30 a\xf1os'), (4, 'Mujer joven 19 a 30 a\xf1os'), (5, 'Hombre adolescente 13 a 18'), (6, 'Mujer adolescente 13 a 18 '), (7, 'Ni\xf1os 5 a 12 a\xf1os'), (8, 'Ni\xf1as 5 a 12 a\xf1os'), (9, 'Ancianos (> 64 a\xf1os)'), (10, 'Ancianas (> 64 a\xf1os)')])),
                ('cantidad', models.IntegerField()),
                ('ficha_caps', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.FichaCaps')),
            ],
            options={
                'verbose_name_plural': 'Miembros de la familia',
            },
        ),
        migrations.CreateModel(
            name='Necedidades',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
            ],
            options={
                'verbose_name_plural': 'Necesidades de los caps',
            },
        ),
        migrations.CreateModel(
            name='Necesidades',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'Necesidad',
                'verbose_name_plural': 'Necesidades',
            },
        ),
        migrations.CreateModel(
            name='NecesidadesAtendidas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'Necesidad atendida',
                'verbose_name_plural': 'Necesidades atendidas',
            },
        ),
        migrations.CreateModel(
            name='Organizacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_siglas', models.CharField(max_length=250)),
                ('nombre_completo', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'Organizaci\xf3n',
                'verbose_name_plural': 'Organizaciones',
            },
        ),
        migrations.CreateModel(
            name='OtroProveedorFondo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('ficha_caps', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.FichaCaps')),
                ('organizacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.Organizacion')),
            ],
        ),
        migrations.CreateModel(
            name='ProveedorFondo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('ficha_caps', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.FichaCaps')),
                ('organizacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.Organizacion')),
            ],
        ),
        migrations.CreateModel(
            name='Rubro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'Rubro',
                'verbose_name_plural': 'Rubros',
            },
        ),
        migrations.AddField(
            model_name='necedidades',
            name='descripcion_necesidad',
            field=models.ManyToManyField(to='datosCaps.Necesidades'),
        ),
        migrations.AddField(
            model_name='necedidades',
            name='ficha_caps',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.FichaCaps'),
        ),
        migrations.AddField(
            model_name='necedidades',
            name='necesidad_atendida',
            field=models.ManyToManyField(to='datosCaps.NecesidadesAtendidas'),
        ),
        migrations.AddField(
            model_name='inversionproveedor',
            name='proveedor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.ProveedorFondo'),
        ),
        migrations.AddField(
            model_name='inversionproveedor',
            name='rubro',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.Rubro'),
        ),
        migrations.AddField(
            model_name='inversionotrosproveedores',
            name='proveedor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.OtroProveedorFondo'),
        ),
        migrations.AddField(
            model_name='inversionotrosproveedores',
            name='rubro',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.Rubro'),
        ),
        migrations.AddField(
            model_name='familiasatendidas',
            name='ficha_caps',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datosCaps.FichaCaps'),
        ),
    ]
