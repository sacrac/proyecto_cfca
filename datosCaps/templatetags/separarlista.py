from django import template

register = template.Library()

@register.filter(name='separalist')
def separalist(value):
    string = ""
    for s in value:
        string += "<li>" + str(s) + "</li>\n"
    return string
    # try:
    #     return '<li> '.join(value)
    # except:
    #     return None

@register.filter
def calcular_porcentaje(cantidad, total):
    if total == None or cantidad == None or total == 0:
        x = 0
    else:
        x = (cantidad * 100) / float(total)
    return round(x, 1)