# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Prefetch
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from .models import *
from .forms import FiltroMapaForms, FiltroActividadMapaForms
from actividades.models import EjesTransversales, Actividad
import json as simplejson
from django.http import HttpResponse

from proyecto_cfca.utils import render_to_pdf

#mapa de actividades
from actividades.models import Actividad
# Create your views here.

class ListaCaps(ListView):
    model = FichaCaps
    template_name = 'seguimiento/segimiento_cobertura.html'
    #queryset = FichaCaps.objects.select_related()#.values('id','nombre_caps', 'municipio__nombre','comunidad__nombre')

    def get_queryset(self):
        # q1 = Comunidad.objects.select_related('municipio')
        # queryset = FichaCaps.objects.prefetch_related(
        #             Prefetch('comunidad', queryset=q1)
        #         ).values('id','nombre_caps','municipio__nombre','comunidad__nombre','datosconstitucion__cantidad_familia')

        # q2 = queryset
        # algo = q2.values('id','nombre_caps','municipio__nombre','comunidad__nombre','datosconstitucion__cantidad_familia')
        # print(algo)
        queryset = FichaCaps.objects.prefetch_related('comunidad', 'datosconstitucion_set').select_related('municipio')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListaCaps, self).get_context_data(**kwargs)

        return context


class DetalleCaps(DetailView):
    model = FichaCaps
    template_name = 'caps/perfil_caps.html'

    def get_context_data(self, **kwargs):
        context = super(DetalleCaps, self).get_context_data(**kwargs)
        #print(self.object)
        years = []

        for x in self.object.datosconstitucion_set.all():
            years.append((x.fecha.year,x.fecha.year))

        lista_year = list(sorted(set(years)))

        perfil = {}
        asistencia_modulos = {}
        for obj in lista_year:
            generales = self.object.datosgenerales_set.filter(fecha__year=obj[0])
            constitucion = self.object.datosconstitucion_set.filter(fecha__year=obj[0])
            estructuracion = self.object.estructuragobernanza_set.filter(fecha__year=obj[0])
            composicion = self.object.composicionestructura_set.filter(fecha__year=obj[0])
            funciones = self.object.funciones_set.filter(fecha__year=obj[0])
            patrimonio = self.object.patrimonio_set.filter(fecha__year=obj[0])
            patrimonio_estimacion = self.object.patrimonioestimacion_set.filter(fecha__year=obj[0])
            asistencia = self.object.asistenciatecnica_set.filter(fecha__year=obj[0])
            foda = self.object.fodacaps_set.filter(fecha__year=obj[0])
            documentos = self.object.documentoscaps_set.filter(fecha__year=obj[0])
            fotos = self.object.imagenesadjuntos_set.all()
            for modulo in EjesTransversales.objects.exclude(id=7).order_by('id'):
                asistio = Actividad.objects.filter(modulos=modulo,caps=self.object).count()
                asistencia_modulos[modulo] = asistio

            perfil[obj[1]] = [generales,constitucion,estructuracion,
                              composicion,funciones,patrimonio,
                              patrimonio_estimacion,asistencia,foda,
                              documentos,asistencia_modulos,fotos]

        context['perfil'] = perfil

        # for k,v in perfil.items():
        #     #print(v[2])
        #     for estructura in v[2]:
        #         print(estructura.estructuras)
        #         for x in estructura.estructuras:
        #             print(x)

        return context

def obtener_lista_mapa_caps(request):
    lista = []
    caps = FichaCaps.objects.prefetch_related()
    # 
    #caps = FichaCaps.objects.prefetch_related('datosgenerales_set',
    #                                         'datosconstitucion_set',
    #                                         'imagenesadjuntos_set').select_related('municipio').values('id',
    #                                         'nombre_caps',
    #                                         'latitud',
    #                                         'longitud',
    #                                         'direccion',
    #                                         'municipio__latitud',
    #                                         'municipio__longitud',
    #                                         'datosgenerales__contacto',
    #                                         'datosgenerales__cel_movistar',
    #                                         'datosgenerales__cel_claro',
    #                                         'datosconstitucion__tomas_administrada',
    #                                         'datosconstitucion__cantidad_familia',
    #                                         'datosconstitucion__cantidad_beneficiarios',
    #                                         'imagenesadjuntos__foto'
    #                                         )
    #print(len(caps))
    contacto = 'Sin Contacto'
    tigo = 'Sin Linea'
    claro = 'Sin Linea'
    tomas = '0'
    familias = '0'
    beneficiarios = '0'
    foto='https://placehold.it/50x50'
    for objeto in caps:
        for obj in objeto.datosgenerales_set.values_list('cel_movistar','cel_claro','contacto'): 
            if obj[0] != None:
                tigo = obj[0]
            else:
                tigo = ''
            if obj[1] != None:
                claro =obj[1]
            else:
                claro = ''
            if obj[2] != None:
                contacto = obj[2]
            else:
                contacto = 'Sin Contacto'
        for obj in objeto.datosconstitucion_set.values_list('tomas_administrada',
                                                        'cantidad_familia','cantidad_beneficiarios'):  
            if obj[0] != None:
                tomas = obj[0]
            if obj[1] != None:
                familias = obj[1]
            if obj[2] != None:
                beneficiarios = obj[2]
        for obj in objeto.imagenesadjuntos_set.values_list('foto',flat=True):   
            
            if obj:
                foto = '/media/'+obj
            else:
                foto='https://placehold.it/50x50'
        if objeto.latitud  and objeto.longitud:
            dicc = dict(nombre=objeto.nombre_caps,
                        id=objeto.id,
                        lon=float(objeto.longitud) ,
                        lat=float(objeto.latitud),
                        direccion=str(objeto.direccion),
                        contacto=str(contacto),
                        telefono1=str(tigo),
                        telefono2=str(claro),
                        tomas_administrada=tomas,
                        cantidad_familias=familias,
                        cantidad_beneficiarios=beneficiarios,
                        fotos=str(foto)
                        )
            lista.append(dicc)
        elif objeto.municipio.latitud != None and objeto.municipio.longitud !=None:
            dicc = dict(nombre=objeto.nombre_caps,
                        id=objeto.id,
                        lon=float(objeto.municipio.longitud) ,
                        lat=float(objeto.municipio.latitud),
                        direccion=str(objeto.direccion),
                        contacto=str(contacto),
                        telefono1=str(tigo),
                        telefono2=str(claro),
                        tomas_administrada=tomas,
                        cantidad_familias=familias,
                        cantidad_beneficiarios=beneficiarios,
                        fotos=str(foto)
                        )
            lista.append(dicc)

    #print(lista)
    serializado = simplejson.dumps(lista)
    return HttpResponse(serializado, content_type='application/json')

def _mapa_filtros_request(request):
    params = {}

    if 'departamento' in request.session:
        params['departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['municipio'] = request.session['municipio']

    if 'tipo_sistema' in request.session:
        params['datosconstitucion__tipo_sistema_agua__contains'] = request.session['tipo_sistema']

    if 'legalizado' in request.session:
        if request.session['legalizado'] == True:
            params['datosconstitucion__registro_aval__contains'] = "1"

    if 'legal_nacional' in request.session:
        if request.session['legal_nacional'] == True:
            params['datosconstitucion__registro_aval__contains'] = "3"

    if 'perforacion' in request.session:
        if request.session['perforacion'] == True:
            params['actividad__tipo_actividad__id__exact'] = 12

    if 'mejora' in request.session:
        if request.session['mejora'] == True:
            params['actividad__tipo_actividad__id__exact'] = 13

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]
    print("params")
    print(params)
    #FichaCaps.objects.filter(datosconstitucion__registro_aval__contains="1").filter(datosconstitucion__registro_aval__contains="3"
    #FichaCaps.objects.filter(Q(datosconstitucion__registro_aval__contains="1") & Q(datosconstitucion__registro_aval__contains="3"))
    
    return FichaCaps.objects.filter(**params)

def mapa_con_filtro(request, template='seguimiento/seguimiento_cobertura_filtro.html'):
    contacto = 'Sin Contacto'
    tigo = 'Sin Linea'
    claro = 'Sin Linea'
    tomas = '0'
    familias = '0'
    beneficiarios = '0'
    foto='https://placehold.it/50x50'
    if request.method == 'POST':
        form = FiltroMapaForms(request.POST)
        if form.is_valid():
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['tipo_sistema'] = form.cleaned_data['tipo_sistema']
            request.session['legalizado'] = form.cleaned_data['legalizado']
            request.session['legal_nacional'] = form.cleaned_data['legal_nacional']
            request.session['perforacion'] = form.cleaned_data['perforacion']
            request.session['mejora'] = form.cleaned_data['mejora']
            centinela = 1
            print("hubo consulta")
        else:
            centinela = 0
            print("hubo egorrr")
    else:
        form = FiltroMapaForms()
        centinela = 0

    if centinela == 1:
        filtro = _mapa_filtros_request(request)
        cantidad_caps = filtro.count()
        object_list = filtro.prefetch_related('comunidad','datosconstitucion_set','datosgenerales_set','imagenesadjuntos_set').select_related('municipio','departamento')
        lista = []
        for objeto in object_list:
            for obj in objeto.datosgenerales_set.values_list('cel_movistar','cel_claro','contacto'): 
                if obj[0] != None:
                    tigo = obj[0]
                else:
                    tigo = ''
                if obj[1] != None:
                    claro =obj[1]
                else:
                    claro = ''
                if obj[2] != None:
                    contacto = obj[2]
                else:
                    contacto = 'Sin Contacto'
            for obj in objeto.datosconstitucion_set.values_list('tomas_administrada',
                                                            'cantidad_familia','cantidad_beneficiarios'):  
                if obj[0] != None:
                    tomas = obj[0]
                if obj[1] != None:
                    familias = obj[1]
                if obj[2] != None:
                    beneficiarios = obj[2]
            for obj in objeto.imagenesadjuntos_set.values_list('foto',flat=True):   
                
                if obj:
                    foto = '/media/'+obj
                else:
                    foto='https://placehold.it/50x50'
            if objeto.latitud  and objeto.longitud:
                dicc = dict(nombre=objeto.nombre_caps,
                            id=objeto.id,
                            lon=float(objeto.longitud) ,
                            lat=float(objeto.latitud),
                            direccion=str(objeto.direccion),
                            contacto=str(contacto),
                            telefono1=str(tigo),
                            telefono2=str(claro),
                            tomas_administrada=tomas,
                            cantidad_familias=familias,
                            cantidad_beneficiarios=beneficiarios,
                            fotos=str(foto)
                            )
                lista.append(dicc)
            elif objeto.municipio.latitud != None and objeto.municipio.longitud !=None:
                dicc = dict(nombre=objeto.nombre_caps,
                            id=objeto.id,
                            lon=float(objeto.municipio.longitud) ,
                            lat=float(objeto.municipio.latitud),
                            direccion=str(objeto.direccion),
                            contacto=str(contacto),
                            telefono1=str(tigo),
                            telefono2=str(claro),
                            tomas_administrada=tomas,
                            cantidad_familias=familias,
                            cantidad_beneficiarios=beneficiarios,
                            fotos=str(foto)
                            )
                lista.append(dicc)
    if centinela == 0:
        object_list = FichaCaps.objects.prefetch_related('comunidad','datosconstitucion_set','datosgenerales_set','imagenesadjuntos_set','composicionestructura_set').select_related('municipio','departamento')
        cantidad_caps = object_list.count()

    return render(request, template, locals())



def mapa_con_filtro_mobile(request, template='seguimiento/mapa_mobile.html'):
    contacto = 'Sin Contacto'
    tigo = 'Sin Linea'
    claro = 'Sin Linea'
    tomas = '0'
    familias = '0'
    beneficiarios = '0'
    foto='https://placehold.it/50x50'
    if request.method == 'POST':
        form = FiltroMapaForms(request.POST)
        if form.is_valid():
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['tipo_sistema'] = form.cleaned_data['tipo_sistema']
            request.session['legalizado'] = form.cleaned_data['legalizado']
            request.session['legal_nacional'] = form.cleaned_data['legal_nacional']
            request.session['perforacion'] = form.cleaned_data['perforacion']
            request.session['mejora'] = form.cleaned_data['mejora']
            centinela = 1
            print("hubo consulta")
        else:
            centinela = 0
            print("hubo egorrr")
    else:
        form = FiltroMapaForms()
        centinela = 0

    if centinela == 1:
        filtro = _mapa_filtros_request(request)
        cantidad_caps = filtro.count()
        object_list = filtro.prefetch_related('comunidad','datosconstitucion_set','datosgenerales_set','imagenesadjuntos_set').select_related('municipio','departamento')
        lista = []
        for objeto in object_list:
            for obj in objeto.datosgenerales_set.values_list('cel_movistar','cel_claro','contacto'): 
                if obj[0] != None:
                    tigo = obj[0]
                else:
                    tigo = ''
                if obj[1] != None:
                    claro =obj[1]
                else:
                    claro = ''
                if obj[2] != None:
                    contacto = obj[2]
                else:
                    contacto = 'Sin Contacto'
            for obj in objeto.datosconstitucion_set.values_list('tomas_administrada',
                                                            'cantidad_familia','cantidad_beneficiarios'):  
                if obj[0] != None:
                    tomas = obj[0]
                if obj[1] != None:
                    familias = obj[1]
                if obj[2] != None:
                    beneficiarios = obj[2]
            for obj in objeto.imagenesadjuntos_set.values_list('foto',flat=True):   
                
                if obj:
                    foto = '/media/'+obj
                else:
                    foto='https://placehold.it/50x50'
            if objeto.latitud  and objeto.longitud:
                dicc = dict(nombre=objeto.nombre_caps,
                            id=objeto.id,
                            lon=float(objeto.longitud) ,
                            lat=float(objeto.latitud),
                            direccion=str(objeto.direccion),
                            contacto=str(contacto),
                            telefono1=str(tigo),
                            telefono2=str(claro),
                            tomas_administrada=tomas,
                            cantidad_familias=familias,
                            cantidad_beneficiarios=beneficiarios,
                            fotos=str(foto)
                            )
                lista.append(dicc)
            elif objeto.municipio.latitud != None and objeto.municipio.longitud !=None:
                dicc = dict(nombre=objeto.nombre_caps,
                            id=objeto.id,
                            lon=float(objeto.municipio.longitud) ,
                            lat=float(objeto.municipio.latitud),
                            direccion=str(objeto.direccion),
                            contacto=str(contacto),
                            telefono1=str(tigo),
                            telefono2=str(claro),
                            tomas_administrada=tomas,
                            cantidad_familias=familias,
                            cantidad_beneficiarios=beneficiarios,
                            fotos=str(foto)
                            )
                lista.append(dicc)
    if centinela == 0:
        object_list = FichaCaps.objects.prefetch_related('comunidad','datosconstitucion_set','datosgenerales_set','imagenesadjuntos_set').select_related('municipio','departamento')
        cantidad_caps = object_list.count()

    return render(request, template, locals())

#generate pdf

def genratePdf(request,pk):
    caps = FichaCaps.objects.get(pk = pk)
    years = []

    for x in caps.datosconstitucion_set.all():
        years.append((x.fecha.year,x.fecha.year))

    lista_year = list(sorted(set(years)))

    perfil = {}
    asistencia_modulos = {}
    for obj in lista_year:
        generales = caps.datosgenerales_set.filter(fecha__year=obj[0])
        constitucion = caps.datosconstitucion_set.filter(fecha__year=obj[0])
        estructuracion = caps.estructuragobernanza_set.filter(fecha__year=obj[0])
        composicion = caps.composicionestructura_set.filter(fecha__year=obj[0])
        funciones = caps.funciones_set.filter(fecha__year=obj[0])
        patrimonio = caps.patrimonio_set.filter(fecha__year=obj[0])
        patrimonio_estimacion = caps.patrimonioestimacion_set.filter(fecha__year=obj[0])
        asistencia = caps.asistenciatecnica_set.filter(fecha__year=obj[0])
        foda = caps.fodacaps_set.filter(fecha__year=obj[0])
        documentos = caps.documentoscaps_set.filter(fecha__year=obj[0])
        fotos = caps.imagenesadjuntos_set.all()
        for modulo in EjesTransversales.objects.exclude(id=7).order_by('id'):
            asistio = Actividad.objects.filter(modulos=modulo,caps=caps).count()
            asistencia_modulos[modulo] = asistio

        perfil[obj[1]] = [generales,constitucion,estructuracion,
                            composicion,funciones,patrimonio,
                            patrimonio_estimacion,asistencia,foda,
                            documentos,asistencia_modulos,fotos]
                            
    data = {'caps': caps,'perfil': perfil, 'request': request}
    pdf = render_to_pdf('caps-to-pdf.html', data)
    return HttpResponse(pdf, content_type='application/pdf')


def _mapa_actividad_filtros_request(request):
    params = {}

    # if 'fecha' in request.session:
    #     params['fecha__year'] = request.session['fecha']

    # if 'departamento' in request.session:
    #     params['departamento'] = request.session['departamento']

    # if 'municipio' in request.session:
    #     params['municipio'] = request.session['municipio']

    if 'perforacion' in request.session:
        if request.session['perforacion'] == True:
            params['tipo_actividad__id__exact'] = 12

    if 'mejora' in request.session:
        if request.session['mejora'] == True:
            params['tipo_actividad__id__exact'] = 13

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]
    print("params")
    print(params)
    
    return Actividad.objects.filter(**params).prefetch_related('caps','presupuestoinfraestructura_set','financiamientoinstituciones_set')



def mapa_actividades(request, template='seguimiento/mapa_actividad.html'):
    centinela = 0
    if request.method == 'POST':
        form = FiltroActividadMapaForms(request.POST)
        if form.is_valid():
            #request.session['fecha'] = form.cleaned_data['fecha']
            #request.session['departamento'] = form.cleaned_data['departamento']
            #request.session['municipio'] = form.cleaned_data['municipio']
            request.session['perforacion'] = form.cleaned_data['perforacion']
            request.session['mejora'] = form.cleaned_data['mejora']
            centinela = 1
            print("hubo consulta")
        else:
            centinela = 0
            print("hubo egorrr")
    else:
        # del request.session['perforacion']
        # del request.session['mejora']
        form = FiltroActividadMapaForms()
        centinela = 0
        print("Esta entrando en centinela zero 0")
        lista = []
        filtro2 = Actividad.objects.filter(tipo_actividad__in=[12,13]).prefetch_related('caps','presupuestoinfraestructura_set','financiamientoinstituciones_set')
        cantidad_caps = filtro2.count()
        object_list = filtro2
        for objeto in filtro2:
            total = 0
            for presu in objeto.presupuestoinfraestructura_set.all():
                if presu.alcaldia:
                    total += presu.alcaldia
                if presu.comunidad:
                    total += presu.comunidad
                if presu.otros:
                    total += presu.otros
            total_instituciones = 0
            logos = []
            for insti in objeto.financiamientoinstituciones_set.all():
                total_instituciones += insti.monto
                logos.append(insti.institucion.logo)
            
            for cap in objeto.caps.all():
                if cap.latitud  and cap.longitud:
                    dicc = dict(nombre=objeto.actividad,
                                id=cap.id,
                                nombreCaps=cap.nombre_caps,
                                lon=float(cap.longitud),
                                lat=float(cap.latitud),
                                ejecutado=objeto.ejecutado,
                                infraestructura=total,
                                instituciones=total_instituciones,
                                descripcion=objeto.descripcion_inversion,
                                logo=logos
                                )
                    lista.append(dicc)

    if centinela == 1:
        filtro = _mapa_actividad_filtros_request(request)
        cantidad_caps = filtro.count()
        object_list = filtro
        lista = []
        for objeto in filtro:
            total = 0
            for presu in objeto.presupuestoinfraestructura_set.all():
                if presu.alcaldia:
                    total += presu.alcaldia
                if presu.comunidad:
                    total += presu.comunidad
                if presu.otros:
                    total += presu.otros
            total_instituciones = 0
            logos = []
            for insti in objeto.financiamientoinstituciones_set.all():
                total_instituciones += insti.monto
                logos.append(insti.institucion.logo)

            for cap in objeto.caps.all():
                if cap.latitud  and cap.longitud:
                    dicc = dict(nombre=objeto.actividad,
                                id=cap.id,
                                nombreCaps=cap.nombre_caps,
                                lon=float(cap.longitud),
                                lat=float(cap.latitud),
                                ejecutado=objeto.ejecutado,
                                infraestructura=total,
                                instituciones=total_instituciones,
                                descripcion=objeto.descripcion_inversion,
                                logo=logos
                                )
                    lista.append(dicc)
    

    return render(request, template, locals())