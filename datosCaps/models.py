# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from lugar.models import Departamento, Municipio, Comunidad
from sorl.thumbnail import ImageField
from multiselectfield import MultiSelectField

# Create your models here.
CHOICE_MAYOR_COMPLEJIDAD = (
                        (1,'Mini acueducto por bombeo eléctrico solar (MABE-ES)'),
                        (2,'Mini acueducto por bombeo eléctrico comercial (MABE-EC)'),
                        (3,'Mini-acueducto por gravedad (MAG)'),
                        (4,'Pozo excavado con bomba de mano (PEBM)'),
                        (5,'Pozo perforado con bomba de mano (PPBM)'),
                    )

CHOICE_MENOR_COMPLEJIDAD = (
                        (1,'Pozo excavado con bomba de mano (PEBM)'),
                        (2,'Pozo perforado con bomba de mano (PPBM)'),
                    )

CHOICE_SEXO = (
                        (1,'Hombre'),
                        (2,'Mujer'),
                    )

CHOICE_SI_NO = (
                        (1,'Si'),
                        (2,'No'),
                    )

CHOICE_EDAD_FAMILIA = (
                        (1,'Hombres mayores 31 años'),
                        (2,'Mujeres mayores 31 años '),
                        (3,'Hombre joven 19 a 30 años'),
                        (4,'Mujer joven 19 a 30 años'),
                        (5,'Hombre adolescente 13 a 18'),
                        (6,'Mujer adolescente 13 a 18 '),
                        (7,'Niños 5 a 12 años'),
                        (8,'Niñas 5 a 12 años'),
                        (9,'Ancianos (> 64 años)'),
                        (10,'Ancianas (> 64 años)')

                    )

CHOICE_REGISTRO_AVAL = (
                        (1,'Certificado de Registro Municipal'),
                        (2,'Inscripción en el Registro Central de Proveedores de Servicios de Agua Potable y Saneamiento'),
                        (3,'Aval de INAA'),
                        (4,'Otros'),
                        (5, 'En proceso'),
                    )

CHOICE_ESTRUCTURA = (
                        ('A','Asamblea general de pobladores'),
                        ('B','Junta Directiva'),
                        ('C','Administración'),
                        ('D','Comisión de apoyo'),
                    )

CHOICE_JUNTA_DIRECTIVA_CARGOS = (
                        ('A','Presidente'),
                        ('B','Vocal'),
                        ('C','Secretario'),
                        ('D','Tesorero'),
                        ('E','Fiscal'),
                        ('F','Otros'),
                    )

CHOICE_PERSONAL_AYUDA_ECONOMICA = (
                        ('A','Colector'),
                        ('B','Bombero'),
                        ('C','Electricista'),
                        ('D','Plomero'),
                        ('E','Contador'),
                        ('F','Otros'),
                    )

CHOICE_DOCUMENTOS_CAPS = (
                        ('A','Estatutos'),
                        ('B','Reglamento'),
                        ('C','Acta constitutiva'),
                        ('D','Certificado de registro del CAPS'),
                        ('E','Manual contable'),
                        ('F','Pliego tarifario'),
                        ('G','Plan de trabajo'),
                        ('H','Estados financieros'),
                        ('I','Plan de mantenimiento de obras'),
                        ('J','Plan de inversiones'),
                        ('K','Libro de Acta'),
                        ('L','Libro Diario'),
                        ('M','Registro de Usuarios'),
                        ('O', 'Libro Mayor'),
                        ('N','Otros'),
                    )

CHOICE_FUNCIONES_CAPS = (
                        ('A','Cumplir y hacer cumplir el reglamento y las normativas que establece el INAA'),
                        ('B','Convocar a reuniones para tratar asuntos relativos al acueducto'),
                        ('C','Velar por el buen funcionamiento del servicio'),
                        ('D','Autorizar o suspender los servicios domiciliarios'),
                        ('E','Recaudar y administrar los fondos provenientes de tarifas del servicio y otros ingresos'),
                        ('F','Colaborar con INAA en las campañas de promoción comunal y divulgación sanitaria'),
                        ('G','Colaborar con Alcaldía en las campañas de promoción comunal y divulgación sanitaria'),
                        ('H','Colaborar con MINSA en las campañas de promoción comunal y divulgación sanitaria'),
                        ('I','Fomentar el uso adecuado del sistema de agua'),
                        ('J','Vigilar y proteger las fuentes de abastecimiento de agua'),
                        ('K','Contratar el personal necesario para la operación y mantenimiento del sistema de agua'),
                        ('L','Rendir informe de funcionamiento del CAPS'),
                        ('M','Cumplir normas de calidad del agua que establezca INAA en coordinación con MINSA'),
                        ('N','Actividades comunitarias'),
                    )

CHOICE_INGRESOS_CAPS = (
                        ('A','Pago de tarifa'),
                        ('B','Aportaciones voluntarias'),
                        ('C','Donaciones'),
                        ('E','Prestamos'),
                        ('F','Proyectos de la cooperación'),
                    )

CHOICE_GASTOS_CAPS = (
                        ('A','Gasto de operaciones'),
                        ('B','Gasto de mantenimiento'),
                        ('C','Gasto de administración del servicio'),
                        ('E','Gastos de personal'),
                        ('F','Otros'),
                    )

CHOICE_PATRIMONIO_CAPS = (
                        ('A','Sistema de distribución del agua'),
                        ('B','Pozo'),
                        ('C','Pila, Tanque y Torre'),
                        ('D','Terreno'),
                        ('E','Equipo de oficiana'),
                        ('F','Oficina'),
                        ('G','Otros'),
                    )

CHOICE_IMPUESTOS_CAPS = (
                         ('A','Impuesto sobre la renta (IR)'),
                         ('B','Impuesto sobre el valor agregado (IVA)'),
                         ('C', 'No paga impuestos'),
                    )

class Necesidades(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Necesidad'
        verbose_name_plural = 'Necesidades'

class NecesidadesAtendidas(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Necesidad atendida'
        verbose_name_plural = 'Necesidades atendidas'

class FichaCaps(models.Model):
    nombre_caps = models.CharField('Nombre del CAPS', max_length=50)
    departamento = models.ForeignKey(Departamento, null=True, blank=True)
    municipio = models.ForeignKey(Municipio, null=True, blank=True)
    comunidad = models.ManyToManyField(Comunidad, blank=True)
    direccion = models.TextField('Dirección fisica/sede', null=True, blank=True)
    latitud = models.DecimalField('Latitud', max_digits=8, decimal_places=5, blank=True, null=True)
    longitud = models.DecimalField('Longitud', max_digits=8, decimal_places=5, blank=True, null=True)
    ruc = models.CharField('Número RUC', max_length=250, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.nombre_caps,self.municipio)

    class Meta:
        verbose_name = 'Perfil CAPS'
        verbose_name_plural = 'Perfil CAPS'
        ordering = ('nombre_caps',)

    def __get_comunidades_values(self):
        ret = ''
        #print(self.comunidad.all())
        for dept in self.comunidad.all():
            ret = ret + dept.nombre + ','
        # remove the last ',' and return the value.
        return ret[:-1]
    all_comunidades = property(__get_comunidades_values)

class DatosGenerales(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    contacto = models.CharField(max_length=250, null=True, blank=True)
    cargo = models.CharField(max_length=250, null=True, blank=True)
    cel_movistar = models.CharField(max_length=50, null=True, blank=True)
    cel_claro = models.CharField(max_length=50, null=True, blank=True)
    facebook = models.URLField(max_length=250, null=True, blank=True)

    class Meta:
        verbose_name_plural = "Datos Generales"

class DatosConstitucion(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    presidente = models.CharField('Nombre del presidente', max_length=250)
    sexo = models.IntegerField(choices=CHOICE_SEXO)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    fecha_constitucion = models.DateField('Fecha constitución del CAPS',null=True,blank=True)
    fecha_legalizacion = models.DateField('Fecha de registro municipal',null=True,blank=True)
    registro_aval = MultiSelectField(choices=CHOICE_REGISTRO_AVAL,null=True,blank=True)
    otros_aval = models.CharField(max_length=250, null=True, blank=True)
    tipo_sistema_agua = MultiSelectField(choices=CHOICE_MAYOR_COMPLEJIDAD,null=True,blank=True)
    #tipo_sistema_agua = models.IntegerField(choices=CHOICE_MAYOR_COMPLEJIDAD)
    tomas_administrada = models.IntegerField('Cantidad de tomas administradas por el CAPS')
    cantidad_familia = models.IntegerField('Cantidad de familias que conforman el CAPS')
    cantidad_beneficiarios = models.IntegerField('Cantidad de personas beneficiaras por el CAPS')

    class Meta:
        verbose_name_plural = "Datos de constitución"

class EstructuraGobernanza(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    estructuras = MultiSelectField(choices=CHOICE_ESTRUCTURA)
    estructura_apoyo = models.CharField('Si cuenta con estructuras de apoyo puede indicar los nombres', max_length=350,null=True,blank=True)
    junta_directiva_cargos = MultiSelectField(choices=CHOICE_JUNTA_DIRECTIVA_CARGOS)
    junta_directiva_mujer = MultiSelectField(choices=CHOICE_JUNTA_DIRECTIVA_CARGOS,null=True,blank=True)
    documentos_caps = MultiSelectField(choices=CHOICE_DOCUMENTOS_CAPS,null=True,blank=True)
    jd_ayuda_economica = MultiSelectField(choices=CHOICE_JUNTA_DIRECTIVA_CARGOS,
                                          null=True,blank=True)
    personal_ayuda_economica = MultiSelectField(choices=CHOICE_PERSONAL_AYUDA_ECONOMICA,
                                          null=True,blank=True)
    personal_mujer = MultiSelectField(choices=CHOICE_PERSONAL_AYUDA_ECONOMICA,
                                          null=True,blank=True)


    class Meta:
        verbose_name_plural = 'Estructuras y documentos de gobernanza de el CAPS'

class ComposicionEstructura(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    mujeres_asamblea_general = models.IntegerField()
    hombres_asamblea_general = models.IntegerField()
    mujeres_junta_directiva = models.IntegerField()
    hombres_junta_directiva = models.IntegerField()
    mujeres_administracion = models.IntegerField()
    hombres_administracion = models.IntegerField()
    mujeres_comisiones_apoyo = models.IntegerField()
    hombres_comisiones_apoyo = models.IntegerField()

    class Meta:
        verbose_name_plural = 'Composición de las estructuras del CAPS'

class Funciones(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    funciones_jd = MultiSelectField(choices=CHOICE_FUNCIONES_CAPS,
        verbose_name='Cuáles de las siguiente funciones desempeñadas por la JD del CAPS',null=True,blank=True)
    ingresos = MultiSelectField(choices=CHOICE_INGRESOS_CAPS,
        verbose_name='De donde provienen los ingresos del CAPS',null=True,blank=True)
    gastos = MultiSelectField(choices=CHOICE_GASTOS_CAPS,
        verbose_name='Cuales son los principales gastos del CAPS',null=True,blank=True)

    class Meta:
        verbose_name_plural = 'Funciones del CAPS'

class Patrimonio(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    patrimonio_caps = MultiSelectField(choices=CHOICE_PATRIMONIO_CAPS,
        verbose_name='el CAPS  cuenta con patrimonio',null=True,blank=True)
    pagos = MultiSelectField(choices=CHOICE_IMPUESTOS_CAPS,
        verbose_name='el CAPS paga impuestos')
    energia = models.IntegerField(choices=CHOICE_SI_NO,
        verbose_name='El CAPS tiene beneficio de tarifa diferenciada por la energia electrica',null=True,blank=True)
    no = models.CharField('Puede indicar las razones, si su respuesta es NO', max_length=250, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'El Patrimonio del CAPS'

CHOICES_ESTIMACION_PATRIMONIO = (
                            (1, 'Sistema distribución del agua'),
                            (2, 'Pozo'),
                            (3, 'Pila, tanque y torre'),
                            (4, 'Terreno'),
                            (5, 'Equipo de oficina'),
                            (6, 'Oficinas'),
                            (7, 'Otros'),
                )

class PatrimonioEstimacion(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    patriminio = models.IntegerField(choices=CHOICES_ESTIMACION_PATRIMONIO)
    monto = models.FloatField()

    class Meta:
        verbose_name_plural = 'Estimación del valor Patrimonio del CAPS'

CHOICES_ASISTENCIA_TECNICA = (
                            (1, 'INAA'),
                            (2, 'MINSA'),
                            (3, 'ALCALDIA'),
                            (4, 'ONG NACIONAL'),
                            (5, 'ONG INTER.'),
                            (6, 'OTROS CAPS'),
                            (7, 'DGI'),
                            (8, 'INIFOM'),
                            (9, 'NUEVO FISE'),
                            (10, 'ENACAL'),
                            (11, 'UT MUNICIPAL'),
                            (12, 'UMAS')
                )

class AsistenciaTecnica(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    opcion = models.IntegerField(choices=CHOICES_ASISTENCIA_TECNICA)
    reglamento = models.BooleanField()
    ley_722 = models.BooleanField()
    educacion = models.BooleanField()
    agua_saneamiento = models.BooleanField()
    manejo_financiero = models.BooleanField()
    manejo_administrativo = models.BooleanField()
    otros = models.BooleanField()

    class Meta:
        verbose_name_plural = 'Asistencia técnica del CAPS'

class FamiliasAtendidas(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    numero_tomas = models.IntegerField()
    numero_familias = models.IntegerField()
    numero_personas = models.IntegerField()

    class Meta:
        verbose_name_plural = 'Número de familias atendidas'

class MiembrosFamilias(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    edad = models.IntegerField(choices=CHOICE_EDAD_FAMILIA)
    cantidad = models.IntegerField()

    class Meta:
        verbose_name_plural = 'Miembros de la familia'

class Necedidades(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    descripcion_necesidad = models.ManyToManyField(Necesidades)
    necesidad_atendida = models.ManyToManyField(NecesidadesAtendidas)

    class Meta:
        verbose_name_plural = 'Necesidades de los caps'

class ImagenesAdjuntos(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    foto = ImageField(upload_to='fotos/ficha_caps/', null=True, blank=True)
    credito_foto = models.CharField(max_length=250, null=True, blank=True)
    descripcion = models.CharField(max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = 'Imagen CAPS'
        verbose_name_plural = 'Imagenes CAPS'

class DocumentosCaps(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    nombre_documento = models.CharField(max_length=250)
    documentos = models.FileField(upload_to='documentosCaps/%Y/%m/%d/')

    def __unicode__(self):
        return self.nombre_documento

    class Meta:
        verbose_name = 'Documento CAPS'
        verbose_name_plural = 'Documentos CAPS'

class FodaCaps(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    fecha = models.DateField()
    fortalezas = models.TextField()
    oportunidad = models.TextField()
    debilidades = models.TextField()
    amenazas = models.TextField()

class Organizacion(models.Model):
    nombre_siglas = models.CharField(max_length=250)
    nombre_completo = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre_siglas

    class Meta:
        verbose_name = 'Organización'
        verbose_name_plural = 'Organizaciones'

class Rubro(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Rubro'
        verbose_name_plural = 'Rubros'


class ProveedorFondo(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    organizacion = models.ForeignKey(Organizacion)
    fecha = models.DateField()

class InversionProveedor(models.Model):
    proveedor = models.ForeignKey(ProveedorFondo)
    rubro = models.ForeignKey(Rubro)
    presupuestado = models.FloatField()
    ejecutado = models.FloatField()

    class Meta:
        verbose_name_plural = 'Tipos de inversiones'


class OtroProveedorFondo(models.Model):
    ficha_caps = models.ForeignKey(FichaCaps)
    organizacion = models.ForeignKey(Organizacion)
    fecha = models.DateField()

class InversionOtrosProveedores(models.Model):
    proveedor = models.ForeignKey(OtroProveedorFondo)
    rubro = models.ForeignKey(Rubro)
    ejecutado = models.FloatField()

    class Meta:
        verbose_name_plural = 'Tipos de inversiones de otros proveedores'
