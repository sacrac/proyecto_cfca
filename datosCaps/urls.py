from django.conf.urls import url
from django.views.generic import TemplateView

from . import views
from beneficiarios.views import mejoras_perforacion_personas

urlpatterns = [
    url(r'^cobertura/$', views.mapa_con_filtro, name="cobertura"),
    url(r'^perfiles-caps/(?P<pk>\d+)/$', views.DetalleCaps.as_view(), name="detail-caps"),
    url(r'^mapacaps/', views.obtener_lista_mapa_caps, name='obtener-lista-mapa-caps'),
    url(r'^personas/', mejoras_perforacion_personas, name='personas'),
    url(r'^filtro-mapa/', views.mapa_con_filtro, name='filtros-mapa-caps'),
    url(r'^mobile-mapa/', views.mapa_con_filtro_mobile, name='mobile-mapa-caps'),
    url(r'^caps-pdf/(?P<pk>\d+)/$', views.genratePdf, name='generatePdf'),
    url(r'^mapa-actividades/', views.mapa_actividades, name='mapa-actividades'),
]
