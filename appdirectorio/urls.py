from django.conf.urls import url, include
from appdirectorio.views import MunicipioAutocomplete, ComunidadAutocomplete, LugarAutocomplete
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'departamento', views.DepartamentoViewSet)
router.register(r'directorio', views.DirectorioViewSet)
router.register(r'municipio', views.MunicipioViewSet)
router.register(r'cursos', views.CursosViewSet)
router.register(r'contenidos', views.ContenidoViewSet)
router.register(r'modulos', views.ModuloViewSet)
router.register(r'temas', views.TemaViewSet)
router.register(r'directorios', views.DirectoriosListView)

urlpatterns = [
    url(
        'municipio-autocomplete/$',
        MunicipioAutocomplete.as_view(create_field='nombre'),
        name='muni-autocomplete',
    ),
    url(
        'comunidad-autocomplete/$',
        ComunidadAutocomplete.as_view(create_field='nombre'),
        name='comu-autocomplete',
    ),
    url(
        'lugar-autocomplete/$',
        LugarAutocomplete.as_view(create_field='nombre'),
        name='lugar-autocomplete',
    ),
    url(r'^',include(router.urls)),
]
