from appdirectorio.models import Directorio, Tema
from lugar.models import Departamento,Municipio,Comunidad
from aprendizaje.models import Cursos, Modulos, Contenidos
from rest_framework import serializers

class ComunidadSerializer(serializers.ModelSerializer):
	class Meta:
		model = Comunidad
		fields = ('id','nombre')

class DepartamentoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Departamento
		fields = ('id','nombre')

class MunicipioSerializer(serializers.ModelSerializer):
	departamento = DepartamentoSerializer()
	class Meta:
		model = Municipio
		fields = ('id','nombre','departamento')

class TemaSerializer(serializers.ModelSerializer):
	class Meta:
		model =  Tema
		fields = ('id','nombre_tema')

class DirectorioSerializer(serializers.HyperlinkedModelSerializer):
	tema = TemaSerializer()
	departamento = DepartamentoSerializer()
	municipio = MunicipioSerializer()
	comunidad = ComunidadSerializer()

	class Meta:
		model = Directorio
		fields = ('id','departamento','municipio','comunidad','tema','empresa','nombre_persona','direccion', 'telefono', 'oficina','email')

class CursoSerializer(serializers.ModelSerializer):

	class Meta:
		model = Cursos
		fields = ('id','titulo','imagen','imagen_banner','descripcion','fecha')

class ModulosSerializer(serializers.ModelSerializer):
	curso = CursoSerializer()

	class Meta:
		model = Modulos
		fields = ('id', 'curso', 'titulo', 'order')

class ContenidoSerializer(serializers.ModelSerializer):
	modulo = ModulosSerializer()

	class Meta:
		model = Contenidos
		fields = ('id','modulo','titulo','contenido','order','url_video','nombre_video')


class DirectoriosSerializer(serializers.ModelSerializer):
	departamento = serializers.ReadOnlyField(source="departamento.nombre")
	municipio = serializers.ReadOnlyField(source="municipio.nombre")
	comunidad = serializers.ReadOnlyField(source="comunidad.nombre")
	tema = serializers.ReadOnlyField(source="tema.nombre_tema")

	class Meta:
		model = Directorio
		fields = ('id','departamento','municipio','comunidad',
			      'tema','empresa','nombre_persona','direccion', 
			      'telefono', 'oficina','email')