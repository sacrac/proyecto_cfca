from django.shortcuts import render
from dal import autocomplete
from appdirectorio.models import Departamento, Municipio, Comunidad, Directorio, Cursos, Modulos, Contenidos, Tema
# from rest_framework.decorators import api_views
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework import mixins
from .serializers import (DepartamentoSerializer, DirectorioSerializer, 
                          MunicipioSerializer, CursoSerializer, 
                          ContenidoSerializer, ModulosSerializer, 
                          TemaSerializer, DirectoriosSerializer)

from actividades.models import Lugares
class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.is_authenticated():
        #     return Departamento.objects.none()

        qs = Municipio.objects.all()

        departamento = self.forwarded.get('departamento', None)

        if departamento:
            qs = qs.filter(departamento=departamento)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class ComunidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        #if not self.request.user.is_authenticated():
        #    return Departamento.objects.none()

        qs = Comunidad.objects.all()

        municipio = self.forwarded.get('municipio', None)

        if municipio:
            qs = qs.filter(municipio=municipio)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class LugarAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        #if not self.request.user.is_authenticated():
        #    return Departamento.objects.none()

        qs = Lugares.objects.all()

        municipio = self.forwarded.get('municipio', None)

        if municipio:
            qs = qs.filter(municipio=municipio)

        if self.q:
            qs = qs.filter(lugar__istartswith=self.q)

        return qs

class DepartamentoViewSet(viewsets.ModelViewSet):
    queryset = Departamento.objects.all()
    serializer_class = DepartamentoSerializer

class DirectorioViewSet(viewsets.ModelViewSet):
    queryset = Directorio.objects.all()
    serializer_class = DirectorioSerializer

class MunicipioViewSet(viewsets.ModelViewSet):
    queryset = Municipio.objects.all()
    serializer_class = MunicipioSerializer

class CursosViewSet(viewsets.ModelViewSet):
    queryset = Cursos.objects.all()
    serializer_class = CursoSerializer

class ContenidoViewSet(viewsets.ModelViewSet):
    queryset = Contenidos.objects.all()
    serializer_class = ContenidoSerializer

class ModuloViewSet(viewsets.ModelViewSet):
    queryset = Modulos.objects.all()
    serializer_class = ModulosSerializer

class TemaViewSet(viewsets.ModelViewSet):
    queryset = Tema.objects.all()
    serializer_class = TemaSerializer

class DirectoriosListView(mixins.ListModelMixin,viewsets.GenericViewSet):
    queryset = Directorio.objects.all()
    serializer_class = DirectoriosSerializer
    http_method_names = ['get', 'head']