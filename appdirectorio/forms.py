from dal import autocomplete

from django import forms
from .models import Directorio

class DirectorioForm(forms.ModelForm):
    class Meta:
        model = Directorio
        fields = ('__all__')
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete',forward=['departamento']),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio'])
        }
