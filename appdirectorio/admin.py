from django.contrib import admin

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import *
from lugar.models import Departamento, Municipio, Comunidad
from .forms import DirectorioForm



class DirectorioResource(resources.ModelResource):

    class Meta:
        model = Directorio

class DepartamentoAdmin(admin.ModelAdmin):
	search_fields = ['nombre_departamento']

class MunicipioAdmin(admin.ModelAdmin):
	search_fields = ['nombre_municipio']

class ComunidadAdmin(admin.ModelAdmin):
	search_fields = ['nombre_comunidad']


class DirectorioAdmin(ImportExportModelAdmin):
    form = DirectorioForm
    resource_class = DirectorioResource
    list_display = ['nombre_persona', 'tema', 'departamento', 'municipio']
    search_fields = ['nombre_persona','empresa','direccion']
    list_filter = ['tema','departamento','municipio']
    readonly_fields = ('modificado',)

class TemaAdmin(ImportExportModelAdmin):
    list_display = ['nombre_tema', 'slug']

# Register your models here.
admin.site.register(Tema, TemaAdmin)
admin.site.register(Directorio, DirectorioAdmin)
