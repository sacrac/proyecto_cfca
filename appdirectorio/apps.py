from django.apps import AppConfig


class AppdirectorioConfig(AppConfig):
    name = 'appdirectorio'
