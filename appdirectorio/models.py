# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify
from lugar.models import Departamento, Municipio, Comunidad
from aprendizaje.models import Cursos, Modulos, Contenidos

# Create your models here.

class Tema(models.Model):
    nombre_tema = models.CharField(max_length = 255)
    slug = models.SlugField(max_length=255, editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.nombre_tema))
        super(Tema, self).save(*args, **kwargs)

    def __str__(self):
        return self.nombre_tema

    class Meta:
        verbose_name = 'Clasificación'
        verbose_name_plural = 'Clasificaciones'

class Directorio(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete = models.CASCADE, null = True)
    municipio = models.ForeignKey(Municipio, on_delete = models.CASCADE, null=True)
    comunidad = models.ForeignKey(Comunidad, on_delete = models.CASCADE, null=True)
    tema = models.ForeignKey(Tema, on_delete = models.CASCADE, verbose_name='Clasificación')
    empresa = models.CharField('Empresa/Institucion', max_length = 255, null=True, blank=True)
    nombre_persona = models.CharField(max_length = 255, null=True, blank=True)
    direccion = models.CharField(max_length = 255, null=True, blank=True)
    telefono = models.IntegerField(null=True, blank=True)
    oficina = models.IntegerField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    modificado = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre_persona

    def save(self, *args, **kwargs):
        self.modificado += 1
        super(Directorio, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Directorio de contacto'
        verbose_name_plural = 'Directorios de contactos'
