# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-09-14 03:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('encuestas', '0004_auto_20190822_2058'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ('order',), 'verbose_name': 'Pregunta', 'verbose_name_plural': 'Preguntas'},
        ),
        migrations.AlterField(
            model_name='question',
            name='choices',
            field=models.TextField(blank=True, help_text='si el tipo de pregunta es "radio", "seleccionar" o "seleccionar múltiples", proporcione una lista de opciones separadas por comas para esta pregunta .', null=True, verbose_name='Opciones'),
        ),
    ]
