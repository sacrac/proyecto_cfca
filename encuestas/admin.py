# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from encuestas.models import Question, Category, Survey, Response, AnswerText, AnswerRadio, AnswerSelect, AnswerInteger, AnswerSelectMultiple
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

class QuestionInline(admin.TabularInline):
    model = Question
    ordering = ('category',)
    extra = 0

class CategoryInline(admin.TabularInline):
    model = Category
    extra = 0

class SurveyAdmin(admin.ModelAdmin):
    inlines = [QuestionInline]

class AnswerBaseInline(admin.StackedInline):
    fields = ('question', 'body')
    readonly_fields = ('question',)
    extra = 0

class AnswerTextInline(AnswerBaseInline):
    model= AnswerText
    max_num = 1
    can_delete = False

class AnswerRadioInline(AnswerBaseInline):
    model= AnswerRadio
    max_num = 1
    can_delete = False

class AnswerSelectInline(AnswerBaseInline):
    model= AnswerSelect
    max_num = 1
    can_delete = False

class AnswerSelectMultipleInline(AnswerBaseInline):
    model= AnswerSelectMultiple
    max_num = 1
    can_delete = False

class AnswerIntegerInline(AnswerBaseInline):
    model= AnswerInteger
    max_num = 1
    can_delete = False

class ResponseAdmin(admin.ModelAdmin):
    list_display = ('interview_uuid', 'created','interviewee','municipio','comunidad','caps')
    inlines = [AnswerTextInline, AnswerRadioInline, AnswerSelectInline, AnswerSelectMultipleInline, AnswerIntegerInline]
    # specifies the order as well as which fields to act on
    readonly_fields = ('survey', 'created', 'updated', 'interview_uuid')
    search_fields = ('interview_uuid','interviewee')
    list_filter = ['municipio','survey']

#admin.site.register(Question, QuestionInline)
admin.site.register(Category)
admin.site.register(Survey, SurveyAdmin)

admin.site.register(Response, ResponseAdmin)
