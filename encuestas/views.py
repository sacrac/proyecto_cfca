# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404, redirect
from django.http import  HttpResponseRedirect
from .models import (Question, Survey, Category,
                     Response, AnswerBase,
                     AnswerRadio, AnswerSelect,
                     AnswerSelectMultiple, AnswerText)
from .forms import ResponseForm, ConsultaForm
from django.contrib import messages

from lugar.models import Comunidad, Municipio
from datosCaps.models import FichaCaps
# Create your views here.

def _queryset_filtrado(request):
    params = {}

    if 'fecha_inicial' in request.session:
       params['created__range'] = (request.session['fecha_inicial'],request.session['fecha_final'])

    if 'municipio' in request.session:
       params['municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
       params['comunidad'] = request.session['comunidad']

    if 'caps' in request.session:
       params['caps'] = request.session['caps']

    if 'sexo' in request.session:
       params['sexo'] = request.session['sexo']

    if 'rango_edad' in request.session:
       params['rango_edad'] = request.session['rango_edad']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]
    print("params")
    print(params)
    return Response.objects.filter(**params)

def Index(request):
    encuestas = Survey.objects.all()
    return render( request, 'encuestas/index_encuesta.html', {'survey': encuestas} )


def SurveyDetail(request, id):
    survey = Survey.objects.get(id=id)
    #category_items = Category.objects.filter(survey=survey)
    #categories = [c.name for c in category_items]
    #print 'categorías para esta encuesta este texto esta en la vista:'
    #print categories
    if request.method == 'POST':
        form = ResponseForm(request.POST, survey=survey)
        if form.is_valid():
            response = form.save()
            #return HttpResponseRedirect("/encuestas/confirm/%s" % response.interview_uuid)
            messages.success(request, 'Gracias! por realizar la encuestas. Su respuesta ha sido almacenada con exito!!!')
            return redirect('/encuestas')
    else:
        form = ResponseForm(survey=survey)
        #print form
        # TODO sort by category
    return render(request, 'encuestas/encuesta.html', {'response_form': form, 'survey': survey})

def confirm(request, uuid):
    return render(request, 'encuestas/confirmacion.html', {'uuid':uuid})

def stadistic_survey(request, id):
    if request.method == 'POST':
        form = ConsultaForm(request.POST)
        if form.is_valid():
            request.session['fecha_inicial'] = form.cleaned_data['fecha_inicial']
            request.session['fecha_final'] = form.cleaned_data['fecha_final']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['caps'] = form.cleaned_data['caps']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['rango_edad'] = form.cleaned_data['rango_edad']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaForm()
        centinela = 0

    if centinela == 1:
        #print("entro bien al filtro")
        survey = get_object_or_404(Survey, pk=id)
        responses = _queryset_filtrado(request)
        filtro = responses.filter(survey=survey)
        numero_respuestas = filtro.count()
        #print(survey)
        #print(filtro.count())
    else:
        #print("aca entra sin filtro")
        survey = get_object_or_404(Survey, pk=id)
        filtro = Response.objects.filter(survey=survey)
        numero_respuestas = filtro.count()
        #print(survey)
        #print(filtro.count())

    # lista_participante = []
    # for obj in filtro:
    #     lista_participante.append([obj.survey,
    #                                obj.interviewee,
    #                                obj.municipio,
    #                                obj.caps,
    #                                obj.sexo])
    # print(lista_participante)

    dict_conteo_radio = {}
    dict_conteo_select = {}
    dict_conteo_multiselect = {}
    dict_conteo_text = {}
    for obj in survey.questions():
        #analisis de los radios
        if obj.question_type == Question.RADIO:
            dict_conteo_radio[obj] = {}
            for x in obj.choices.split(','):
                #if filtro:
                conteo = AnswerRadio.objects.filter(question = obj.id,body__contains=x,response__in=filtro).count()
                # else:
                #     conteo = AnswerRadio.objects.filter(question = obj.id,body__contains=x).count()
                dict_conteo_radio[obj][x] = conteo
        #analisis de los select
        if obj.question_type == Question.SELECT:
            dict_conteo_select[obj] = {}
            for x in obj.choices.split(','):
                #if filtro:
                conteo = AnswerSelect.objects.filter(question = obj.id,body__contains=x,response__in=filtro).count()
                # else:
                #     conteo = AnswerSelect.objects.filter(question = obj.id,body__contains=x).count()
                dict_conteo_select[obj][x] = conteo
        #analisis de los multipleSelect
        if obj.question_type == Question.SELECT_MULTIPLE:
            dict_conteo_multiselect[obj] = {}
            for x in obj.choices.split(','):
                #if filtro:
                conteo = AnswerSelectMultiple.objects.filter(question = obj.id,body__contains=x,response__in=filtro).count()
                # else:
                #     conteo = AnswerSelectMultiple.objects.filter(question = obj.id,body__contains=x).count()
                dict_conteo_multiselect[obj][x] = conteo
        #analisis de los TEXT
        if obj.question_type == Question.TEXT:
            respuestas = []
            for x in AnswerText.objects.filter(question = obj.id,response__in=filtro):
                respuestas.append(x.answertext.body)
                dict_conteo_text[obj] = respuestas

    return render(request, 'encuestas/analisis.html',{'survey':survey,
                           'dict_conteo_radio':dict_conteo_radio,
                           'dict_conteo_select':dict_conteo_select,
                           'dict_conteo_multiselect':dict_conteo_multiselect,
                           'dict_conteo_text':dict_conteo_text,
                           'form':form,
                           'numero_respuestas':numero_respuestas,
                           'centinela':centinela})

def load_municipios(request):
    departamento_id = request.GET.get('departamento')
    lugares = Municipio.objects.filter(departamento_id=departamento_id).order_by('nombre')

    return render(request, 'encuestas/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_comunidades(request):
    municipio_id = request.GET.get('municipio')
    lugares = Comunidad.objects.filter(municipio_id=municipio_id).order_by('nombre')

    return render(request, 'encuestas/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_caps(request):
    comunidad_id = request.GET.get('comunidad')
    lugares = FichaCaps.objects.filter(comunidad__id=comunidad_id).order_by('nombre_caps')

    return render(request, 'encuestas/lugares_dropdown_list_options.html', {'lugares': lugares})

def ver_lista(request, id):
    params = {}

    if 'fecha_inicial' in request.session:
       params['created__range'] = (request.session['fecha_inicial'],request.session['fecha_final'])

    if 'municipio' in request.session:
       params['municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
       params['comunidad'] = request.session['comunidad']

    if 'caps' in request.session:
       params['caps'] = request.session['caps']

    if 'sexo' in request.session:
       params['sexo'] = request.session['sexo']

    if 'rango_edad' in request.session:
       params['rango_edad'] = request.session['rango_edad']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    responses = Response.objects.filter(**params).select_related('caps','municipio')
    survey = get_object_or_404(Survey, pk=id)
    respuestas = responses.filter(survey=survey).select_related('caps','municipio')
    return render(request,'encuestas/tabla_entrevistado.html',{'tabla':respuestas,
                                                            'encuesta':survey})

def update_survey(request, pk_survey, pk_response):
    response = Response.objects.get(id=pk_response)
    if request.method == 'POST':
        form = ResponseForm(request.POST, survey=survey)
        if form.is_valid():
            response = form.save()
            #return HttpResponseRedirect("/encuestas/confirm/%s" % response.interview_uuid)
            messages.success(request, 'Gracias! por realizar la encuestas. Su respuesta ha sido almacenada con exito!!!')
            return redirect('/encuestas')
    else:
        form = ResponseForm(survey=survey)
    return render()
