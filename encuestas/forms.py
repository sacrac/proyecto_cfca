# -*- coding: utf-8 -*-
from django import forms
from django.forms import models
from django.utils.safestring import mark_safe

from lugar.models import Municipio, Comunidad
from datosCaps.models import FichaCaps
from encuestas.models import Question, Category, Survey, Response, AnswerText, AnswerRadio, AnswerSelect, AnswerInteger, AnswerSelectMultiple

import uuid

# blatantly stolen from
# http://stackoverflow.com/questions/5935546/align-radio-buttons-horizontally-in-django-forms?rq=1
#class HorizontalRadioRenderer(forms.RadioSelect.renderer):
#  def render(self):
#    return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))
CHOICES_SEXO = (
                (1, 'Hombre'),
                (2, 'Mujer'),
    )

CHOICES_EDAD_RANGOS = (
                (1, '16 - 22'),
                (2, '23 - 29'),
                (3, '30 - 36'),
                (4, '37 - 43'),
                (5, '44 - 49'),
                (6, '50 - 56'),
                (7, '57 en adelante'),
    )

class ResponseForm(models.ModelForm):
    sexo = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect,
        choices=CHOICES_SEXO)
    rango_edad = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect,
        choices=CHOICES_EDAD_RANGOS)
    class Meta:
        model = Response
        fields = ('interviewee','municipio','comunidad','caps','sexo','rango_edad')

    def __init__(self, *args, **kwargs):
        # espera que un objeto de encuesta sea aprobado inicialmente
        survey = kwargs.pop('survey')
        self.survey = survey
        super(ResponseForm, self).__init__(*args, **kwargs)
        self.uuid = random_uuid = uuid.uuid4().hex

        # agregue un campo para cada pregunta de la encuesta, correspondiente al tipo de pregunta
        # según corresponda.
        data = kwargs.get('data')
        for q in survey.questions().order_by('order'):
            if q.question_type == Question.TEXT:
                self.fields["question_%d" % q.pk] = forms.CharField(label=q.text,
                    widget=forms.Textarea)
            elif q.question_type == Question.RADIO:
                question_choices = q.get_choices()
                self.fields["question_%d" % q.pk] = forms.ChoiceField(label=q.text,
                    widget=forms.RadioSelect(),
                    choices = question_choices)
            elif q.question_type == Question.SELECT:
                question_choices = q.get_choices()
                # agregue una opción vacía en la parte superior para que el usuario tenga que
                # seleccionar explícitamente una de las opciones
                question_choices = tuple([('', '-------------')]) + question_choices
                self.fields["question_%d" % q.pk] = forms.ChoiceField(label=q.text,
                    widget=forms.Select, choices = question_choices)
            elif q.question_type == Question.SELECT_MULTIPLE:
                question_choices = q.get_choices()
                self.fields["question_%d" % q.pk] = forms.MultipleChoiceField(label=q.text,
                    widget=forms.CheckboxSelectMultiple, choices = question_choices)
            elif q.question_type == Question.INTEGER:
                self.fields["question_%d" % q.pk] = forms.IntegerField(label=q.text)

            # si el campo es obligatorio, proporciónalo con una clase CSS correspondiente.
            if q.required:
                self.fields["question_%d" % q.pk].required = True
                self.fields["question_%d" % q.pk].widget.attrs["class"] = "required"
            else:
                self.fields["question_%d" % q.pk].required = False

            # agregue la categoría como una clase css, y agréguela como un atributo de datos
            # también (esto se usa en la plantilla para permitir ordenar las preguntas por categoría)
            if q.category:
                classes = self.fields["question_%d" % q.pk].widget.attrs.get("class")
                if classes:
                    self.fields["question_%d" % q.pk].widget.attrs["class"] = classes + (" cat_%s" % q.category.name)
                else:
                    self.fields["question_%d" % q.pk].widget.attrs["class"] = (" cat_%s" % q.category.name)
                self.fields["question_%d" % q.pk].widget.attrs["category"] = q.category.name


            # inicialice el campo de formulario con los valores de una solicitud POST, si corresponde.
            if data:
                self.fields["question_%d" % q.pk].initial = data.get('question_%d' % q.pk)

    def save(self, commit=True):
        # guardar el objeto de respuesta
        response = super(ResponseForm, self).save(commit=False)
        response.survey = self.survey
        response.interview_uuid = self.uuid
        response.save()

        # crear un objeto de respuesta para cada pregunta y asociarlo con esta
        # respuesta.
        for field_name, field_value in self.cleaned_data.items():
            if field_name.startswith("question_"):
                # advertencia: esta forma de extraer la identificación es muy frágil y
                # completamente dependiente de la forma en que el question_id está codificado en el
                # nombre de campo en el método __init__ de esta clase de formulario.
                q_id = int(field_name.split("_")[1])
                q = Question.objects.get(pk=q_id)

                if q.question_type == Question.TEXT:
                    a = AnswerText(question = q)
                    a.body = field_value
                elif q.question_type == Question.RADIO:
                    a = AnswerRadio(question = q)
                    a.body = field_value
                elif q.question_type == Question.SELECT:
                    a = AnswerSelect(question = q)
                    a.body = field_value
                elif q.question_type == Question.SELECT_MULTIPLE:
                    a = AnswerSelectMultiple(question = q)
                    a.body = field_value
                elif q.question_type == Question.INTEGER:
                    a = AnswerInteger(question = q)
                    a.body = field_value
                #print "creando una respuesta para la pregunta %d de tipo %s" % (q_id, a.question.question_type)
                #print a.question.text
                #print 'valor de respuesta:'
                #print field_value
                a.response = response
                a.save()
        return response



CHOICES_SEXO2 = (
                ('', '------'),
                (1, 'Hombre'),
                (2, 'Mujer'),
    )

CHOICES_EDAD_RANGOS2 = (
                ('', '--------'),
                (1, '16 - 22'),
                (2, '23 - 29'),
                (3, '30 - 36'),
                (4, '37 - 43'),
                (5, '44 - 49'),
                (6, '50 - 56'),
                (7, '57 en adelante'),
    )

class ConsultaForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ConsultaForm, self).__init__(*args, **kwargs)
        self.fields['fecha_inicial'].widget.attrs.update({'class': 'form-control ',
                                                   'data-placeholder':'Fecha inicial', 'tabindex':'-1','area-hidden': 'true'})
        self.fields['fecha_final'].widget.attrs.update({'class': 'form-control ',
                                                   'data-placeholder':'Fecha final', 'tabindex':'-1','area-hidden': 'true'})
        self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['comunidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['caps'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
        self.fields['rango_edad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})

        # if 'pais' in self.data:
        #     try:
        #         pais_id = int(self.data.get('pais'))
        #         self.fields['departamento'].queryset = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
        #     except (ValueError, TypeError):
        #         pass  # invalid input from the client; ignore and fallback to empty City queryset

        if 'comunidad' in self.data:
            try:
                municipio_id = int(self.data.get('municipio'))
                self.fields['comunidad'].queryset = Comunidad.objects.filter(municipio_id=municipio_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty Comunidad queryset

        if 'caps' in self.data:
            try:
                comunidad_id = int(self.data.get('comunidad'))
                self.fields['caps'].queryset = FichaCaps.objects.filter(comunidad=comunidad_id).order_by('nombre_caps')
            except (ValueError, TypeError):
                pass  # invalid input form the client

    fecha_inicial = forms.CharField(widget=forms.DateInput(), required=True)
    fecha_final = forms.CharField(widget=forms.DateInput(), required=True)
    sexo = forms.ChoiceField(choices=CHOICES_SEXO2, required=False)
    rango_edad = forms.ChoiceField(choices=CHOICES_EDAD_RANGOS2, required=False, label='Rango de edad')
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.all(), required=False)
    comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.none(), required=False)
    caps = forms.ModelChoiceField(queryset=FichaCaps.objects.none(), required=False)

