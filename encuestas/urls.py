from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.Index, name='home'),
    url(r'^survey/(?P<id>\d+)/$', views.SurveyDetail, name='survey_detail'),
    url(r'^confirm/(?P<uuid>\w+)/$', views.confirm, name='confirmation'),
    url(r'^analisis/survey/(?P<id>\d+)/$', views.stadistic_survey, name='survey_analisis'),
    url(r'^ver-tabla/(?P<id>\d+)/$', views.ver_lista, name='ver-tabla-participante'),

    url(r'^ajax/load-municipios/', views.load_municipios, name='ajax_load_municipios'),
    url(r'^ajax/load-comunidad/', views.load_comunidades, name='ajax_load_comunidad'),
    url(r'^ajax/load-caps/', views.load_caps, name='ajax_load_caps'),
]
