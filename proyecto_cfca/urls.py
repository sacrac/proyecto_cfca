# -*- coding: utf-8 -*-
"""proyecto_cfca URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views
from proyecto_cfca import settings
#vistas
from django.views.generic import TemplateView
from aprendizaje.views import signup

admin.site.site_header = 'Administración proyecto C.F.C.A'

urlpatterns = [
    url(r'', include('web.urls')),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^signup/$', signup, name='signup'),
    url(r'^paginas/', include('django.contrib.flatpages.urls')),
    url(r'^cursos/', include('aprendizaje.urls')),
    url(r'^encuestas/', include('encuestas.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    url(r'^actividades/', include('actividades.urls')),
    url(r'^censo/', include('beneficiarios.urls')),
    url(r'^caps/', include('datosCaps.urls')),
    url(r'^lugar/', include('lugar.urls')),
    url(r'^app/v1/', include('appdirectorio.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^redes-sociales/', include('redes_sociales.urls')),
    #url(r'^webpush/', include('webpush.urls')),
    #url(r'^sw.js', TemplateView.as_view(template_name='sw.js', content_type='application/x-javascript')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL , document_root = settings.STATIC_ROOT )
    urlpatterns += static(settings.MEDIA_URL , document_root = settings.MEDIA_ROOT )
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))

urlpatterns += staticfiles_urlpatterns()
