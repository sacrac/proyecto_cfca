# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from .models import *
from actividadgerencial.models import Years, CHOICES_TRIMESTRE
from django.db.models import Avg, Sum, F, Count, Q
import collections
from django.db.models.functions import Coalesce

# Create your views here.
@login_required
def financiero(request, template='seguimiento/seguimiento_financiero.html'):
	anios = Years.objects.values_list('id','nombre')
	dict = collections.OrderedDict()
	for x in anios:
		presupuesto_acumulado = InformeFinanciero.objects.filter(anio = x[0]).aggregate(total = Sum('presupuesto'))['total']
		desembolso_acumulado = EstadoIngvsGastos.objects.filter(anio = x[0]).aggregate(total = Coalesce(Sum('ingresos__valor'),0))['total']
		total_ejecucion = InformeFinanciero.objects.filter(anio = x[0]).aggregate(total = Sum('detalleinforme__monto'))['total']
		ingresos_fecha = Ingresos.objects.filter(estado__anio = x[0])
		
		rubro = collections.OrderedDict()
		tabla = []
		presupuesto_total = 0
		ejecucion_total = 0
		saldo_total = 0
		t1_total = 0
		t2_total = 0
		t3_total = 0
		t4_total = 0
		list_totales = []
		for ru in Rubros.objects.all():
			presupuesto = InformeFinanciero.objects.filter(rubro = ru,anio = x[0]).aggregate(total = Coalesce(Sum('presupuesto'),0))['total']
			ejecucion = InformeFinanciero.objects.filter(rubro = ru,anio = x[0]).aggregate(total = Coalesce(Sum('detalleinforme__monto'),0))['total']
			
			lista = []
			for tri in CHOICES_TRIMESTRE:
				ejecutado = DetalleInforme.objects.filter(informe__rubro = ru,trimestre = tri[0],informe__anio = x[0]).aggregate(
														total = Coalesce(Sum('monto'),0))['total']
				lista.append(ejecutado)
			rubro[ru] = presupuesto,ejecucion,lista

			#tabla
			saldo_por_ejecutar = presupuesto - ejecucion
			variacion = saca_porcentajes(ejecucion,presupuesto,False)
			tabla.append((ru,presupuesto,lista,ejecucion,saldo_por_ejecutar,variacion))

			#totales	
			presupuesto_total += presupuesto
			ejecucion_total += ejecucion
			saldo_total += saldo_por_ejecutar
			t1 = DetalleInforme.objects.filter(informe__rubro = ru,trimestre = 1,informe__anio = x[0]).aggregate(
														total = Coalesce(Sum('monto'),0))['total']
			t1_total += t1
			
			t2 = DetalleInforme.objects.filter(informe__rubro = ru,trimestre = 2,informe__anio = x[0]).aggregate(
															total = Coalesce(Sum('monto'),0))['total']
			t2_total += t2
			
			t3 = DetalleInforme.objects.filter(informe__rubro = ru,trimestre = 3,informe__anio = x[0]).aggregate(
															total = Coalesce(Sum('monto'),0))['total']
			t3_total += t3
			
			t4 = DetalleInforme.objects.filter(informe__rubro = ru,trimestre = 4,informe__anio = x[0]).aggregate(
														total = Coalesce(Sum('monto'),0))['total']
			t4_total += t4

		list_totales.append(t1_total)
		list_totales.append(t2_total)
		list_totales.append(t3_total)
		list_totales.append(t4_total)

		variacion_total = saca_porcentajes(ejecucion_total,presupuesto_total,False)
		tabla.append(('Total general',presupuesto_total,list_totales,ejecucion_total,saldo_total,variacion_total))

		if presupuesto_acumulado != None:
			dict[x[1]] = ingresos_fecha,presupuesto_acumulado,desembolso_acumulado,total_ejecucion,rubro,tabla

	return render(request,template,locals())

def saca_porcentajes(dato, total, formato=True):
	if dato != None:
		try:
			porcentaje = (dato/float(total)) * 100 if total != None or total != 0 else 0
		except:
			return 0
		if formato:
			return porcentaje
		else:
			return '%.2f' % porcentaje
	else:
		return 0
