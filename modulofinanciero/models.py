# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import slugify
from actividadgerencial.models import Years, CHOICES_TRIMESTRE

# Create your models here.
class Rubros(models.Model):
	nombre = models.CharField(max_length=200)
	slug = models.SlugField(max_length=220, editable=False)

	class Meta:
		verbose_name = 'Rubro'
		verbose_name_plural = "Rubros"

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.slug = (slugify(self.nombre))
		super(Rubros, self).save(*args, **kwargs)

class InformeFinanciero(models.Model):
	rubro = models.ForeignKey(Rubros, on_delete=models.DO_NOTHING)
	anio = models.ForeignKey(Years, on_delete=models.CASCADE,verbose_name='Año')
	presupuesto = models.FloatField()

	def __str__(self):
		return self.rubro.nombre

	class Meta:
		verbose_name = "Ejecución"
		verbose_name_plural = "Ejecución"

class DetalleInforme(models.Model):
	informe = models.ForeignKey(InformeFinanciero, on_delete=models.CASCADE)
	trimestre = models.IntegerField(choices=CHOICES_TRIMESTRE)
	monto = models.FloatField()

class EstadoIngvsGastos(models.Model):
	anio = models.ForeignKey(Years, on_delete=models.CASCADE,verbose_name='Año')
	trimestre = models.IntegerField(choices=CHOICES_TRIMESTRE)

	def __str__(self):
		return '%s %s' % (self.anio,self.get_trimestre_display()) 

	class Meta:
		verbose_name = "Estado de Ingresos vs Gastos"
		verbose_name_plural = "Estado de Ingresos vs Gastos"

class Ingresos(models.Model):
	estado = models.ForeignKey(EstadoIngvsGastos, on_delete=models.CASCADE)
	fecha_recibida = models.DateField()
	valor = models.FloatField()
	
	class Meta:
		verbose_name_plural = "Ingresos"