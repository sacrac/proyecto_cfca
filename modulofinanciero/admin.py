# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.
class DetalleInformeInline(admin.TabularInline):
	model = DetalleInforme
	extra = 1
	max_num = 4 

class InformeFinancieroAdmin(admin.ModelAdmin):
	inlines = [DetalleInformeInline,]
	list_display = ('rubro','anio','presupuesto')

class IngresosInline(admin.TabularInline):
	model = Ingresos
	extra = 1

class EstadoIngvsGastosAdmin(admin.ModelAdmin):
	inlines = [IngresosInline,]
	list_display = ('anio','trimestre')

admin.site.register(InformeFinanciero,InformeFinancieroAdmin)
admin.site.register(Rubros)
admin.site.register(EstadoIngvsGastos,EstadoIngvsGastosAdmin)
